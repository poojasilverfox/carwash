package com.silverfox.carwash;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;




import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Activity.MainActivity;
import com.silverfox.carwash.Activity.SplashActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Random;

import static android.content.ContentValues.TAG;


public class MyFirebaseMessagingService extends FirebaseMessagingService {


    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("TAG", "Refreshed token 222::::: " + refreshedToken);

    }


    private String path;
    Map<String, String> data;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG, "From: " + remoteMessage.getFrom());
        String title, is_requesting, message, tournament_type_id, user_id, game_id, notification_type;
        data = remoteMessage.getData();
        Log.e(TAG, "Notification Message Body: " + remoteMessage.getNotification() + " data = " + data);
//        Log.e(TAG, "Notification Message LINK:::: " + data.get("play_link"));
//        { title=Joining confirmation, is_requesting=0,message=vb has accepted to join your clan request}
        title = data.get("title");
        is_requesting = data.get("is_requesting");
        message = data.get("message");
        tournament_type_id = data.get("tournament_type_id");
        user_id = data.get("user_id");
        game_id = data.get("game_id");
        notification_type = data.get("notification_type");
        Log.e(TAG, "Notification Message DATA:::: " + data);
        Log.e(TAG, "Notification Message MSG:::: " + title);
        Log.e(TAG, "Notification Message MSG:::: " + is_requesting);
        Log.e(TAG, "Notification Message MSG:::: " + message);
        Log.e(TAG, "Notification Message MSG:::: " + tournament_type_id);
        Log.e(TAG, "Notification Message MSG:::: " + user_id);
        Log.e(TAG, "Notification Message MSG:::: " + game_id);
        if (message != null && (!message.equals(""))) {
            sendNotification(message, title, is_requesting, tournament_type_id, user_id, game_id, notification_type);
        }
    }

    private void sendNotification(String messageBody, String title, String is_requesting,
                                  String tournament_type_id, String user_id, String game_id, String notification_type) {
        title = getString(R.string.app_name);
        Intent intent = new Intent();
        final String PRIMARY_CHANNEL = "default";
//        new generatePictureStyleNotification(this, title, messageBody, iconUrl).execute();
        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;
        if(Preference.getUSER_ID() == 0) {
            intent = new Intent(this, LoginActivity.class);

        }else {
            intent = new Intent(this, MainActivity.class);
            intent.putExtra("notification", true);
        }


//        intent.putExtra(AppConstant.NOTIFICTION_ALERT, true);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, m, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notif;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel chan1 = new NotificationChannel(PRIMARY_CHANNEL,
                    getString(R.string.noti_channel_default), NotificationManager.IMPORTANCE_DEFAULT);
            chan1.setLightColor(Color.GREEN);
            chan1.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            notificationManager.createNotificationChannel(chan1);
            notif = new Notification.Builder(getApplicationContext(), PRIMARY_CHANNEL)
                    .setContentIntent(pendingIntent)
                    .setTicker(messageBody)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setSound(alarmSound)
                    .setAutoCancel(false)
                    .setContentText(messageBody)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setStyle(new Notification.BigTextStyle()
                            .bigText(messageBody)
                            .setBigContentTitle(title))
                    .build();
        } else {
            notif = new Notification.Builder(this)
                    .setContentIntent(pendingIntent)
                    .setTicker(messageBody)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setAutoCancel(false)
                    .setSound(alarmSound)
                    .setContentText(messageBody)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setStyle(new Notification.BigTextStyle()
                            .bigText(messageBody)
                            .setBigContentTitle(title))
                    .build();
        }
        notif.flags = Notification.FLAG_ONGOING_EVENT;
        notif.flags = Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(m, notif);
    }

    public class generatePictureStyleNotification extends AsyncTask<String, Void, Bitmap> {

        private Context mContext;
        private String title, message, imageUrl;

        public generatePictureStyleNotification(Context context, String title, String message, String imageUrl) {
            super();
            this.mContext = context;
            this.title = title;
            this.message = message;
            this.imageUrl = imageUrl;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            InputStream in;
            try {
                URL url = new URL(this.imageUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                in = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(in);
                return myBitmap;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            Random random = new Random();
            int m = random.nextInt(9999 - 1000) + 1000;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(path));
            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, m, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notif = new Notification.Builder(mContext)
                    .setContentIntent(pendingIntent)
                    .setContentTitle(title)
                    .setAutoCancel(false)
                    .setOngoing(true)
                    .setSound(alarmSound)
                    .setContentText(message)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(result)
                    .setStyle(new Notification.BigPictureStyle().bigPicture(result))
                    .build();
            notif.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(m, notif);
        }
    }
}