package com.silverfox.carwash.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import com.silverfox.carwash.R;


public class CustomRobotoAutoCompleteTextView extends androidx.appcompat.widget.AppCompatAutoCompleteTextView {

    public CustomRobotoAutoCompleteTextView(Context context) {
        super(context);
    }

    public CustomRobotoAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        parseAttributes(context, attrs); //I'll explain this method later
    }

    public CustomRobotoAutoCompleteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        parseAttributes(context, attrs);
    }

    private void parseAttributes(Context context, AttributeSet attrs) {
        @SuppressLint("CustomViewStyleable") TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TextViewCustom);
        String customFont = typedArray.getString(R.styleable.TextViewCustom_custom_fonts);
        if (customFont != null && customFont.length() > 0) {
            setTypeface(TypeFaceProvider.getTypeFace(context, customFont));
        }
        typedArray.recycle();
    }
}
