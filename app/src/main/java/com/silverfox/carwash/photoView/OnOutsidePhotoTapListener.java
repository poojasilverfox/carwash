package com.silverfox.carwash.photoView;

import android.widget.ImageView;

public interface OnOutsidePhotoTapListener {
    void onOutsidePhotoTap(ImageView imageView);
}
