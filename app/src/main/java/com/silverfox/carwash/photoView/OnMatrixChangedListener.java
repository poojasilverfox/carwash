package com.silverfox.carwash.photoView;

import android.graphics.RectF;

public interface OnMatrixChangedListener {
    void onMatrixChanged(RectF rect);
}
