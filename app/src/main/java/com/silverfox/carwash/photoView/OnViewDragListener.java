package com.silverfox.carwash.photoView;

public interface OnViewDragListener {
    void onDrag(float dx, float dy);
}
