package com.silverfox.carwash.photoView;

import android.view.View;

public interface OnViewTapListener {
    void onViewTap(View view, float x, float y);
}
