package com.silverfox.carwash.photoView;

import android.widget.ImageView;

public interface OnPhotoTapListener {
    void onPhotoTap(ImageView view, float x, float y);
}
