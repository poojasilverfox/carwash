package com.silverfox.carwash.photoView;

public interface OnScaleChangedListener {
    void onScaleChange(float scaleFactor, float focusX, float focusY);

}
