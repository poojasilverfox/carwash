package com.silverfox.carwash.Api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    static Gson gson = new GsonBuilder().setLenient().create();
    public static final String BASE_URL = "https://yourhomewash.com/api/v1/";
//    public static final String BASE_URL = "https://yourhomewash.com/vb/yourhomewash/api/v1/";
    private static Retrofit retrofit = null;

    public static Retrofit getClientResponse() {

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor("admin", "admin"))
                .build();

        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }



}
