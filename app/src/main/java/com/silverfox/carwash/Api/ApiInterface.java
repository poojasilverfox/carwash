package com.silverfox.carwash.Api;


import com.silverfox.carwash.Model.CancelPlanResponse;
import com.silverfox.carwash.Model.CarMakeResponse;
import com.silverfox.carwash.Model.CarModelResponse;
import com.silverfox.carwash.Model.CarSizeResponse;
import com.silverfox.carwash.Model.CarWashListResponse;
import com.silverfox.carwash.Model.CardUpdateResponse;
import com.silverfox.carwash.Model.FaceBookLoginResponse;
import com.silverfox.carwash.Model.ForgotResponse;
import com.silverfox.carwash.Model.FreezeResponse;
import com.silverfox.carwash.Model.LogOutResponse;
import com.silverfox.carwash.Model.LoginResponse;
import com.silverfox.carwash.Model.MailInvoiceResponse;
import com.silverfox.carwash.Model.MembershipListResponse;
import com.silverfox.carwash.Model.MembershipTListResponse;
import com.silverfox.carwash.Model.PaymentResponse;
import com.silverfox.carwash.Model.PrintInvoiceResponse;
import com.silverfox.carwash.Model.PromocodeResponse;
import com.silverfox.carwash.Model.PublicKeyResponse;
import com.silverfox.carwash.Model.ReactivePlanResponse;
import com.silverfox.carwash.Model.ReedemResponse;
import com.silverfox.carwash.Model.RegisterResponse;
import com.silverfox.carwash.Model.RepaymentPlanResponse;
import com.silverfox.carwash.Model.UpdateProfileResponse;
import com.silverfox.carwash.Model.UpdateResponse;
import com.silverfox.carwash.Model.WalletTransactionResponse;
import com.silverfox.carwash.Model.WalletUpdateResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {


    @FormUrlEncoded
    @POST("facebook_login")
    Call<FaceBookLoginResponse> getFaceBookData(@Field("name") String name, @Field("email") String email, @Field("facebook_id") String facebook_id, @Field("mobile_code") String mobile_code, @Field("mobile") String mobile);

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> getLoginData(@Field("password") String password, @Field("mobile") String mobile, @Field("device_type") String device_type, @Field("device_token") String device_token);

    @FormUrlEncoded
    @POST("registration")
    Call<RegisterResponse> getRegistrationData(@Field("name") String name,
                                               @Field("last_name") String last_name,
                                               @Field("password") String password,
                                               @Field("reffer_by") String reffer_by,
                                               @Field("mobile_code") String mobile_code,
                                               @Field("mobile") String mobile);

    @FormUrlEncoded
    @POST("carwash_list")
    Call<CarWashListResponse> getCarWashList(@Field("api_token") String api_token, @Field("search") String search, @Field("page") String page);

    @FormUrlEncoded
    @POST("update_card_detail")
    Call<CardUpdateResponse> getUpdateCardData(@Field("api_token") String api_token, @Field("card_number") String card_number, @Field("expiry_year") String expiry_year, @Field("expiry_month") String expiry_month, @Field("cvv_code") String cvv_code);


    @FormUrlEncoded
    @POST("logout")
    Call<LogOutResponse> getLogoutData(@Field("api_token") String api_token);

    @FormUrlEncoded
    @POST("reactive_plan")
    Call<ReactivePlanResponse> getReactivePlan(@Field("api_token") String api_token, @Field("membership_id") String membership_id);

    @FormUrlEncoded
    @POST("repayment_plan")
    Call<RepaymentPlanResponse> getRepaymentPlan(@Field("api_token") String api_token, @Field("membership_id") String membership_id);

    @FormUrlEncoded
    @POST("update_note")
    Call<UpdateResponse> getUpdateNoteData(@Field("api_token") String api_token, @Field("membership_id") String membership_id, @Field("note") String note);

    @FormUrlEncoded
    @POST("apply_redeem_code")
    Call<ReedemResponse> getRedeemData(@Field("api_token") String api_token, @Field("code") String code);

    @FormUrlEncoded
    @POST("forgot/password")
    Call<ForgotResponse> getForgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("update_profile")
    Call<UpdateProfileResponse> getUpdateProfile(@Field("api_token") String api_token, @Field("email") String email, @Field("name") String name, @Field("last_name") String last_name);

    @FormUrlEncoded
    @POST("update_profile")
    Call<UpdateProfileResponse> getChangePassword(@Field("api_token") String api_token, @Field("email") String email, @Field("name") String name);

    @FormUrlEncoded
    @POST("wallet_transaction")
    Call<WalletTransactionResponse> getWalletTData(@Field("api_token") String api_token, @Field("page") int page);

    @FormUrlEncoded
    @POST("wallet_updated")
    Call<WalletUpdateResponse> getWalletData(@Field("api_token") String api_token);

    @FormUrlEncoded
    @POST("mail_invoice")
    Call<MailInvoiceResponse> getMailinvoice(@Field("api_token") String api_token, @Field("id") String id);

    @FormUrlEncoded
    @POST("print_invoice")
    Call<PrintInvoiceResponse> getPrintinvoice(@Field("api_token") String api_token, @Field("id") String id);

    @FormUrlEncoded
    @POST("membership_list")
    Call<MembershipListResponse> getMemberShipList(@Field("api_token") String api_token, @Field("page") String page);

    @FormUrlEncoded
    @POST("membership_transaction")
    Call<MembershipTListResponse> getMemberShipTList(@Field("api_token") String api_token, @Field("page") String page);

    @FormUrlEncoded
    @POST("purchase_membership")
    Call<PaymentResponse> getPaymentData(@Field("api_token") String api_token,
                                         @Field("token_value") String token_value,
                                         @Field("car_color") String car_color,
                                         @Field("carwash_owner_id") String carwash_owner_id,
                                         @Field("carwash_price_id") String carwash_price_id,
                                         @Field("promo_code") String promo_code,
                                         @Field("price") String price,
                                         @Field("car_make") String car_make,
                                         @Field("model") String model,
                                         @Field("car_type") String car_type,
                                         @Field("licence_no") String licence_no,
                                         @Field("extra") String extra,
                                         @Field("card_number") String card_number,
                                         @Field("expiry_month") String expiry_month,
                                         @Field("expiry_year") String expiry_year,
                                         @Field("cvv_code") String cvv_code
    );

    @FormUrlEncoded
    @POST("apply_promocode")
    Call<PromocodeResponse> getPromocodeData(@Field("api_token") String api_token, @Field("code") String code, @Field("owner_id") String owner_id);

    @FormUrlEncoded
    @POST("print_invoice")
    Call<PrintInvoiceResponse> getPrintData(@Field("api_token") String api_token, @Field("id") int code);

    @FormUrlEncoded
    @POST("get_public_key")
    Call<PublicKeyResponse> get_public_key(@Field("api_token") String api_token, @Field("carwash_owner_id") String carwash_owner_id);

    @GET("car_make_list")
    Call<CarMakeResponse> getCarMakeData();

    @FormUrlEncoded
    @POST("car_model_list")
    Call<CarModelResponse> getCarModelData(@Field("make_id") String make_id);

    @FormUrlEncoded
    @POST("car_type_list")
    Call<CarSizeResponse> getTypeData(@Field("api_token") String api_token, @Field("carwash_owner_id") String carwash_owner_id);

    @FormUrlEncoded
    @POST("plan_cancel")
    Call<CancelPlanResponse> getCancelPlanData(@Field("api_token") String api_token, @Field("membership_id") String membership_id, @Field("val") String val);

    @FormUrlEncoded
    @POST("plan_freeze")
    Call<FreezeResponse> getFreezeData(@Field("api_token") String api_token, @Field("membership_id") String membership_id, @Field("date") String date);


}
