package com.silverfox.carwash;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.multidex.MultiDex;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.firebase.FirebaseApp;
import com.silverfox.carwash.service.LruBitmapCache;


public class Application extends android.app.Application {
	private RequestQueue mRequestQueue;

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		}
		return mRequestQueue;
	}


	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	public  static Application application;


	public static final String TAG = Application.class.getSimpleName();


	private static Application mInstance;


	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}

	public Application() {
		application = this;
	}



	@Override
	public void onTerminate() {
		super.onTerminate();
	}



	@Override
	public void onCreate() {
		super.onCreate();

		mInstance=this;
		FirebaseApp.initializeApp(this);
	}


	public static synchronized Application getInstance() {
		return mInstance;
	}

	public static Application getApp() {
		if (application == null) {
			application = new Application();
		}
		return application;
	}




}
