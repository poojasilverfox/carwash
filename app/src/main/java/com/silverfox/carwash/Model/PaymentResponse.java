package com.silverfox.carwash.Model;

public class PaymentResponse {


    /**
     * status : 0
     * message : Transaction rejected because the lookup on the supplied token failed.
     */

    private int status;
    private String message;
    public DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataBean{
        private String wallet;

        public String getWallet() {
            return wallet;
        }

        public void setWallet(String wallet) {
            this.wallet = wallet;
        }
    }
}
