package com.silverfox.carwash.Model;

import java.util.List;

public class CarWashListResponse {


    /**
     * status : 1
     * data : [{"carwash_id":4,"logo":"https://silverfoxstudio.in/car/public/images/owners/15752871795de4f98b480ec.jpg","owner_name":"HELLO CARWASH","email":"PHP@GMAIL.COM","mobile_code":"213","mobile":"7894561230","shop_name":"HELLO CARWASH","working_days":"MONDAY TO FRIDAY ON 7:00AM TO 6:00PM","full_address":"BAPA SITARAM CHOWK, KATARGAM, SURAT-395004","lat":21,"lng":72,"city":"SURAT, INDIA","distance":0,"plan_details":[{"plan_id":3,"name":"Ultimate Wash","details":"Simoniz Hot Wax & Shine, Simoniz Tire Shine, Simoniz Triple Foam Polish, Side Blasters, Bug Buster, Free Air Freshener, Underbody Blast, Tire & Wheel Cleaner, Presoak, Friction Wash, High Pressure Rinse, Spot Free Rinse, Turbo Dry, Free Vacuum Token","type":0,"price":"25.00"},{"plan_id":4,"name":"Supreme Wash","details":"Simoniz Triple Foam Polish, Side Blasters, Bug Buster, Free Air Freshener, Underbody Blast, Tire & Wheel Cleaner, Presoak, Friction Wash, High Pressure Rinse, Spot Free Rinse, Turbo Dry, Free Vacuum Token","type":0,"price":"22.00"},{"plan_id":5,"name":"Deluxe Wash","details":"Underbody Blast, Tire & Wheel Cleaner, Presoak, Friction Wash, High Pressure Rinse, Spot Free Rinse, Turbo Dry, Free Vacuum Token","type":0,"price":"19.00"}]},{"carwash_id":5,"logo":"https://silverfoxstudio.in/car/public/images/owners/15752882695de4fdcdd43f3.jpg","owner_name":"Bubble Wash, KS","email":"test@gmail.com","mobile_code":"1441","mobile":"9897855745","shop_name":"Bubble Wash, KS","working_days":"Mon-Sat: 8am-5pm; Sunday 10am-5:30pm","full_address":"Khattalwada, Wadgaon, Maharashtra 442305,India","lat":20,"lng":78,"city":"surat","distance":0,"plan_details":[{"plan_id":6,"name":"Master wash","details":"Simoniz Triple Foam Polish, Side Blasters, Bug Buster, Free Air Freshener, Underbody Blast, Tire & Wheel Cleaner, Presoak, Friction Wash, High Pressure Rinse, Spot Free Rinse, Turbo Dry, Free Vacuum Token","type":0,"price":"251.00"},{"plan_id":7,"name":"Live Wash","details":"Simoniz Triple Foam Polish, Side Blasters, Bug Buster, Free Air Freshener, Underbody Blast, Tire & Wheel Cleaner, Presoak, Friction Wash, High Pressure Rinse, Spot Free Rinse, Turbo Dry, Free Vacuum Token","type":0,"price":"122.00"},{"plan_id":8,"name":"Urgent wash","details":"Simoniz Triple Foam Polish, Side Blasters, Bug Buster, Free Air Freshener, Underbody Blast, Tire & Wheel Cleaner, Presoak, Friction Wash, High Pressure Rinse, Spot Free Rinse, Turbo Dry, Free Vacuum Token","type":0,"price":"520.00"}]}]
     * message : list successfully.
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * carwash_id : 4
         * logo : https://silverfoxstudio.in/car/public/images/owners/15752871795de4f98b480ec.jpg
         * owner_name : HELLO CARWASH
         * email : PHP@GMAIL.COM
         * mobile_code : 213
         * mobile : 7894561230
         * shop_name : HELLO CARWASH
         * working_days : MONDAY TO FRIDAY ON 7:00AM TO 6:00PM
         * full_address : BAPA SITARAM CHOWK, KATARGAM, SURAT-395004
         * lat : 21
         * lng : 72
         * city : SURAT, INDIA
         * distance : 0
         * plan_details : [{"plan_id":3,"name":"Ultimate Wash","details":"Simoniz Hot Wax & Shine, Simoniz Tire Shine, Simoniz Triple Foam Polish, Side Blasters, Bug Buster, Free Air Freshener, Underbody Blast, Tire & Wheel Cleaner, Presoak, Friction Wash, High Pressure Rinse, Spot Free Rinse, Turbo Dry, Free Vacuum Token","type":0,"price":"25.00"},{"plan_id":4,"name":"Supreme Wash","details":"Simoniz Triple Foam Polish, Side Blasters, Bug Buster, Free Air Freshener, Underbody Blast, Tire & Wheel Cleaner, Presoak, Friction Wash, High Pressure Rinse, Spot Free Rinse, Turbo Dry, Free Vacuum Token","type":0,"price":"22.00"},{"plan_id":5,"name":"Deluxe Wash","details":"Underbody Blast, Tire & Wheel Cleaner, Presoak, Friction Wash, High Pressure Rinse, Spot Free Rinse, Turbo Dry, Free Vacuum Token","type":0,"price":"19.00"}]
         */

        private int carwash_id;
        private String logo;
        private String owner_name;
        private String email;
        private String mobile_code;
        private String mobile;
        private String shop_name;
        private String working_days;
        private String full_address;
        private float lat;
        private float lng;
        private String city;
        private int distance;
        private List<PlanDetailsBean> plan_details;

        public int getCarwash_id() {
            return carwash_id;
        }

        public void setCarwash_id(int carwash_id) {
            this.carwash_id = carwash_id;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getOwner_name() {
            return owner_name;
        }

        public void setOwner_name(String owner_name) {
            this.owner_name = owner_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile_code() {
            return mobile_code;
        }

        public void setMobile_code(String mobile_code) {
            this.mobile_code = mobile_code;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public String getWorking_days() {
            return working_days;
        }

        public void setWorking_days(String working_days) {
            this.working_days = working_days;
        }

        public String getFull_address() {
            return full_address;
        }

        public void setFull_address(String full_address) {
            this.full_address = full_address;
        }

        public float getLat() {
            return lat;
        }

        public void setLat(float lat) {
            this.lat = lat;
        }

        public float getLng() {
            return lng;
        }

        public void setLng(float lng) {
            this.lng = lng;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }

        public List<PlanDetailsBean> getPlan_details() {
            return plan_details;
        }

        public void setPlan_details(List<PlanDetailsBean> plan_details) {
            this.plan_details = plan_details;
        }

        public static class PlanDetailsBean {
            /**
             * plan_id : 3
             * name : Ultimate Wash
             * details : Simoniz Hot Wax & Shine, Simoniz Tire Shine, Simoniz Triple Foam Polish, Side Blasters, Bug Buster, Free Air Freshener, Underbody Blast, Tire & Wheel Cleaner, Presoak, Friction Wash, High Pressure Rinse, Spot Free Rinse, Turbo Dry, Free Vacuum Token
             * type : 0
             * extra_plan : ""
             * price : 25.00
             */

            private int plan_id;
            private String name;
            private String details;
            private String name_note;
            private String extra_plan;
            private String color_code;
            private String color_code_second;
            private int type;
            private float price;
            private List<ExtraPlanDEtail> extra_plan_details;


            public String getName_note() {
                return name_note;
            }

            public void setName_note(String name_note) {
                this.name_note = name_note;
            }

            public String getExtra_plan() {
                return extra_plan;
            }

            public void setExtra_plan(String extra_plan) {
                this.extra_plan = extra_plan;
            }

            public int getPlan_id() {
                return plan_id;
            }

            public void setPlan_id(int plan_id) {
                this.plan_id = plan_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDetails() {
                return details;
            }

            public void setDetails(String details) {
                this.details = details;
            }

            public int getType() {
                return type;
            }

            public String getColor_code() {
                return color_code;
            }

            public String getColor_code_second() {
                return color_code_second;
            }


            public List<ExtraPlanDEtail> getExtra_plan_details() {
                return extra_plan_details;
            }

            public void setExtra_plan_details(List<ExtraPlanDEtail> extra_plan_details) {
                this.extra_plan_details = extra_plan_details;
            }

            public void setColor_code_second(String color_code_second) {
                this.color_code_second = color_code_second;
            }

            public void setColor_code(String color_code) {
                this.color_code = color_code;
            }

            public void setType(int type) {
                this.type = type;
            }

            public float getPrice() {
                return price;
            }

            public void setPrice(float price) {
                this.price = price;
            }
        }
    }
}
