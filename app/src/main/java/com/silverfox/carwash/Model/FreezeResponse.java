package com.silverfox.carwash.Model;

public class FreezeResponse {


    /**
     * status : 1
     * data : {"is_freeze":1,"freeze_date":"2020-12-12"}
     * message : Freeze Applied successfully.
     */

    private int status;
    private DataBean data;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataBean {
        /**
         * is_freeze : 1
         * freeze_date : 2020-12-12
         */

        private int is_freeze;
        private String freeze_date;

        public int getIs_freeze() {
            return is_freeze;
        }

        public void setIs_freeze(int is_freeze) {
            this.is_freeze = is_freeze;
        }

        public String getFreeze_date() {
            return freeze_date;
        }

        public void setFreeze_date(String freeze_date) {
            this.freeze_date = freeze_date;
        }
    }
}
