package com.silverfox.carwash.Model;

import java.util.List;

public class WalletTransactionResponse {


    /**
     * status : 1
     * data : [{"amount":"10.00","type":"Debit","redeem_code":"","created_at":"2019-12-12"},{"amount":"10.00","type":"Credit","redeem_code":"ADD10","created_at":"2019-12-12"}]
     * message : list successfully.
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * amount : 10.00
         * type : Debit
         * redeem_code :
         * created_at : 2019-12-12
         */

        private String amount;
        private String type;
        private String redeem_code;
        private String created_at;

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getRedeem_code() {
            return redeem_code;
        }

        public void setRedeem_code(String redeem_code) {
            this.redeem_code = redeem_code;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }
    }
}
