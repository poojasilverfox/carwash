package com.silverfox.carwash.Model;

import java.util.List;

public class CarSizeResponse {


    /**
     * status : 1
     * normal_type_data : [{"name":"Sedan Car","amount":"0.00"},{"name":"Crossover/Small SUV","amount":"0.00"},{"name":"SUV/Small Truck","amount":"0.00"},{"name":"Large SUV/Van/Medium Truck","amount":"0.00"},{"name":"Large Truck","amount":"0.00"},{"name":"Oversized Vehicles","amount":"0.00"}]
     * inside_tunnel_data : [{"name":"Sedan Car","amount":"0.00"},{"name":"Crossover/Small SUV","amount":"3.00"},{"name":"SUV/Small Truck","amount":"5.00"},{"name":"Large SUV/Van/Medium Truck","amount":"7.00"},{"name":"Large Truck","amount":"10.00"},{"name":"Oversized Vehicles","amount":"12.00"}]
     * outside_tunnel_data : [{"name":"Sedan Car","amount":"0.00"},{"name":"Crossover/Small SUV","amount":"10.00"},{"name":"SUV/Small Truck","amount":"20.00"},{"name":"Large SUV/Van/Medium Truck","amount":"30.00"},{"name":"Large Truck","amount":"40.00"},{"name":"Oversized Vehicles","amount":"50.00"}]
     * message : Data Load successfully.
     */

    private int status;
    private String message;
    private List<NormalTypeDataBean> normal_type_data;
    private List<InsideTunnelDataBean> inside_tunnel_data;
    private List<OutsideTunnelDataBean> outside_tunnel_data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NormalTypeDataBean> getNormal_type_data() {
        return normal_type_data;
    }

    public void setNormal_type_data(List<NormalTypeDataBean> normal_type_data) {
        this.normal_type_data = normal_type_data;
    }

    public List<InsideTunnelDataBean> getInside_tunnel_data() {
        return inside_tunnel_data;
    }

    public void setInside_tunnel_data(List<InsideTunnelDataBean> inside_tunnel_data) {
        this.inside_tunnel_data = inside_tunnel_data;
    }

    public List<OutsideTunnelDataBean> getOutside_tunnel_data() {
        return outside_tunnel_data;
    }

    public void setOutside_tunnel_data(List<OutsideTunnelDataBean> outside_tunnel_data) {
        this.outside_tunnel_data = outside_tunnel_data;
    }

    public static class NormalTypeDataBean {
        /**
         * name : Sedan Car
         * amount : 0.00
         */

        private String name;
        private String amount;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }
    }

    public static class InsideTunnelDataBean {
        /**
         * name : Sedan Car
         * amount : 0.00
         */

        private String name;
        private String amount;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }
    }

    public static class OutsideTunnelDataBean {
        /**
         * name : Sedan Car
         * amount : 0.00
         */

        private String name;
        private String amount;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }
    }
}
