package com.silverfox.carwash.Model;

public class RepaymentPlanResponse {


    /**
     * status : 0
     * message : Not activated Plan now.
     */

    private int status;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
