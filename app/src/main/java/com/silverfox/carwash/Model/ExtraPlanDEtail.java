package com.silverfox.carwash.Model;

public class ExtraPlanDEtail {


    /**
     * extra_plan_id : 1
     * name : Tire Dressing, Air Freshener, & Sealer Wax
     * amount : 5.00
     */

    private int extra_plan_id;
    private String name;
    private String amount;

    public int getExtra_plan_id() {
        return extra_plan_id;
    }

    public void setExtra_plan_id(int extra_plan_id) {
        this.extra_plan_id = extra_plan_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
