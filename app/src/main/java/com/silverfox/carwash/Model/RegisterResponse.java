package com.silverfox.carwash.Model;

public class  RegisterResponse {


    /**
     * status : 1
     * data : {"user_id":111,"name":"Pankaj","email":"","mobile_code":"91","mobile":"9638527417","otp":"4750","wallet":0,"reffer_code":"5074111","reffer_by":null,"card_number":"","expiry_month":"","expiry_year":"","cvv_code":"","api_token":"blhUMHJCbnV5NVhTQXFCVnl0SDBTaGJXRVJKeDZVWFU0Z1pwWktIRWZWSjRDVGxZeUt5YWFRYTBLU1hI5def8b81871f1","created_at":{"date":"2019-12-10 12:11:45.000000","timezone_type":3,"timezone":"UTC"}}
     * message : Registration successfully.
     */

    private int status;
    private DataBean data;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataBean {
        /**
         * user_id : 111
         * name : Pankaj
         * email :
         * mobile_code : 91
         * mobile : 9638527417
         * otp : 4750
         * wallet : 0
         * reffer_code : 5074111
         * reffer_by : null
         * card_number :
         * expiry_month :
         * expiry_year :
         * cvv_code :
         * api_token : blhUMHJCbnV5NVhTQXFCVnl0SDBTaGJXRVJKeDZVWFU0Z1pwWktIRWZWSjRDVGxZeUt5YWFRYTBLU1hI5def8b81871f1
         * created_at : {"date":"2019-12-10 12:11:45.000000","timezone_type":3,"timezone":"UTC"}
         */

        private int user_id;
        private String name;
        private String customer_id;
        private String email;
        private String mobile_code;
        private String mobile;
        private String otp;
        private int wallet;
        private String reffer_code;
        private Object reffer_by;
        private String card_number;
        private String expiry_month;
        private String expiry_year;
        private String cvv_code;
        private String api_token;
        private CreatedAtBean created_at;

        public String getCustomer_id() {
            return customer_id;
        }

        public void setCustomer_id(String customer_id) {
            this.customer_id = customer_id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile_code() {
            return mobile_code;
        }

        public void setMobile_code(String mobile_code) {
            this.mobile_code = mobile_code;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public int getWallet() {
            return wallet;
        }

        public void setWallet(int wallet) {
            this.wallet = wallet;
        }

        public String getReffer_code() {
            return reffer_code;
        }

        public void setReffer_code(String reffer_code) {
            this.reffer_code = reffer_code;
        }

        public Object getReffer_by() {
            return reffer_by;
        }

        public void setReffer_by(Object reffer_by) {
            this.reffer_by = reffer_by;
        }

        public String getCard_number() {
            return card_number;
        }

        public void setCard_number(String card_number) {
            this.card_number = card_number;
        }

        public String getExpiry_month() {
            return expiry_month;
        }

        public void setExpiry_month(String expiry_month) {
            this.expiry_month = expiry_month;
        }

        public String getExpiry_year() {
            return expiry_year;
        }

        public void setExpiry_year(String expiry_year) {
            this.expiry_year = expiry_year;
        }

        public String getCvv_code() {
            return cvv_code;
        }

        public void setCvv_code(String cvv_code) {
            this.cvv_code = cvv_code;
        }

        public String getApi_token() {
            return api_token;
        }

        public void setApi_token(String api_token) {
            this.api_token = api_token;
        }

        public CreatedAtBean getCreated_at() {
            return created_at;
        }

        public void setCreated_at(CreatedAtBean created_at) {
            this.created_at = created_at;
        }

        public static class CreatedAtBean {
            /**
             * date : 2019-12-10 12:11:45.000000
             * timezone_type : 3
             * timezone : UTC
             */

            private String date;
            private int timezone_type;
            private String timezone;

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public int getTimezone_type() {
                return timezone_type;
            }

            public void setTimezone_type(int timezone_type) {
                this.timezone_type = timezone_type;
            }

            public String getTimezone() {
                return timezone;
            }

            public void setTimezone(String timezone) {
                this.timezone = timezone;
            }
        }
    }
}