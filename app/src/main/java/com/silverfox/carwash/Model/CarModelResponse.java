package com.silverfox.carwash.Model;

import java.util.List;

public class CarModelResponse {


    /**
     * status : 1
     * data : [{"id":1,"make_id":1,"code":"CL_MODELS","title":"CL Models (4)"},{"id":2,"make_id":1,"code":"2.2CL","title":" - 2.2CL"},{"id":3,"make_id":1,"code":"2.3CL","title":" - 2.3CL"},{"id":4,"make_id":1,"code":"3.0CL","title":" - 3.0CL"},{"id":5,"make_id":1,"code":"3.2CL","title":" - 3.2CL"},{"id":6,"make_id":1,"code":"ILX","title":"ILX"},{"id":7,"make_id":1,"code":"INTEG","title":"Integra"},{"id":8,"make_id":1,"code":"LEGEND","title":"Legend"},{"id":9,"make_id":1,"code":"MDX","title":"MDX"},{"id":10,"make_id":1,"code":"NSX","title":"NSX"},{"id":11,"make_id":1,"code":"RDX","title":"RDX"},{"id":12,"make_id":1,"code":"RL_MODELS","title":"RL Models (2)"},{"id":13,"make_id":1,"code":"3.5RL","title":" - 3.5 RL"},{"id":14,"make_id":1,"code":"RL","title":" - RL"},{"id":15,"make_id":1,"code":"RSX","title":"RSX"},{"id":16,"make_id":1,"code":"SLX","title":"SLX"},{"id":17,"make_id":1,"code":"TL_MODELS","title":"TL Models (3)"},{"id":18,"make_id":1,"code":"2.5TL","title":" - 2.5TL"},{"id":19,"make_id":1,"code":"3.2TL","title":" - 3.2TL"},{"id":20,"make_id":1,"code":"TL","title":" - TL"},{"id":21,"make_id":1,"code":"TSX","title":"TSX"},{"id":22,"make_id":1,"code":"VIGOR","title":"Vigor"},{"id":23,"make_id":1,"code":"ZDX","title":"ZDX"},{"id":24,"make_id":1,"code":"ACUOTH","title":"Other Acura Models"}]
     * message : List Load successfully.
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * make_id : 1
         * code : CL_MODELS
         * title : CL Models (4)
         */

        private int id;
        private int make_id;
        private String code;
        private String title;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getMake_id() {
            return make_id;
        }

        public void setMake_id(int make_id) {
            this.make_id = make_id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
