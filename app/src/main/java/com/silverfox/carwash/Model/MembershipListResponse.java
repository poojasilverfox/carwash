package com.silverfox.carwash.Model;

import java.util.List;

public class MembershipListResponse {

    /**
     * status : 1
     * data : [{"membership_id":186,"carwash_owner_id":"18","carwash_price_id":"45","location":"5893 W Tropicana Ave\r\nLas Vegas, NV 89103","wash_plan":"EXTERIOR WASH","qr_code":"http://yourhomewash.com/public/web/triple.jpg","promo_code":"","first_month_price":"0.01","is_freeze":"0","is_cancel":"0","cancel_date":"","freeze_date":"","inactive_plan":"0","deactive_plan":"0","is_failed":0,"pay_trough_date":"2020-08-25","next_transaction_date":"2020-08-26","note":"","offer_days":"You may wash your Vehicle Sunday-Thursday Through Thursday","show_freeze":1,"show_cancel":1,"plan_details":{"plan_id":45,"name":"EXTERIOR WASH","type":"0","discount":"","extra_plan":"","details":"Exterior Car Wash, Spot-Free Rinse, & Clean Outside Windows","price":"19.99"},"carwash_owner_details":{"carwash_id":18,"logo":"http://yourhomewash.com/public/images/owners/15760393005df07384e7b9a.PNG","owner_name":"Jose Canseco Showtime Car Wash","email":"showtimecarwashcustomer@gmail.com","mobile_code":"1","mobile":"453453434534","shop_name":"Jose Canseco Showtime Car Wash","working_days":"Sunday-Thursday","to_working_day":"Thursday","full_address":"5893 W Tropicana Ave\r\nLas Vegas, NV 89103","lat":36.1104125,"lng":-115.20670110000003,"city":"Las Vegas","distance":0},"car_details":{"car_id":184,"car_make":"Bentley","modal":"Brooklands","car_type":"Sedan Car","car_color":"Blue","licence_no":""},"freeze_select_dates":["2020-05-27","2020-06-27","2020-07-27","2020-08-27"],"created_at":{"date":"2020-04-26 21:52:18.000000","timezone_type":3,"timezone":"America/Los_Angeles"}},{"membership_id":185,"carwash_owner_id":"18","carwash_price_id":"45","location":"5893 W Tropicana Ave\r\nLas Vegas, NV 89103","wash_plan":"EXTERIOR WASH","qr_code":"http://yourhomewash.com/public/web/triple.jpg","promo_code":"","first_month_price":"0.01","is_freeze":"0","is_cancel":"0","cancel_date":"","freeze_date":"","inactive_plan":"0","deactive_plan":"0","is_failed":0,"pay_trough_date":"2020-05-25","next_transaction_date":"2020-05-26","note":"","offer_days":"You may wash your Vehicle Sunday-Thursday Through Thursday","show_freeze":1,"show_cancel":1,"plan_details":{"plan_id":45,"name":"EXTERIOR WASH","type":"0","discount":"","extra_plan":"","details":"Exterior Car Wash, Spot-Free Rinse, & Clean Outside Windows","price":"19.99"},"carwash_owner_details":{"carwash_id":18,"logo":"http://yourhomewash.com/public/images/owners/15760393005df07384e7b9a.PNG","owner_name":"Jose Canseco Showtime Car Wash","email":"showtimecarwashcustomer@gmail.com","mobile_code":"1","mobile":"453453434534","shop_name":"Jose Canseco Showtime Car Wash","working_days":"Sunday-Thursday","to_working_day":"Thursday","full_address":"5893 W Tropicana Ave\r\nLas Vegas, NV 89103","lat":36.1104125,"lng":-115.20670110000003,"city":"Las Vegas","distance":0},"car_details":{"car_id":183,"car_make":"Aston Martin","modal":"DBS","car_type":"Sedan Car","car_color":null,"licence_no":""},"freeze_select_dates":["2020-05-26","2020-06-26","2020-07-26","2020-08-26"],"created_at":{"date":"2020-04-26 21:42:04.000000","timezone_type":3,"timezone":"America/Los_Angeles"}},{"membership_id":171,"carwash_owner_id":"20","carwash_price_id":"39","location":"5893 W Tropicana Ave\r\nLas Vegas, NV 89103","wash_plan":"US Gas General Member","qr_code":"http://yourhomewash.com/public/web/triple.jpg","promo_code":"","first_month_price":"0.01","is_freeze":"0","is_cancel":"0","cancel_date":"","freeze_date":"","inactive_plan":"0","deactive_plan":"0","is_failed":0,"pay_trough_date":"2020-05-23","next_transaction_date":"2020-05-24","note":"","offer_days":"You may wash your Vehicle 7 days a week.","show_freeze":1,"show_cancel":1,"plan_details":{"plan_id":39,"name":"US Gas General Member","type":"2","discount":"","extra_plan":"","details":"20 Cents Off A Gallon (2 Locations)<\/br>\r\n20% Off any Car Wash at Showtime Car Wash (If Best Discount)<\/br>\r\n$10 Off Double & Up Car Wash Package (If Best Discount)<\/br>\r\n20% Off Detailing Services <\/br>\r\n20% Off Windshield Repair<\/br>\r\n20% Off DMV Services <\/br>\r\n20% Off Ceramic Coating<\/br>\r\n15% Off in Convenience Store\r\n\r\nYou May Use 7 Days A Week","price":"19.99"},"carwash_owner_details":{"carwash_id":20,"logo":"http://yourhomewash.com/public/images/owners/15783907685e1454f08bf7d.PNG","owner_name":"US Gas (Tropicana & Jones)","email":"zach@gmail.com","mobile_code":"1","mobile":"7678678678","shop_name":"US Gas (Tropicana & Jones)","working_days":"Monday","to_working_day":"Sunday","full_address":"5893 W Tropicana Ave\r\nLas Vegas, NV 89103","lat":36.0994985,"lng":-115.2222314,"city":"Las Vegas","distance":0},"car_details":{"car_id":169,"car_make":"Audi","modal":"200","car_type":"Sedan Car","car_color":null,"licence_no":"fdfd"},"freeze_select_dates":["2020-05-24","2020-06-24","2020-07-24","2020-08-24"],"created_at":{"date":"2020-04-24 07:25:22.000000","timezone_type":3,"timezone":"America/Los_Angeles"}},{"membership_id":170,"carwash_owner_id":"20","carwash_price_id":"39","location":"5893 W Tropicana Ave\r\nLas Vegas, NV 89103","wash_plan":"US Gas General Member","qr_code":"http://yourhomewash.com/public/web/triple.jpg","promo_code":"","first_month_price":"0.01","is_freeze":"0","is_cancel":"0","cancel_date":"","freeze_date":"","inactive_plan":"0","deactive_plan":"0","is_failed":0,"pay_trough_date":"2020-05-23","next_transaction_date":"2020-05-24","note":"","offer_days":"You may wash your Vehicle 7 days a week.","show_freeze":1,"show_cancel":1,"plan_details":{"plan_id":39,"name":"US Gas General Member","type":"2","discount":"","extra_plan":"","details":"20 Cents Off A Gallon (2 Locations)<\/br>\r\n20% Off any Car Wash at Showtime Car Wash (If Best Discount)<\/br>\r\n$10 Off Double & Up Car Wash Package (If Best Discount)<\/br>\r\n20% Off Detailing Services <\/br>\r\n20% Off Windshield Repair<\/br>\r\n20% Off DMV Services <\/br>\r\n20% Off Ceramic Coating<\/br>\r\n15% Off in Convenience Store\r\n\r\nYou May Use 7 Days A Week","price":"19.99"},"carwash_owner_details":{"carwash_id":20,"logo":"http://yourhomewash.com/public/images/owners/15783907685e1454f08bf7d.PNG","owner_name":"US Gas (Tropicana & Jones)","email":"zach@gmail.com","mobile_code":"1","mobile":"7678678678","shop_name":"US Gas (Tropicana & Jones)","working_days":"Monday","to_working_day":"Sunday","full_address":"5893 W Tropicana Ave\r\nLas Vegas, NV 89103","lat":36.0994985,"lng":-115.2222314,"city":"Las Vegas","distance":0},"car_details":{"car_id":168,"car_make":"4","modal":"DB9","car_type":"Sedan Car","car_color":null,"licence_no":""},"freeze_select_dates":["2020-05-24","2020-06-24","2020-07-24","2020-08-24"],"created_at":{"date":"2020-04-24 04:46:41.000000","timezone_type":3,"timezone":"America/Los_Angeles"}},{"membership_id":157,"carwash_owner_id":"21","carwash_price_id":"40","location":"6100 W. Charleston Blvd\r\nLas Vegas, NV 89146","wash_plan":"US Gas Captain Member","qr_code":"http://yourhomewash.com/public/web/triple.jpg","promo_code":"","first_month_price":"0.01","is_freeze":"0","is_cancel":"0","cancel_date":"","freeze_date":"","inactive_plan":"0","deactive_plan":"0","is_failed":0,"pay_trough_date":"2020-05-14","next_transaction_date":"2020-05-15","note":"","offer_days":"You may wash your Vehicle 7 days a week.","show_freeze":1,"show_cancel":1,"plan_details":{"plan_id":40,"name":"US Gas Captain Member","type":"2","discount":"","extra_plan":"","details":"10 Cents Off A Gallon<\/br> \r\n10% Off Any Car Wash At Showtime Car Wash\r\n\r\nYou May Use 7 Days A Week","price":"4.99"},"carwash_owner_details":{"carwash_id":21,"logo":"http://yourhomewash.com/public/images/owners/15783897745e14510eeb12f.PNG","owner_name":"US Gas (Charleston & Jones)","email":"zach@gmail.com","mobile_code":"1","mobile":"7678678678","shop_name":"US Gas (Charleston & Jones)","working_days":"Monday","to_working_day":"Sunday","full_address":"6100 W. Charleston Blvd\r\nLas Vegas, NV 89146","lat":36.1595149,"lng":-115.22443900000002,"city":"Las Vegas","distance":0},"car_details":{"car_id":155,"car_make":"3","modal":"Eagle","car_type":"Sedan Car","car_color":null,"licence_no":""},"freeze_select_dates":["2020-05-15","2020-06-15","2020-07-15","2020-08-15"],"created_at":{"date":"2020-04-15 06:57:11.000000","timezone_type":3,"timezone":"America/Los_Angeles"}},{"membership_id":156,"carwash_owner_id":"21","carwash_price_id":"40","location":"6100 W. Charleston Blvd\r\nLas Vegas, NV 89146","wash_plan":"US Gas Captain Member","qr_code":"http://yourhomewash.com/public/web/triple.jpg","promo_code":"","first_month_price":"0.01","is_freeze":"0","is_cancel":"0","cancel_date":"","freeze_date":"","inactive_plan":"0","deactive_plan":"0","is_failed":0,"pay_trough_date":"2020-05-14","next_transaction_date":"2020-05-15","note":"","offer_days":"You may wash your Vehicle 7 days a week.","show_freeze":1,"show_cancel":1,"plan_details":{"plan_id":40,"name":"US Gas Captain Member","type":"2","discount":"","extra_plan":"","details":"10 Cents Off A Gallon<\/br> \r\n10% Off Any Car Wash At Showtime Car Wash\r\n\r\nYou May Use 7 Days A Week","price":"4.99"},"carwash_owner_details":{"carwash_id":21,"logo":"http://yourhomewash.com/public/images/owners/15783897745e14510eeb12f.PNG","owner_name":"US Gas (Charleston & Jones)","email":"zach@gmail.com","mobile_code":"1","mobile":"7678678678","shop_name":"US Gas (Charleston & Jones)","working_days":"Monday","to_working_day":"Sunday","full_address":"6100 W. Charleston Blvd\r\nLas Vegas, NV 89146","lat":36.1595149,"lng":-115.22443900000002,"city":"Las Vegas","distance":0},"car_details":{"car_id":154,"car_make":"3","modal":"Concord","car_type":"Sedan Car","car_color":null,"licence_no":""},"freeze_select_dates":["2020-05-15","2020-06-15","2020-07-15","2020-08-15"],"created_at":{"date":"2020-04-15 06:55:13.000000","timezone_type":3,"timezone":"America/Los_Angeles"}},{"membership_id":155,"carwash_owner_id":"21","carwash_price_id":"40","location":"6100 W. Charleston Blvd\r\nLas Vegas, NV 89146","wash_plan":"US Gas Captain Member","qr_code":"http://yourhomewash.com/public/web/triple.jpg","promo_code":"","first_month_price":"0.01","is_freeze":"0","is_cancel":"0","cancel_date":"","freeze_date":"","inactive_plan":"0","deactive_plan":"0","is_failed":0,"pay_trough_date":"2020-05-14","next_transaction_date":"2020-05-15","note":"","offer_days":"You may wash your Vehicle 7 days a week.","show_freeze":1,"show_cancel":1,"plan_details":{"plan_id":40,"name":"US Gas Captain Member","type":"2","discount":"","extra_plan":"","details":"10 Cents Off A Gallon<\/br> \r\n10% Off Any Car Wash At Showtime Car Wash\r\n\r\nYou May Use 7 Days A Week","price":"4.99"},"carwash_owner_details":{"carwash_id":21,"logo":"http://yourhomewash.com/public/images/owners/15783897745e14510eeb12f.PNG","owner_name":"US Gas (Charleston & Jones)","email":"zach@gmail.com","mobile_code":"1","mobile":"7678678678","shop_name":"US Gas (Charleston & Jones)","working_days":"Monday","to_working_day":"Sunday","full_address":"6100 W. Charleston Blvd\r\nLas Vegas, NV 89146","lat":36.1595149,"lng":-115.22443900000002,"city":"Las Vegas","distance":0},"car_details":{"car_id":153,"car_make":"Audi","modal":"80","car_type":"Sedan Car","car_color":"Red","licence_no":""},"freeze_select_dates":["2020-05-15","2020-06-15","2020-07-15","2020-08-15"],"created_at":{"date":"2020-04-15 06:49:42.000000","timezone_type":3,"timezone":"America/Los_Angeles"}},{"membership_id":149,"carwash_owner_id":"21","carwash_price_id":"40","location":"6100 W. Charleston Blvd\r\nLas Vegas, NV 89146","wash_plan":"US Gas Captain Member","qr_code":"http://yourhomewash.com/public/web/triple.jpg","promo_code":"null","first_month_price":"0.01","is_freeze":"0","is_cancel":"0","cancel_date":"","freeze_date":"","inactive_plan":"0","deactive_plan":"0","is_failed":0,"pay_trough_date":"2020-06-03","next_transaction_date":"2020-06-04","note":"heyyyy","offer_days":"You may wash your Vehicle 7 days a week.","show_freeze":1,"show_cancel":1,"plan_details":{"plan_id":40,"name":"US Gas Captain Member","type":"2","discount":"","extra_plan":"","details":"10 Cents Off A Gallon<\/br> \r\n10% Off Any Car Wash At Showtime Car Wash\r\n\r\nYou May Use 7 Days A Week","price":"4.99"},"carwash_owner_details":{"carwash_id":21,"logo":"http://yourhomewash.com/public/images/owners/15783897745e14510eeb12f.PNG","owner_name":"US Gas (Charleston & Jones)","email":"zach@gmail.com","mobile_code":"1","mobile":"7678678678","shop_name":"US Gas (Charleston & Jones)","working_days":"Monday","to_working_day":"Sunday","full_address":"6100 W. Charleston Blvd\r\nLas Vegas, NV 89146","lat":36.1595149,"lng":-115.22443900000002,"city":"Las Vegas","distance":0},"car_details":{"car_id":147,"car_make":"Audi","modal":"A4","car_type":"Sedan Car","car_color":"Red","licence_no":""},"freeze_select_dates":["2020-06-04","2020-07-04","2020-08-04","2020-09-04"],"created_at":{"date":"2020-04-03 23:37:14.000000","timezone_type":3,"timezone":"America/Los_Angeles"}}]
     * message : list successfully.
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * membership_id : 186
         * carwash_owner_id : 18
         * carwash_price_id : 45
         * location : 5893 W Tropicana Ave
         Las Vegas, NV 89103
         * wash_plan : EXTERIOR WASH
         * qr_code : http://yourhomewash.com/public/web/triple.jpg
         * promo_code :
         * first_month_price : 0.01
         * is_freeze : 0
         * is_cancel : 0
         * cancel_date :
         * freeze_date :
         * inactive_plan : 0
         * deactive_plan : 0
         * is_failed : 0
         * pay_trough_date : 2020-08-25
         * next_transaction_date : 2020-08-26
         * note :
         * offer_days : You may wash your Vehicle Sunday-Thursday Through Thursday
         * show_freeze : 1
         * show_cancel : 1
         * plan_details : {"plan_id":45,"name":"EXTERIOR WASH","type":"0","discount":"","extra_plan":"","details":"Exterior Car Wash, Spot-Free Rinse, & Clean Outside Windows","price":"19.99"}
         * carwash_owner_details : {"carwash_id":18,"logo":"http://yourhomewash.com/public/images/owners/15760393005df07384e7b9a.PNG","owner_name":"Jose Canseco Showtime Car Wash","email":"showtimecarwashcustomer@gmail.com","mobile_code":"1","mobile":"453453434534","shop_name":"Jose Canseco Showtime Car Wash","working_days":"Sunday-Thursday","to_working_day":"Thursday","full_address":"5893 W Tropicana Ave\r\nLas Vegas, NV 89103","lat":36.1104125,"lng":-115.20670110000003,"city":"Las Vegas","distance":0}
         * car_details : {"car_id":184,"car_make":"Bentley","modal":"Brooklands","car_type":"Sedan Car","car_color":"Blue","licence_no":""}
         * freeze_select_dates : ["2020-05-27","2020-06-27","2020-07-27","2020-08-27"]
         * created_at : {"date":"2020-04-26 21:52:18.000000","timezone_type":3,"timezone":"America/Los_Angeles"}
         */

        private int membership_id;
        private String carwash_owner_id;
        private String carwash_price_id;
        private String location;
        private String wash_plan;
        private String qr_code;
        private String promo_code;
        private String first_month_price;
        private int is_freeze;
        private int is_cancel;
        private String cancel_date;
        private String freeze_date;
        private int inactive_plan;
        private int deactive_plan;
        private int is_failed;
        private String pay_trough_date;
        private String next_transaction_date;
        private String note;
        private String offer_days;
        private int show_freeze;
        private int show_cancel;
        private PlanDetailsBean plan_details;
        private CarwashOwnerDetailsBean carwash_owner_details;
        private CarDetailsBean car_details;
        private CreatedAtBean created_at;
        private List<String> freeze_select_dates;

        public int getMembership_id() {
            return membership_id;
        }

        public void setMembership_id(int membership_id) {
            this.membership_id = membership_id;
        }

        public String getCarwash_owner_id() {
            return carwash_owner_id;
        }

        public void setCarwash_owner_id(String carwash_owner_id) {
            this.carwash_owner_id = carwash_owner_id;
        }

        public String getCarwash_price_id() {
            return carwash_price_id;
        }

        public void setCarwash_price_id(String carwash_price_id) {
            this.carwash_price_id = carwash_price_id;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getWash_plan() {
            return wash_plan;
        }

        public void setWash_plan(String wash_plan) {
            this.wash_plan = wash_plan;
        }

        public String getQr_code() {
            return qr_code;
        }

        public void setQr_code(String qr_code) {
            this.qr_code = qr_code;
        }

        public String getPromo_code() {
            return promo_code;
        }

        public void setPromo_code(String promo_code) {
            this.promo_code = promo_code;
        }

        public String getFirst_month_price() {
            return first_month_price;
        }

        public void setFirst_month_price(String first_month_price) {
            this.first_month_price = first_month_price;
        }

        public int getIs_freeze() {
            return is_freeze;
        }

        public void setIs_freeze(int is_freeze) {
            this.is_freeze = is_freeze;
        }

        public int getIs_cancel() {
            return is_cancel;
        }

        public void setIs_cancel(int is_cancel) {
            this.is_cancel = is_cancel;
        }

        public String getCancel_date() {
            return cancel_date;
        }

        public void setCancel_date(String cancel_date) {
            this.cancel_date = cancel_date;
        }

        public String getFreeze_date() {
            return freeze_date;
        }

        public void setFreeze_date(String freeze_date) {
            this.freeze_date = freeze_date;
        }

        public int getInactive_plan() {
            return inactive_plan;
        }

        public void setInactive_plan(int inactive_plan) {
            this.inactive_plan = inactive_plan;
        }

        public int getDeactive_plan() {
            return deactive_plan;
        }

        public void setDeactive_plan(int deactive_plan) {
            this.deactive_plan = deactive_plan;
        }

        public int getIs_failed() {
            return is_failed;
        }

        public void setIs_failed(int is_failed) {
            this.is_failed = is_failed;
        }

        public String getPay_trough_date() {
            return pay_trough_date;
        }

        public void setPay_trough_date(String pay_trough_date) {
            this.pay_trough_date = pay_trough_date;
        }

        public String getNext_transaction_date() {
            return next_transaction_date;
        }

        public void setNext_transaction_date(String next_transaction_date) {
            this.next_transaction_date = next_transaction_date;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getOffer_days() {
            return offer_days;
        }

        public void setOffer_days(String offer_days) {
            this.offer_days = offer_days;
        }

        public int getShow_freeze() {
            return show_freeze;
        }

        public void setShow_freeze(int show_freeze) {
            this.show_freeze = show_freeze;
        }

        public int getShow_cancel() {
            return show_cancel;
        }

        public void setShow_cancel(int show_cancel) {
            this.show_cancel = show_cancel;
        }

        public PlanDetailsBean getPlan_details() {
            return plan_details;
        }

        public void setPlan_details(PlanDetailsBean plan_details) {
            this.plan_details = plan_details;
        }

        public CarwashOwnerDetailsBean getCarwash_owner_details() {
            return carwash_owner_details;
        }

        public void setCarwash_owner_details(CarwashOwnerDetailsBean carwash_owner_details) {
            this.carwash_owner_details = carwash_owner_details;
        }

        public CarDetailsBean getCar_details() {
            return car_details;
        }

        public void setCar_details(CarDetailsBean car_details) {
            this.car_details = car_details;
        }

        public CreatedAtBean getCreated_at() {
            return created_at;
        }

        public void setCreated_at(CreatedAtBean created_at) {
            this.created_at = created_at;
        }

        public List<String> getFreeze_select_dates() {
            return freeze_select_dates;
        }

        public void setFreeze_select_dates(List<String> freeze_select_dates) {
            this.freeze_select_dates = freeze_select_dates;
        }

        public static class PlanDetailsBean {
            /**
             * plan_id : 45
             * name : EXTERIOR WASH
             * type : 0
             * discount :
             * extra_plan :
             * details : Exterior Car Wash, Spot-Free Rinse, & Clean Outside Windows
             * price : 19.99
             */

            private int plan_id;
            private String name;
            private String type;
            private String discount;
            private String extra_plan;
            private String details;
            private String price;

            public int getPlan_id() {
                return plan_id;
            }

            public void setPlan_id(int plan_id) {
                this.plan_id = plan_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getDiscount() {
                return discount;
            }

            public void setDiscount(String discount) {
                this.discount = discount;
            }

            public String getExtra_plan() {
                return extra_plan;
            }

            public void setExtra_plan(String extra_plan) {
                this.extra_plan = extra_plan;
            }

            public String getDetails() {
                return details;
            }

            public void setDetails(String details) {
                this.details = details;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }
        }

        public static class CarwashOwnerDetailsBean {
            /**
             * carwash_id : 18
             * logo : http://yourhomewash.com/public/images/owners/15760393005df07384e7b9a.PNG
             * owner_name : Jose Canseco Showtime Car Wash
             * email : showtimecarwashcustomer@gmail.com
             * mobile_code : 1
             * mobile : 453453434534
             * shop_name : Jose Canseco Showtime Car Wash
             * working_days : Sunday-Thursday
             * to_working_day : Thursday
             * full_address : 5893 W Tropicana Ave
             Las Vegas, NV 89103
             * lat : 36.1104125
             * lng : -115.20670110000003
             * city : Las Vegas
             * distance : 0
             */

            private int carwash_id;
            private String logo;
            private String owner_name;
            private String email;
            private String mobile_code;
            private String mobile;
            private String shop_name;
            private String working_days;
            private String to_working_day;
            private String full_address;
            private double lat;
            private double lng;
            private String city;
            private int distance;

            public int getCarwash_id() {
                return carwash_id;
            }

            public void setCarwash_id(int carwash_id) {
                this.carwash_id = carwash_id;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getOwner_name() {
                return owner_name;
            }

            public void setOwner_name(String owner_name) {
                this.owner_name = owner_name;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getMobile_code() {
                return mobile_code;
            }

            public void setMobile_code(String mobile_code) {
                this.mobile_code = mobile_code;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getShop_name() {
                return shop_name;
            }

            public void setShop_name(String shop_name) {
                this.shop_name = shop_name;
            }

            public String getWorking_days() {
                return working_days;
            }

            public void setWorking_days(String working_days) {
                this.working_days = working_days;
            }

            public String getTo_working_day() {
                return to_working_day;
            }

            public void setTo_working_day(String to_working_day) {
                this.to_working_day = to_working_day;
            }

            public String getFull_address() {
                return full_address;
            }

            public void setFull_address(String full_address) {
                this.full_address = full_address;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public int getDistance() {
                return distance;
            }

            public void setDistance(int distance) {
                this.distance = distance;
            }
        }

        public static class CarDetailsBean {
            /**
             * car_id : 184
             * car_make : Bentley
             * modal : Brooklands
             * car_type : Sedan Car
             * car_color : Blue
             * licence_no :
             */

            private int car_id;
            private String car_make;
            private String modal;
            private String car_type;
            private String car_color;
            private String licence_no;

            public int getCar_id() {
                return car_id;
            }

            public void setCar_id(int car_id) {
                this.car_id = car_id;
            }

            public String getCar_make() {
                return car_make;
            }

            public void setCar_make(String car_make) {
                this.car_make = car_make;
            }

            public String getModal() {
                return modal;
            }

            public void setModal(String modal) {
                this.modal = modal;
            }

            public String getCar_type() {
                return car_type;
            }

            public void setCar_type(String car_type) {
                this.car_type = car_type;
            }

            public String getCar_color() {
                return car_color;
            }

            public void setCar_color(String car_color) {
                this.car_color = car_color;
            }

            public String getLicence_no() {
                return licence_no;
            }

            public void setLicence_no(String licence_no) {
                this.licence_no = licence_no;
            }
        }

        public static class CreatedAtBean {
            /**
             * date : 2020-04-26 21:52:18.000000
             * timezone_type : 3
             * timezone : America/Los_Angeles
             */

            private String date;
            private int timezone_type;
            private String timezone;

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public int getTimezone_type() {
                return timezone_type;
            }

            public void setTimezone_type(int timezone_type) {
                this.timezone_type = timezone_type;
            }

            public String getTimezone() {
                return timezone;
            }

            public void setTimezone(String timezone) {
                this.timezone = timezone;
            }
        }
    }
}
