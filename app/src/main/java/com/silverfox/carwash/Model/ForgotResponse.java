package com.silverfox.carwash.Model;

public class ForgotResponse {


    /**
     * message : Unable to send reset link
     * status : false
     */

    private String message;
    private boolean status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
