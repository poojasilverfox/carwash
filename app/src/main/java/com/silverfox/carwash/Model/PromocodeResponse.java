package com.silverfox.carwash.Model;

public class PromocodeResponse {


    /**
     * status : 1
     * data : {"promocode":"Jose20204040","discount":"10","type":"0"}
     * message : Promocode Applied successfully.
     */

    private int status;
    private DataBean data;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataBean {
        /**
         * promocode : Jose20204040
         * discount : 10
         * type : 0
         */

        private String promocode;
        private String discount;
        private String type;

        public String getPromocode() {
            return promocode;
        }

        public void setPromocode(String promocode) {
            this.promocode = promocode;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
