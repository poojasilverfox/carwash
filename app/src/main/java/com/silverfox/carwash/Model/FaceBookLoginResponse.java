package com.silverfox.carwash.Model;

public class FaceBookLoginResponse {


    /**
     * status : 1
     * data : {"user_id":44,"name":"Hello","email":null,"mobile_code":null,"mobile":null,"otp":"5139","wallet":0,"reffer_code":"283344","reffer_by":null,"card_number":null,"expiry_month":null,"expiry_year":null,"cvv_code":null,"api_token":"MFZ3YTZoT1Zob21hUHRMNEdrRHFKQ1RKczF3ZUNCQVdrVDl5bEh3NXNyZHFWSGFpMVZMSHRHZVJ2aUFJ5de4defc4a623","created_at":{"date":"2019-12-02 09:53:00.000000","timezone_type":3,"timezone":"UTC"}}
     * message : Facebook Login successfully.
     */

    private int status;
    private DataBean data;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataBean {
        /**
         * user_id : 44
         * name : Hello
         * email : null
         * mobile_code : null
         * mobile : null
         * otp : 5139
         * wallet : 0
         * reffer_code : 283344
         * reffer_by : null
         * card_number : null
         * expiry_month : null
         * expiry_year : null
         * cvv_code : null
         * api_token : MFZ3YTZoT1Zob21hUHRMNEdrRHFKQ1RKczF3ZUNCQVdrVDl5bEh3NXNyZHFWSGFpMVZMSHRHZVJ2aUFJ5de4defc4a623
         * created_at : {"date":"2019-12-02 09:53:00.000000","timezone_type":3,"timezone":"UTC"}
         */

        private int user_id;
        private String name;
        private Object email;
        private Object mobile_code;
        private Object mobile;
        private String otp;
        private int wallet;
        private String reffer_code;
        private Object reffer_by;
        private Object card_number;
        private Object expiry_month;
        private Object expiry_year;
        private Object cvv_code;
        private String api_token;
        private CreatedAtBean created_at;

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getEmail() {
            return email;
        }

        public void setEmail(Object email) {
            this.email = email;
        }

        public Object getMobile_code() {
            return mobile_code;
        }

        public void setMobile_code(Object mobile_code) {
            this.mobile_code = mobile_code;
        }

        public Object getMobile() {
            return mobile;
        }

        public void setMobile(Object mobile) {
            this.mobile = mobile;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public int getWallet() {
            return wallet;
        }

        public void setWallet(int wallet) {
            this.wallet = wallet;
        }

        public String getReffer_code() {
            return reffer_code;
        }

        public void setReffer_code(String reffer_code) {
            this.reffer_code = reffer_code;
        }

        public Object getReffer_by() {
            return reffer_by;
        }

        public void setReffer_by(Object reffer_by) {
            this.reffer_by = reffer_by;
        }

        public Object getCard_number() {
            return card_number;
        }

        public void setCard_number(Object card_number) {
            this.card_number = card_number;
        }

        public Object getExpiry_month() {
            return expiry_month;
        }

        public void setExpiry_month(Object expiry_month) {
            this.expiry_month = expiry_month;
        }

        public Object getExpiry_year() {
            return expiry_year;
        }

        public void setExpiry_year(Object expiry_year) {
            this.expiry_year = expiry_year;
        }

        public Object getCvv_code() {
            return cvv_code;
        }

        public void setCvv_code(Object cvv_code) {
            this.cvv_code = cvv_code;
        }

        public String getApi_token() {
            return api_token;
        }

        public void setApi_token(String api_token) {
            this.api_token = api_token;
        }

        public CreatedAtBean getCreated_at() {
            return created_at;
        }

        public void setCreated_at(CreatedAtBean created_at) {
            this.created_at = created_at;
        }

        public static class CreatedAtBean {
            /**
             * date : 2019-12-02 09:53:00.000000
             * timezone_type : 3
             * timezone : UTC
             */

            private String date;
            private int timezone_type;
            private String timezone;

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public int getTimezone_type() {
                return timezone_type;
            }

            public void setTimezone_type(int timezone_type) {
                this.timezone_type = timezone_type;
            }

            public String getTimezone() {
                return timezone;
            }

            public void setTimezone(String timezone) {
                this.timezone = timezone;
            }
        }
    }
}
