package com.silverfox.carwash.Model;

public class PublicKeyResponse {

    /**
     * status : 1
     * public_key : pkapi_prod_oHcJckYRrchrmZwd9H
     * message : Public key get sucessfully
     */

    private int status;
    private String public_key;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getPublic_key() {
        return public_key;
    }

    public void setPublic_key(String public_key) {
        this.public_key = public_key;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
