package com.silverfox.carwash.Model;

import java.util.List;

public class MembershipTListResponse {


    /**
     * status : 1
     * data : [{"transaction_id":"1269244045","date":"2019-12-13","amount":"15.00","transaction_by_wallet":{"amount":"10.00","type":"Debit","redeem_code":"","created_at":"2019-12-12"},"created_at":"2019-12-13","membership_full_details":{"location":"320 N Andover Rd, Andover, KS 67002, USA","wash_plan":"Silver","created_at":"2019-12-13","plan_details":{"plan_id":12,"name":"Silver","details":"Inferno Extreme Shine w/Carnauba, Rocker Panel & Underbody Wash, Hot Foaming Presoak 2x, Bug Prep, Double Pass High Pressure Rinse, Reflection Clear Coat Protection, Tri Color Foam, Super Sealant, Spot Free Rinse, High Velocity Blowers","price":15},"carwash_owner_details":{"carwash_id":16,"logo":"https://silverfoxstudio.in/car/public/images/owners/15756988085deb417863d5c.jpg","owner_name":"Andover Laserwash","email":"andover@gmail.com","mobile_code":"1","mobile":"3475678345893","shop_name":"Andover Laserwash","working_days":"24 Hours","full_address":"320 N Andover Rd, Andover, KS 67002, USA","lat":37,"lng":-97,"city":"surat","distance":0}}},{"transaction_id":"1268606737","date":"2019-12-12","amount":"15.00","transaction_by_wallet":{"amount":"10.00","type":"Debit","redeem_code":"","created_at":"2019-12-12"},"created_at":"2019-12-12","membership_full_details":{"location":"BAPA SITARAM CHOWK, KATARGAM, SURAT-395004","wash_plan":"Ultimate Wash","created_at":"2019-12-12","plan_details":{"plan_id":3,"name":"Ultimate Wash","details":"Simoniz Hot Wax & Shine, Simoniz Tire Shine, Simoniz Triple Foam Polish, Side Blasters, Bug Buster, Free Air Freshener, Underbody Blast, Tire & Wheel Cleaner, Presoak, Friction Wash, High Pressure Rinse, Spot Free Rinse, Turbo Dry, Free Vacuum Token","price":25},"carwash_owner_details":{"carwash_id":4,"logo":"https://silverfoxstudio.in/car/public/images/owners/15752871795de4f98b480ec.jpg","owner_name":"HELLO CARWASH","email":"PHP@GMAIL.COM","mobile_code":"213","mobile":"7894561230","shop_name":"HELLO CARWASH","working_days":"MONDAY TO FRIDAY ON 7:00AM TO 6:00PM","full_address":"BAPA SITARAM CHOWK, KATARGAM, SURAT-395004","lat":21,"lng":72,"city":"SURAT, INDIA","distance":0}}}]
     * message : list successfully.
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * transaction_id : 1269244045
         * date : 2019-12-13
         * amount : 15.00
         * transaction_by_wallet : {"amount":"10.00","type":"Debit","redeem_code":"","created_at":"2019-12-12"}
         * created_at : 2019-12-13
         * membership_full_details : {"location":"320 N Andover Rd, Andover, KS 67002, USA","wash_plan":"Silver","created_at":"2019-12-13","plan_details":{"plan_id":12,"name":"Silver","details":"Inferno Extreme Shine w/Carnauba, Rocker Panel & Underbody Wash, Hot Foaming Presoak 2x, Bug Prep, Double Pass High Pressure Rinse, Reflection Clear Coat Protection, Tri Color Foam, Super Sealant, Spot Free Rinse, High Velocity Blowers","price":15},"carwash_owner_details":{"carwash_id":16,"logo":"https://silverfoxstudio.in/car/public/images/owners/15756988085deb417863d5c.jpg","owner_name":"Andover Laserwash","email":"andover@gmail.com","mobile_code":"1","mobile":"3475678345893","shop_name":"Andover Laserwash","working_days":"24 Hours","full_address":"320 N Andover Rd, Andover, KS 67002, USA","lat":37,"lng":-97,"city":"surat","distance":0}}
         */

        private String transaction_id;
        private String id;
        private String date;
        private String amount;
        private TransactionByWalletBean transaction_by_wallet;
        private String created_at;
        private MembershipFullDetailsBean membership_full_details;

        public String getTransaction_id() {
            return transaction_id;
        }

        public void setTransaction_id(String transaction_id) {
            this.transaction_id = transaction_id;
        }

        public String getDate() {
            return date;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public TransactionByWalletBean getTransaction_by_wallet() {
            return transaction_by_wallet;
        }

        public void setTransaction_by_wallet(TransactionByWalletBean transaction_by_wallet) {
            this.transaction_by_wallet = transaction_by_wallet;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public MembershipFullDetailsBean getMembership_full_details() {
            return membership_full_details;
        }

        public void setMembership_full_details(MembershipFullDetailsBean membership_full_details) {
            this.membership_full_details = membership_full_details;
        }

        public static class TransactionByWalletBean {
            /**
             * amount : 10.00
             * type : Debit
             * redeem_code :
             * created_at : 2019-12-12
             */

            private String amount;
            private String type;
            private String redeem_code;
            private String created_at;

            public String getAmount() {
                return amount;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getRedeem_code() {
                return redeem_code;
            }

            public void setRedeem_code(String redeem_code) {
                this.redeem_code = redeem_code;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }
        }

        public static class MembershipFullDetailsBean {
            /**
             * location : 320 N Andover Rd, Andover, KS 67002, USA
             * wash_plan : Silver
             * created_at : 2019-12-13
             * plan_details : {"plan_id":12,"name":"Silver","details":"Inferno Extreme Shine w/Carnauba, Rocker Panel & Underbody Wash, Hot Foaming Presoak 2x, Bug Prep, Double Pass High Pressure Rinse, Reflection Clear Coat Protection, Tri Color Foam, Super Sealant, Spot Free Rinse, High Velocity Blowers","price":15}
             * carwash_owner_details : {"carwash_id":16,"logo":"https://silverfoxstudio.in/car/public/images/owners/15756988085deb417863d5c.jpg","owner_name":"Andover Laserwash","email":"andover@gmail.com","mobile_code":"1","mobile":"3475678345893","shop_name":"Andover Laserwash","working_days":"24 Hours","full_address":"320 N Andover Rd, Andover, KS 67002, USA","lat":37,"lng":-97,"city":"surat","distance":0}
             */

            private String location;
            private String wash_plan;
            private String created_at;
            private PlanDetailsBean plan_details;
            private CarwashOwnerDetailsBean carwash_owner_details;

            public String getLocation() {
                return location;
            }

            public void setLocation(String location) {
                this.location = location;
            }

            public String getWash_plan() {
                return wash_plan;
            }

            public void setWash_plan(String wash_plan) {
                this.wash_plan = wash_plan;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public PlanDetailsBean getPlan_details() {
                return plan_details;
            }

            public void setPlan_details(PlanDetailsBean plan_details) {
                this.plan_details = plan_details;
            }

            public CarwashOwnerDetailsBean getCarwash_owner_details() {
                return carwash_owner_details;
            }

            public void setCarwash_owner_details(CarwashOwnerDetailsBean carwash_owner_details) {
                this.carwash_owner_details = carwash_owner_details;
            }

            public static class PlanDetailsBean {
                /**
                 * plan_id : 12
                 * name : Silver
                 * details : Inferno Extreme Shine w/Carnauba, Rocker Panel & Underbody Wash, Hot Foaming Presoak 2x, Bug Prep, Double Pass High Pressure Rinse, Reflection Clear Coat Protection, Tri Color Foam, Super Sealant, Spot Free Rinse, High Velocity Blowers
                 * price : 15
                 */

                private int plan_id;
                private String name;
                private String details;
                private String price;

                public int getPlan_id() {
                    return plan_id;
                }

                public void setPlan_id(int plan_id) {
                    this.plan_id = plan_id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getDetails() {
                    return details;
                }

                public void setDetails(String details) {
                    this.details = details;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }
            }

            public static class CarwashOwnerDetailsBean {
                /**
                 * carwash_id : 16
                 * logo : https://silverfoxstudio.in/car/public/images/owners/15756988085deb417863d5c.jpg
                 * owner_name : Andover Laserwash
                 * email : andover@gmail.com
                 * mobile_code : 1
                 * mobile : 3475678345893
                 * shop_name : Andover Laserwash
                 * working_days : 24 Hours
                 * full_address : 320 N Andover Rd, Andover, KS 67002, USA
                 * lat : 37
                 * lng : -97
                 * city : surat
                 * distance : 0
                 */

                private int carwash_id;
                private String logo;
                private String owner_name;
                private String email;
                private String mobile_code;
                private String mobile;
                private String shop_name;
                private String working_days;
                private String full_address;
                private float lat;
                private float lng;
                private String city;
                private int distance;

                public float getLat() {
                    return lat;
                }

                public void setLat(float lat) {
                    this.lat = lat;
                }

                public float getLng() {
                    return lng;
                }

                public void setLng(float lng) {
                    this.lng = lng;
                }

                public int getCarwash_id() {
                    return carwash_id;
                }

                public void setCarwash_id(int carwash_id) {
                    this.carwash_id = carwash_id;
                }

                public String getLogo() {
                    return logo;
                }

                public void setLogo(String logo) {
                    this.logo = logo;
                }

                public String getOwner_name() {
                    return owner_name;
                }

                public void setOwner_name(String owner_name) {
                    this.owner_name = owner_name;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public String getMobile_code() {
                    return mobile_code;
                }

                public void setMobile_code(String mobile_code) {
                    this.mobile_code = mobile_code;
                }

                public String getMobile() {
                    return mobile;
                }

                public void setMobile(String mobile) {
                    this.mobile = mobile;
                }

                public String getShop_name() {
                    return shop_name;
                }

                public void setShop_name(String shop_name) {
                    this.shop_name = shop_name;
                }

                public String getWorking_days() {
                    return working_days;
                }

                public void setWorking_days(String working_days) {
                    this.working_days = working_days;
                }

                public String getFull_address() {
                    return full_address;
                }

                public void setFull_address(String full_address) {
                    this.full_address = full_address;
                }


                public String getCity() {
                    return city;
                }

                public void setCity(String city) {
                    this.city = city;
                }

                public int getDistance() {
                    return distance;
                }

                public void setDistance(int distance) {
                    this.distance = distance;
                }
            }
        }
    }
}
