package com.silverfox.carwash.Model;

public class WalletUpdateResponse {


    /**
     * status : 1
     * data : 0
     * message : Balance Load successfully.
     */

    private int status;
    private int data;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
