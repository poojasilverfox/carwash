package com.silverfox.carwash.Model;

public class CancelPlanResponse {


    /**
     * status : 1
     * data : {"is_cancel":"0"}
     * message : Cancel Applied successfully.
     */

    private int status;
    private DataBean data;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataBean {
        /**
         * is_cancel : 0
         */

        private String is_cancel;

        public String getIs_cancel() {
            return is_cancel;
        }

        public void setIs_cancel(String is_cancel) {
            this.is_cancel = is_cancel;
        }
    }
}
