package com.silverfox.carwash.Fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Activity.MainActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.Model.CarWashListResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;
import com.silverfox.carwash.adapter.CarWashListAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class LocationFragment extends Fragment implements View.OnClickListener,
        OnMapReadyCallback, CarWashListAdapter.loadFrgment {

    private ApiInterface apiInterface;
    CarWashListAdapter adapter;
    View view;
    EditText et_searchtext;
    TextView tv_viewmap;
    LinearLayout tv_nodata;
    RecyclerView rv_list;
    private LinearLayoutManager layoutManager;
    private GoogleMap mMap;
    private LinearLayout ll_maplayout;



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_location, container, false);


        Log.e(TAG, "onCreateView: api roken::::::::"+Preference.getAPI_TOKEN() );
        findView();

        if (Util.fragment_position_flag) {
            tv_viewmap.setText(getString(R.string.vmap));
            ll_maplayout.setVisibility(View.GONE);
            if (Util.carwashlistData.size() > 0) {
                rv_list.setVisibility(View.VISIBLE);
                tv_nodata.setVisibility(View.GONE);
            } else {

                rv_list.setVisibility(View.GONE);
                tv_nodata.setVisibility(View.VISIBLE);
            }
        } else {
            tv_viewmap.setText(getString(R.string.vlist));
            rv_list.setVisibility(View.GONE);
            if (Util.carwashlistData.size() > 0) {

                ll_maplayout.setVisibility(View.VISIBLE);
                tv_nodata.setVisibility(View.GONE);
            } else {
                ll_maplayout.setVisibility(View.GONE);
                tv_nodata.setVisibility(View.VISIBLE);
            }
        }


        if (Util.location_flag) {

            SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
            tv_nodata.setVisibility(View.GONE);
            rv_list.setVisibility(View.VISIBLE);
            adapter = new CarWashListAdapter(getActivity(), Util.carwashlistData, this);
            rv_list.setAdapter(adapter);

        }


        getList(String.valueOf(Util.currentposition));
        Log.e("tag", "onCreateView: " + Util.currentposition);
        rv_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);


            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager layoutManager = ((LinearLayoutManager) rv_list.getLayoutManager());
                int pos = layoutManager.findLastCompletelyVisibleItemPosition();
                int numItems = adapter.getItemCount();
                if (pos >= numItems - 1) {
                    if (rv_list.getVerticalScrollbarPosition() == 0) {

                        if (dy >= 0) {

                            if (Util.isConnected(getActivity())) {
                                getList(String.valueOf(Util.currentposition));
                            } else {
                                Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                            }

                            Log.e("tag", "onScrolledggggggggupppppp: ");
                        } /*else {
                                Log.e("tag", "onScrolledggggggggdownnnnn: ");

                                if (dy == 0) {

                                    if (Util.isConnected(getActivity())) {
                                        getList(String.valueOf(Util.currentposition));
                                    }else{
                                        Util.ShowToastMsg(getActivity(),getString(R.string.noconn));
                                    }
                                }
                                // Scrolling down




                            }*/
                    }

                }
            }
        });



        /*if(scroll_flag) {
            rv_list.setOnScrollListener(new EndlessRecyclerOnScrollListenerNewGrid(layoutManager) {
                @Override
                public void onLoadMore(int paramInt) {
                    Log.e("tag", "onLoadMore:" + paramInt);
                    Log.e("tag", "onLoadMore: +scroll");


                    if (Util.isConnected(getActivity())) {
                        getList(String.valueOf(Util.currentposition));
                    }else{
                        Util.ShowToastMsg(getActivity(),getString(R.string.noconn));
                    }
                }


            });
        }
*/


        return view;


    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        et_searchtext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {


                    Util.searchfield = et_searchtext.getText().toString();
                    Log.e("tag", "onLoadMore: +search");
                    Util.carwashlistData = new ArrayList<>();
                    Util.hideKeyboard(getActivity());
                    Util.currentposition = 0;
                    Util.location_flag = false;

                    if (Util.isConnected(getActivity())) {
                        getList("0");
                    } else {
                        Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                    }

                    return true;
                }
                return false;
            }
        });


        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney, Australia, and move the camera.


        if (Util.carwashlistData.size() != 0) {


            for (int i = 0; i < Util.carwashlistData.size(); i++) {

                LatLng sydney = new LatLng(Util.carwashlistData.get(i).getLat(), Util.carwashlistData.get(i).getLng());
                mMap.addMarker(new MarkerOptions().position(sydney).title(Util.carwashlistData.get(i).getShop_name()));

                float zoomLevel = 8.0f; //This goes up to 21
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, zoomLevel));
            }

            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            mMap.getUiSettings().setZoomGesturesEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setScrollGesturesEnabled(true);
            mMap.getUiSettings().setTiltGesturesEnabled(true);
// false to disable
        }
    }

    private void findView() {


        ((MainActivity) getActivity()).iv_back.setVisibility(View.GONE);


        et_searchtext = view.findViewById(R.id.et_searchtext);
        tv_viewmap = view.findViewById(R.id.tv_viewmap);
        rv_list = view.findViewById(R.id.rv_list);
        ll_maplayout = view.findViewById(R.id.ll_maplayout);
        tv_nodata = view.findViewById(R.id.tv_nodata);

        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv_list.setLayoutManager(layoutManager);

        tv_viewmap.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_viewmap:
                if (ll_maplayout.getVisibility() == View.VISIBLE) {
                    ll_maplayout.setVisibility(View.GONE);
                    tv_viewmap.setText(getString(R.string.vmap));
                    if (Util.carwashlistData.size() == 0) {
                        tv_nodata.setVisibility(View.VISIBLE);
                        rv_list.setVisibility(View.GONE);
                    } else {
                        Util.hideKeyboard(getActivity());
                        Util.fragment_position_flag = true;
                        rv_list.setVisibility(View.VISIBLE);
                        tv_nodata.setVisibility(View.GONE);
                    }
                } else {
                    rv_list.setVisibility(View.GONE);
                    if (Util.carwashlistData.size() == 0) {
                        tv_nodata.setVisibility(View.VISIBLE);
                        ll_maplayout.setVisibility(View.GONE);
                    } else {
                        Util.fragment_position_flag = false;
                        Util.hideKeyboard(getActivity());
                        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                                .findFragmentById(R.id.map);
                        mapFragment.getMapAsync(this);
                        ll_maplayout.setVisibility(View.VISIBLE);
                        tv_viewmap.setText(getString(R.string.vlist));
                        tv_nodata.setVisibility(View.GONE);
                    }
                }

                break;
        }
    }


    private void getList(final String pno) {

        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<CarWashListResponse> call = apiInterface.getCarWashList(Preference.getAPI_TOKEN(), Util.searchfield, pno);
        call.enqueue(new Callback<CarWashListResponse>() {
            @Override
            public void onResponse(Call<CarWashListResponse> call, Response<CarWashListResponse> response) {

                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();

                switch (status) {
                    case 1:

                        Util.location_flag = true;
                        if (pno.equals("0")) {
                            Util.carwashlistData = new ArrayList<>();
                            Util.carwashlistData.clear();
                            Util.carwashlistData.addAll(response.body().getData());

                            rv_list.setVisibility(View.VISIBLE);
                            ll_maplayout.setVisibility(View.GONE);
                            tv_viewmap.setText("View Map");
                            adapter = new CarWashListAdapter(getActivity(), Util.carwashlistData, LocationFragment.this);
                            rv_list.setAdapter(adapter);

                        } else {

                            Util.carwashlistData.addAll(response.body().getData());

                        }

                        adapter.notifyDataSetChanged();
                        tv_viewmap.setText(getString(R.string.vmap));
                        tv_nodata.setVisibility(View.GONE);
                        ll_maplayout.setVisibility(View.GONE);
                        rv_list.setVisibility(View.VISIBLE);
                        Util.currentposition += 1;


                       /* if (a > 1) {
                            rv_list.setOnScrollListener(new EndlessRecyclerOnScrollListenerNewGrid(layoutManager) {
                                @Override
                                public void onLoadMore(int paramInt) {
                                    scroll_flag = false;
                                    Log.e("tag", "onLoadMore:" + paramInt);
                                    Log.e("tag", "onLoadMore: +scroll");


                                    if (Util.isConnected(getActivity())) {
                                        getList(String.valueOf(Util.currentposition));
                                    }else{
                                        Util.ShowToastMsg(getActivity(),getString(R.string.noconn));
                                    }                                }

                            });
                        }*/


                        break;


                    case 0:
                        if (pno.equals("0")) {
                            if (Util.carwashlistData.size() == 0) {
                                rv_list.setVisibility(View.GONE);
                                ll_maplayout.setVisibility(View.GONE);
                                tv_nodata.setVisibility(View.VISIBLE);
                                tv_viewmap.setText(getString(R.string.vmap));
                                if (adapter != null) {
                                    adapter.notifyDataSetChanged();
                                }

                            }


                        }
                        break;

                    case 404:
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;


                }


            }

            @Override
            public void onFailure(Call<CarWashListResponse> call, Throwable t) {
                Util.dismissCustomLoader();
                Log.e(TAG, "onFailure: failur"+t.getMessage() );

            }
        });
    }

    @Override
    public void loadFragm(int position) {
        Util.position = position;
        loadFragment(new WashFragment(position));

    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_layout, fragment)
                    .addToBackStack("")
                    .commit();
            return true;
        }
        return false;
    }


}
