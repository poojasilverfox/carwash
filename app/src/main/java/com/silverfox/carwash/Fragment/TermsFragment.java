package com.silverfox.carwash.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.silverfox.carwash.Activity.MainActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermsFragment extends Fragment {


 View view;
Button btn;


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.layout_terms, container, false);

       


        return view;


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity) getActivity()).iv_back.setVisibility(View.VISIBLE);

        ((MainActivity) getActivity()).iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();
            }
        });
        
        

    }




//        Call<RestResponse<FormData>> callApi = apiInterface.uplodData(f.getReport_no(), f.getReport_date(), f.getProject(), f.getPurchaser_po_no(), f.getManufacturer(), f.getPurchase_dated(), f.getLocation(), f.getVendor_wo_no(), f.getVendor_dated(), f.getContractor(), f.getPmc_job_no(), f.getItem_code(), f.getMaterial_description(), f.getQuantity_offered(), f.getQuantity_accepted(), f.getDrawing_no(), f.getDrawing_no(), f.getDrawing_date_approve(), f.getMaterial_certificate(), f.getRaw_material_certificate(), f.getEddy_current_testing(), f.getChemical_test_report(), f.getWeight(), f.getPhysical_test(), f.getFlattening_test(), f.getInstrument_calibration_a(), f.getWorkmanship(), f.getMarking(), f.getHydro_test(), f.getVisual_inspection(), f.getDimentional_check(), f.getOrder_complete(), f.getSeparate_attachment(), f.getIdentification(), "1", f.getDestination(), f.getBatch_no(), f.getMarking_frm_2(), f.getRefer_report_no(), f.getDistribution_list());


}
