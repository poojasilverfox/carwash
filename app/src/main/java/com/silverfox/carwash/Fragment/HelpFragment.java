package com.silverfox.carwash.Fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;
import com.wang.avi.AVLoadingIndicatorView;

public class HelpFragment extends Fragment{

    private WebView webView;
    AVLoadingIndicatorView loader;

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_help, container, false);

        webView = (WebView) view.findViewById(R.id.webView);
        loader = view.findViewById(R.id.loader);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());

        if(Util.isConnected(getActivity())) {

//            webView.loadUrl("https://silverfoxstudio.in/car/support_for_mobile");
           /* webView.loadUrl("http://yourhomewash.com/support_for_mobile");

            webView.setVisibility(View.VISIBLE);
            loader.setVisibility(View.GONE);*/

            webView.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }

                public void onPageFinished(WebView view, String url) {
                    loader.setVisibility(View.GONE);
                    webView.setVisibility(View.VISIBLE);
                }

                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

                }
            });
            webView.loadUrl("http://yourhomewash.com/support_for_mobile");

        }else{
            loader.setVisibility(View.GONE);
            webView.setVisibility(View.GONE);
            Util.ShowToastMsg(getActivity(),getString(R.string.noconn));
        }
        return view;
    }





}
