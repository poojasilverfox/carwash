package com.silverfox.carwash.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Activity.MainActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.Model.ReedemResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;
import com.wang.avi.AVLoadingIndicatorView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReedemFragment extends Fragment implements View.OnClickListener {


    View view;
    EditText et_gfbardno;
    LinearLayout llsubmit;
    ApiInterface apiInterface;
    String cardno = "";


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_redeem, container, false);

        findView();

        return view;
    }

    private void findView() {
        ((MainActivity) getActivity()).iv_back.setVisibility(View.VISIBLE);


        et_gfbardno = view.findViewById(R.id.et_gfbardno);
        llsubmit = view.findViewById(R.id.llsubmit);

        llsubmit.setOnClickListener(this);

        ((MainActivity) getActivity()).iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }


    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_layout, fragment)
                    .commit();
            return true;
        }
        return false;
    }
    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.llsubmit:
                cardno = et_gfbardno.getText().toString().trim();
                if (cardno.isEmpty()) {
                    et_gfbardno.setError(getString(R.string.ecardno));
                    et_gfbardno.requestFocus();
                } else {

                    if (Util.isConnected(getActivity())) {
                        getRedeem();
                    }else{
                        Util.ShowToastMsg(getActivity(),getString(R.string.noconn));
                    }

                }

        }
    }

    private void getRedeem() {
        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<ReedemResponse> call = apiInterface.getRedeemData(Preference.getAPI_TOKEN(), cardno);
        call.enqueue(new Callback<ReedemResponse>() {
            @Override
            public void onResponse(Call<ReedemResponse> call, Response<ReedemResponse> response) {
                Util.dismissCustomLoader();
                Log.e("hellooooooo", "data: " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();

                Util.ShowToastMsg(getActivity(), response.body().getMessage());

                switch (status){
                    case 1 :
                        Preference.setWALLET(response.body().getData().getWallet());
                        getActivity().onBackPressed();
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(),LoginActivity.class);
                        intent.putExtra("type","login");
                        startActivity(intent);
                        getActivity().finish();
                        break;
                }




            }

            @Override
            public void onFailure(Call<ReedemResponse> call, Throwable t) {
                Util.dismissCustomLoader();


            }
        });
    }

}
