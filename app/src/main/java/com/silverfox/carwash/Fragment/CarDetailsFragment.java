package com.silverfox.carwash.Fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Activity.MainActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.Model.CarMakeResponse;
import com.silverfox.carwash.Model.CarModelResponse;
import com.silverfox.carwash.Model.CarWashListResponse;
import com.silverfox.carwash.Model.LoginResponse;
import com.silverfox.carwash.Model.PaymentResponse;
import com.silverfox.carwash.Model.PublicKeyResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;
import com.silverfox.carwash.service.HLCard;
import com.silverfox.carwash.service.HLToken;
import com.silverfox.carwash.service.HLTokenService;
import com.silverfox.carwash.view.CustomRobotoEditText;
import com.silverfox.carwash.view.CustomRobotoTextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class CarDetailsFragment extends Fragment implements View.OnClickListener {


    LinearLayout llsubmit;
    EditText et_plate;
    TextView et_name, et_model, et_type, tv_planname, tv_netamounnt, et_color;
    String name = "", model = "", type = "", plateno = "", text = "", color = "";
    View view;
    ApiInterface apiInterface;
    float netamount = 0, amount = 0;
    ArrayAdapter cmakeadapter;
    ArrayAdapter cmodeladapter;
    ArrayAdapter coloradapter;
    ArrayAdapter ctypeadapter;
    ArrayList<CarMakeResponse.DataBean> carmakedata = new ArrayList<>();
    ArrayList<CarModelResponse.DataBean> carmodeldata = new ArrayList<>();
    ArrayList<String> SCarmMake = new ArrayList<>();
    ArrayList<String> SCarmModel = new ArrayList<>();
    String[] cartypearray;
    String[] colorarray;
    int id, position,ownerid;
    String shop_name, city, address;
    List<CarWashListResponse.DataBean.PlanDetailsBean> plan_details;
    Dialog dialog;

    public CarDetailsFragment(int intposition, List<CarWashListResponse.DataBean.PlanDetailsBean> plan_details, int ownerid,String shop_name, String city, String address, float amount) {

        this.position = intposition;
        this.shop_name = shop_name;
        this.city = city;
        this.address = address;
        this.plan_details = plan_details;
        this.amount = amount;
        this.ownerid = ownerid;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.layout_cardetails, container, false);


        if (Util.isConnected(getActivity())) {
            getCarName();
        } else {
            Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
        }
        netamount = amount + Preference.getCARSIZEAMOUNT() + Preference.getEXTRAPLANAMOUNT();
        Log.e(TAG, "onCreateView: net amount::::::"+amount+Preference.getCARSIZEAMOUNT() );
        findview();


        ((MainActivity) getActivity()).iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (((MainActivity) getActivity()) != null) {
                    ((MainActivity) getActivity()).onBackPressed();
                } else {

                    ((MainActivity) WashFragment.activity).onBackPressed();
                }

            }
        });
        return view;
    }

    private void getCarName() {
        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<CarMakeResponse> call = apiInterface.getCarMakeData();
        call.enqueue(new Callback<CarMakeResponse>() {
            @Override
            public void onResponse(Call<CarMakeResponse> call, Response<CarMakeResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));


                int status = response.body().getStatus();
                switch (status) {
                    case 1:

                        carmakedata.addAll(response.body().getData());
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            SCarmMake.add(response.body().getData().get(i).getTitle());
                        }

                        cmakeadapter = new ArrayAdapter(getActivity(),
                                android.R.layout.simple_dropdown_item_1line, SCarmMake);
                        break;

                    case 404:
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;

                }


            }

            @Override
            public void onFailure(Call<CarMakeResponse> call, Throwable t) {
                Util.dismissCustomLoader();
            }
        });
    }

    private void getCarModel() {
        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<CarModelResponse> call = apiInterface.getCarModelData(String.valueOf(id));
        call.enqueue(new Callback<CarModelResponse>() {
            @Override
            public void onResponse(Call<CarModelResponse> call, Response<CarModelResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));


                int status = response.body().getStatus();
                switch (status) {
                    case 1:

                        carmodeldata.addAll(response.body().getData());
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            SCarmModel.add(response.body().getData().get(i).getTitle());
                        }

                        cmodeladapter = new ArrayAdapter(getActivity(),
                                android.R.layout.simple_dropdown_item_1line, SCarmModel);
                        break;

                    case 404:
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;
                }


            }

            @Override
            public void onFailure(Call<CarModelResponse> call, Throwable t) {
                Util.dismissCustomLoader();
            }
        });
    }


    private void findview() {

        ((MainActivity) getActivity()).iv_back.setVisibility(View.VISIBLE);

        tv_netamounnt = view.findViewById(R.id.tv_netamounnt);
        tv_planname = view.findViewById(R.id.tv_planname);
        et_name = view.findViewById(R.id.et_name);
        et_model = view.findViewById(R.id.et_model);
        et_type = view.findViewById(R.id.et_type);
        et_plate = view.findViewById(R.id.et_plate);
        et_color = view.findViewById(R.id.et_color);
        llsubmit = view.findViewById(R.id.llsubmit);
        llsubmit.setOnClickListener(this);
        et_name.setOnClickListener(this);
        et_color.setOnClickListener(this);
        et_model.setOnClickListener(this);
        et_type.setOnClickListener(this);
        cartypearray = getResources().getStringArray(R.array.cartype);
        colorarray = getResources().getStringArray(R.array.colorarray);
        ctypeadapter = new ArrayAdapter(getActivity(),
                android.R.layout.simple_dropdown_item_1line, cartypearray);
        coloradapter = new ArrayAdapter(getActivity(),
                android.R.layout.simple_dropdown_item_1line, colorarray);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        tv_planname.setText(plan_details.get(position).getName());

        tv_netamounnt.setText("Your net Amount is $ " + netamount);

        et_type.setText(Preference.getCARTYPE());
//        Log.e(TAG, "findview: carwash ownerid:::::" + Util.carwashlistData.get(position).getCarwash_id());

    }


    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_layout, fragment)
                    .addToBackStack("")
                    .commit();
            return true;
        }
        return false;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llsubmit:

                name = et_name.getText().toString();
                model = et_model.getText().toString();
                type = et_type.getText().toString();
                color = et_color.getText().toString();

                if (name.isEmpty()) {
                    Util.ShowToastMsg(getActivity(), getString(R.string.ecname));
                } else if (model.isEmpty()) {
                    Util.ShowToastMsg(getActivity(), getString(R.string.emodel));
                } else if (type.isEmpty()) {
                    Util.ShowToastMsg(getActivity(), getString(R.string.ectype));
                } else if (color.isEmpty()) {
                    Util.ShowToastMsg(getActivity(), getString(R.string.eccolor));
                } else {
                    Preference.setCARMAKE(name);
                    Preference.setCARMODEL(model);
                    Preference.setCARTYPE(type);
                    Preference.setPLATENO(plateno);
                    Preference.setCOLOR(color);
                    text = "";
                    if (Preference.getCARDNO().equals("")) {
                        loadFragment(new PaymentInfoFragment(true, netamount));
                    } else {
                        dialogfunction();

                    }
//                    openDialog();
                }

                break;
            case R.id.et_name:

                if (SCarmMake.size() > 0) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Select Make")
                            .setAdapter(cmakeadapter, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    et_name.setText(SCarmMake.get(which));
                                    id = carmakedata.get(which).getId();
                                    dialog.dismiss();

                                    if (Util.isConnected(getActivity())) {
                                        getCarModel();
                                    } else {
                                        Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                                    }
                                }
                            }).create().show();


                } else {
                    if (Util.isConnected(getActivity())) {
                        getCarName();
                        if (SCarmMake.size() > 0) {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Select Make")
                                    .setAdapter(cmakeadapter, new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int which) {
                                            et_name.setText(SCarmMake.get(which));
                                            id = carmakedata.get(which).getId();
                                            dialog.dismiss();

                                            if (Util.isConnected(getActivity())) {
                                                getCarModel();
                                            } else {
                                                Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                                            }
                                        }
                                    }).create().show();


                        } else {
                            et_name.setText("No data");

                        }
                    } else {
                        Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                    }
                }
                break;
            case R.id.et_model:
                if (et_name.getText().toString().isEmpty()) {
                    Util.ShowToastMsg(getActivity(), getString(R.string.ecname));
                } else {

                    if (SCarmModel.size() > 0) {


                        new AlertDialog.Builder(getActivity())
                                .setTitle("Select Model")
                                .setAdapter(cmodeladapter, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        et_model.setText(SCarmModel.get(which));
                                        dialog.dismiss();
                                    }
                                }).create().show();
                    } else {
                        if (Util.isConnected(getActivity())) {
                            getCarModel();
                            if (SCarmModel.size() > 0) {
                                new AlertDialog.Builder(getActivity())
                                        .setTitle("Select Model")
                                        .setAdapter(cmodeladapter, new DialogInterface.OnClickListener() {

                                            public void onClick(DialogInterface dialog, int which) {
                                                et_model.setText(SCarmModel.get(which));
                                                dialog.dismiss();
                                            }
                                        }).create().show();


                            } else {
                                et_model.setText("No data");

                            }
                        } else {
                            Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                        }

                    }
                }
                break;
           /* case R.id.et_type:

                new AlertDialog.Builder(getActivity())
                        .setTitle("Select Model")
                        .setAdapter(ctypeadapter, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                et_type.setText(cartypearray[which]);
                                dialog.dismiss();
                            }
                        }).create().show();


                break;*/
            case R.id.et_color:

                new AlertDialog.Builder(getActivity())
                        .setTitle("Select Car Color")
                        .setAdapter(coloradapter, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                et_color.setText(colorarray[which]);
                                dialog.dismiss();
                            }
                        }).create().show();


                break;

        }
    }

    @Override
    public void onResume() {
        et_plate.setText("");
        super.onResume();
    }

    private void openDialog() {


        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.layout_dialog);

        final CustomRobotoEditText et_email = dialog.findViewById(R.id.et_email);
        final CustomRobotoEditText et_pwd = dialog.findViewById(R.id.et_pwd);
        CustomRobotoTextView tv_submit = dialog.findViewById(R.id.tv_submit);
        CustomRobotoTextView tv_title = dialog.findViewById(R.id.tv_title);

        tv_title.setText("Please enter your password to authenticate your account");

        tv_title.setVisibility(View.VISIBLE);
        et_pwd.setVisibility(View.VISIBLE);
        et_email.setVisibility(View.GONE);


        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String pwd = et_pwd.getText().toString();

                if (pwd.length() == 0) {
                    et_pwd.setError(getString(R.string.epwd));
                    et_pwd.requestFocus();
                } else {
                    if (Util.isConnected(getActivity())) {
                        CallLogin(pwd);
                    } else {
                        Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                    }


                }

            }
        });


        dialog.show();
    }

    private void CallLogin(String pwd) {
        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<LoginResponse> call = apiInterface.getLoginData(pwd, Preference.getMNO(), "android", Util.getUniqueId());
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();


                switch (status) {

                    case 1:
                        dialog.dismiss();
                        Preference.setAPI_TOKEN(response.body().getData().getApi_token());
                        Preference.setUSER_ID(response.body().getData().getUser_id());
                        Preference.setNAME(response.body().getData().getName());
                        Preference.setREFFERCODE(response.body().getData().getReffer_code());


                        text = "";
                        if (Preference.getCARDNO().equals("")) {
                            loadFragment(new PaymentInfoFragment(true, netamount));
                        } else {
                            dialogfunction();

                        }
//                        loadFragment(new PromocodeFragment(city, name, address, plan_details.get(position).getName()));
                        break;
                    case 0:
                        Util.ShowToastMsg(getActivity(), response.body().getMessage());
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Util.dismissCustomLoader();
                dialog.dismiss();
            }
        });
    }

    private void dialogfunction() {


        // custom dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radiobutton_paymentdialog);
        final RadioGroup rg = (RadioGroup) dialog.findViewById(R.id.radio_group);
        TextView tv_submit = dialog.findViewById(R.id.tv_submit);


        final RadioButton rb = new RadioButton(getActivity()); // dynamically creating RadioButton and adding to RadioGroup.
        rb.setText(Preference.getCARDNO().substring(0, Math.min(Preference.getCARDNO().length(), 2)) + "**************");
        rg.addView(rb);
        RadioButton rb1 = new RadioButton(getActivity()); // dynamically creating RadioButton and adding to RadioGroup.
        rb1.setText("Add new card");
        rg.addView(rb1);
        rb.setChecked(true);
        text = rb.getText().toString();
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                for (int i = 0; i < rg.getChildCount(); i++) {
                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                    if (btn.getId() == checkedId) {
                        text = btn.getText().toString();
                        // do something with text
                        return;
                    }
                }
            }
        });


        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (text.equals("")) {
                    Util.ShowToastMsg(getActivity(), "select any option");
                } else {
                    dialog.dismiss();
                    if (rg.getCheckedRadioButtonId() == rb.getId()) {
                        getPublicKey();

                    } else {
                        loadFragment(new PaymentInfoFragment(true, netamount));
                    }


                }
            }
        });

        dialog.show();

    }


    private void startTokenizing(String public_key) {

        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        Util.hideKeyboard(getActivity());
        Util.showCustomLoader(getActivity());
        HLTokenService service = new HLTokenService(public_key); // set the public here.
        HLCard card = new HLCard();
//        card.setNumber(et_cardno.getText().toString());
        String input = Preference.getCARDNO();
        input = input.replace(" ", "");
        Log.e("TAG", "startTokenizing: CARD:::" + input);
        card.setNumber(input);
        card.setExpMonth(Integer.parseInt(Preference.getMONTH()));
        card.setExpYear(Integer.parseInt(Preference.getYEAR()));
        card.setCvv(Preference.getCVV());

        service.getToken(card, new HLTokenService.TokenCallback() {
            @Override
            public void onComplete(HLToken response) {

                if (response == null || response.getError() != null) {
                    Util.dismissCustomLoader();
                    String message = (response == null) ? "Network Call Didnt result in status code 200" : response.getError().getMessage();
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

                } else {
                    Log.e("TAG", "onComplete: FINAL TOKEN :" + response.getTokenValue());
                    if (Util.isConnected(getActivity())) {
                        getPaymentData(response.getTokenValue(), netamount);
                    } else {
                        Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                    }

                }
            }
        });
    }

    private void getPaymentData(String tokenValue, float netamount) {
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Log.e(TAG, "getPaymentData: input parameter" + Preference.getAPI_TOKEN() + " " + tokenValue + " " + Preference.getCOLOR() + " " + String.valueOf(Preference.getCARWASHOWNERID()) + " " + String.valueOf(Preference.getPLANID()) + " " + Preference.getPROMOCODE() + " " + String.valueOf(netamount) + " " + Preference.getCARMAKE() + " " + Preference.getCARTYPE() + " " + Preference.getPLATENO());
        Call<PaymentResponse> call = apiInterface.getPaymentData(Preference.getAPI_TOKEN(),
                tokenValue, Preference.getCOLOR(), String.valueOf(Preference.getCARWASHOWNERID()), String.valueOf(Preference.getPLANID()), Preference.getPROMOCODE(), String.valueOf(netamount), Preference.getCARMAKE(), Preference.getCARMODEL(), Preference.getCARTYPE(), Preference.getPLATENO(), "0",Preference.getCARDNO(),Preference.getMONTH(),Preference.getYEAR(),Preference.getCVV());
        call.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();

                Util.ShowToastMsg(getActivity(), response.body().getMessage());

                switch (status) {
                    case 1:

                        Preference.setPROMOCODE("");
                        Preference.setPLATENO("");
                        Preference.setCARTYPE("");
                        ((MainActivity) getActivity()).bottomNavigationView.setSelectedItemId(R.id.nav_membership);

                        break;
                    case 0:
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;

                }
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                Util.dismissCustomLoader();


            }
        });
    }


    private void getPublicKey() {
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<PublicKeyResponse> call = apiInterface.get_public_key(Preference.getAPI_TOKEN(),
                String.valueOf(ownerid));
        call.enqueue(new Callback<PublicKeyResponse>() {
            @Override
            public void onResponse(Call<PublicKeyResponse> call, Response<PublicKeyResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();

//                Util.ShowToastMsg(getActivity(), response.body().getMessage());

                switch (status) {
                    case 1:
                        startTokenizing(response.body().getPublic_key());
                        break;
                    case 0:
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;

                }

                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


            }

            @Override
            public void onFailure(Call<PublicKeyResponse> call, Throwable t) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                Util.dismissCustomLoader();


            }
        });
    }
}
