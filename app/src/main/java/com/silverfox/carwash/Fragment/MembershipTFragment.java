package com.silverfox.carwash.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.EndlessRecyclerOnScrollListenerNewGrid;
import com.silverfox.carwash.Model.MembershipTListResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;
import com.silverfox.carwash.adapter.MembershipTadapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.share.internal.DeviceShareDialogFragment.TAG;

public class MembershipTFragment extends Fragment {


    View view;
    RecyclerView rv_membershipt;
    int currentpage = 0;
    ApiInterface apiInterface;
    TextView tv_nodata;
    ArrayList<MembershipTListResponse.DataBean> membershipTdata;
    LinearLayoutManager layoutManager;
    MembershipTadapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_membershipt, container, false);

        findview();


        if (Util.isConnected(getActivity())) {
            getMembershipT("0");
        } else {
            Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
        }

        rv_membershipt.setOnScrollListener(new EndlessRecyclerOnScrollListenerNewGrid(layoutManager) {
            @Override
            public void onLoadMore(int paramInt) {
//
//                Log.e("tag", "onLoadMore:" + paramInt);
//                Log.e("tag", "onLoadMore: +scroll");

                if (Util.isConnected(getActivity())) {
                    getMembershipT(String.valueOf(currentpage));
                } else {
                    Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                }
            }

        });


        return view;
    }

    private void findview() {
        rv_membershipt = view.findViewById(R.id.rv_membershipt);
        tv_nodata = view.findViewById(R.id.tv_nodata);

        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv_membershipt.setLayoutManager(layoutManager);
    }

    private void getMembershipT(final String pno) {
        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<MembershipTListResponse> call = apiInterface.getMemberShipTList(Preference.getAPI_TOKEN(), pno);
        call.enqueue(new Callback<MembershipTListResponse>() {
            @Override
            public void onResponse(Call<MembershipTListResponse> call, Response<MembershipTListResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data:history data " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();


                switch (status) {
                    case 1:

                        if (pno.equals("0")) {

                            membershipTdata = new ArrayList<>();
                            membershipTdata.clear();
                            membershipTdata.addAll(response.body().getData());

                            rv_membershipt.setVisibility(View.VISIBLE);
                            tv_nodata.setVisibility(View.GONE);

                            adapter = new MembershipTadapter(getActivity(), membershipTdata);
                            rv_membershipt.setAdapter(adapter);

                        } else {
                            membershipTdata.addAll(response.body().getData());

                        }

                        adapter.notifyDataSetChanged();
                        tv_nodata.setVisibility(View.GONE);
                        rv_membershipt.setVisibility(View.VISIBLE);
                        currentpage += 1;


                        break;


                    case 0:
                        if (pno.equals("0")) {

                            if (Util.carwashlistData.size() == 0) {
                                rv_membershipt.setVisibility(View.GONE);
                                tv_nodata.setVisibility(View.VISIBLE);


                            }


                        }
                        break;

                    case 404:

                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;
                }


            }

            @Override
            public void onFailure(Call<MembershipTListResponse> call, Throwable t) {

                Util.dismissCustomLoader();
                Log.e("TAG", "onFailure: historu" + t.getMessage());
            }
        });
    }


}
