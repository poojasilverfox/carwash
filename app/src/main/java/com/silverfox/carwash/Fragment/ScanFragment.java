package com.silverfox.carwash.Fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Activity.MainActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.Model.CancelPlanResponse;
import com.silverfox.carwash.Model.FreezeResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;
import com.silverfox.carwash.photoView.PhotoView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class ScanFragment extends Fragment implements View.OnClickListener {

    ArrayAdapter dateadapter;
    ArrayList<String> datelist = new ArrayList<>();
    View view;
    TextView tv_invitefrnd, tvTitleDiscount, tv_discount, tv_cardetails,
            tv_plandetails, tv_status, tv_editcard_details, tv_shopname,
            tv_planname, tv_address, tv_cancelplan, tv_frrezeplan, tvDays,
            tvPayThroughDate, tvNextDate;
    int position;
    PhotoView ivPhotoView;
    ImageView ivClose;

    ApiInterface apiInterface;
    String radiotext = "";
    String statuscancel;

    ImageView iv_scan;

    public ScanFragment(int position) {
        this.position = position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_scan, container, false);

        findview();

        ((MainActivity) getActivity()).iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return view;
    }

    private void findview() {

        ((MainActivity) getActivity()).iv_back.setVisibility(View.VISIBLE);

        tv_invitefrnd = view.findViewById(R.id.tv_invitefrnd);
        tv_editcard_details = view.findViewById(R.id.tv_editcard_details);
        ivClose = view.findViewById(R.id.ivClose);

        tv_shopname = view.findViewById(R.id.tv_shopname);
        tv_plandetails = view.findViewById(R.id.tv_plandetails);
        tv_planname = view.findViewById(R.id.tv_planname);
        tvTitleDiscount = view.findViewById(R.id.tvTitleDiscount);
        tv_address = view.findViewById(R.id.tv_address);
        tv_cancelplan = view.findViewById(R.id.tv_cancelplan);
        tv_frrezeplan = view.findViewById(R.id.tv_frrezeplan);
        tvDays = view.findViewById(R.id.tvDays);
        tvPayThroughDate = view.findViewById(R.id.tvPayThroughDate);
        tvNextDate = view.findViewById(R.id.tvNextDate);
        tv_cardetails = view.findViewById(R.id.tv_cardetails);
        tv_status = view.findViewById(R.id.tv_status);
        ivPhotoView = view.findViewById(R.id.ivPhotoView);
        iv_scan = view.findViewById(R.id.iv_scan);
        tv_discount = view.findViewById(R.id.tv_discount);
        tv_invitefrnd.setOnClickListener(this);
        iv_scan.setOnClickListener(this);
        tv_cancelplan.setOnClickListener(this);
        tv_frrezeplan.setOnClickListener(this);
        ivClose.setOnClickListener(this);
        tv_editcard_details.setOnClickListener(this);
        ivClose.setVisibility(View.GONE);
        ivPhotoView.setVisibility(View.GONE);
        if (MemberShipFragment.membershipList.get(position).getPlan_details().getDiscount().equals("")) {
            tv_discount.setVisibility(View.GONE);

        } else {
            tv_discount.setVisibility(View.VISIBLE);
        }

        tv_discount.setText(MemberShipFragment.membershipList.get(position).getPlan_details().getDiscount());


        Glide.with(getActivity())
//                .load(BuildConfig.BASE_URLFrames+frame_data.get(position).getLogo())
                .load(Preference.getQRCODE())
//                .placeholder(R.drawable.loader)
//                .error(R.drawable.ic_error)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(iv_scan);
        Glide.with(getActivity())
                .load(Preference.getQRCODE())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivPhotoView);

        tv_shopname.setText(MemberShipFragment.membershipList.get(position).getCarwash_owner_details().getShop_name());
        tv_planname.setText(MemberShipFragment.membershipList.get(position).getPlan_details().getName());
        tv_address.setText(MemberShipFragment.membershipList.get(position).getCarwash_owner_details().getFull_address());
        tv_plandetails.setText(MemberShipFragment.membershipList.get(position).getPlan_details().getDetails());
        tv_cardetails.setText(MemberShipFragment.membershipList.get(position).getCar_details().getCar_color() + "," + MemberShipFragment.membershipList.get(position).getCar_details().getCar_make() + "," + MemberShipFragment.membershipList.get(position).getCar_details().getModal());
        tv_status.setText(Preference.getSTATUS());
        tvDays.setText("You may wash your vehicle " +
                MemberShipFragment.membershipList.get(position).getCarwash_owner_details().getWorking_days() +
                " Through " + MemberShipFragment.membershipList.get(position).getCarwash_owner_details().getTo_working_day());

        tvPayThroughDate.setText(parseDateToddMMyyyy(MemberShipFragment.membershipList.get(position).getPay_trough_date()));
        tvNextDate.setText(parseDateToddMMyyyy(MemberShipFragment.membershipList.get(position).getNext_transaction_date()));
//        Log.e(TAG, "findview: status::::::"+Preference.getSTATUS() );

        if (MemberShipFragment.membershipList.get(position).getIs_freeze() == 1) {
//
//            tv_frrezeplan.setText("Unfreeze Plan " + MemberShipFragment.membershipList.get(position).getFreeze_date());
//            tv_frrezeplan.setClickable(false);
//            tv_frrezeplan.setFocusableInTouchMode(false);
            tv_frrezeplan.setVisibility(View.GONE);
            tv_cancelplan.setVisibility(View.GONE);
        }
//
//        Log.e(TAG, "findview: VB CANCEL:::" + MemberShipFragment.membershipList.get(position).getIs_cancel());
//        Log.e(TAG, "findview: VB getIs_failed:::" + MemberShipFragment.membershipList.get(position).getIs_failed());
//        Log.e(TAG, "findview: VB getIs_freeze:::" + MemberShipFragment.membershipList.get(position).getIs_freeze());
//        Log.e(TAG, "findview: VB CANCEL:::" + MemberShipFragment.membershipList.get(position).getIs_cancel());
        if (MemberShipFragment.membershipList.get(position).getIs_cancel() == 1) {
            iv_scan.setVisibility(View.GONE);
//            tv_cancelplan.setText("uncancel");
//            tv_cancelplan.setVisibility(View.VISIBLE);
        }
        if (MemberShipFragment.membershipList.get(position).getIs_cancel() == 0) {
            statuscancel = "1";
            tv_cancelplan.setText("cancel");
            tv_cancelplan.setVisibility(View.VISIBLE);
        } else {
            statuscancel = "0";
            tv_cancelplan.setText("uncancel");
            tv_cancelplan.setVisibility(View.GONE);
            tv_frrezeplan.setVisibility(View.GONE);
            String date = MemberShipFragment.membershipList.get(position).getPay_trough_date();
            Log.e(TAG, "findview:VB CANCELLED:::" + date);
            try {
                Date todayDate = Calendar.getInstance().getTime();
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                String todayString = formatter.format(todayDate);
                if (formatter.parse(date).before(todayDate)) {
                    iv_scan.setVisibility(View.GONE);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (MemberShipFragment.membershipList.get(position).getFreeze_select_dates().size() > 0) {
            datelist.addAll(MemberShipFragment.membershipList.get(position).getFreeze_select_dates());
            dateadapter = new ArrayAdapter(getActivity(),
                    android.R.layout.simple_dropdown_item_1line, datelist);
        }

        if (MemberShipFragment.membershipList.get(position).getDeactive_plan() == 1 ||
                MemberShipFragment.membershipList.get(position).getIs_failed() == 1 ||
                MemberShipFragment.membershipList.get(position).getInactive_plan() == 1) {
            tv_cancelplan.setVisibility(View.GONE);
            tv_frrezeplan.setVisibility(View.GONE);

        }

        if (MemberShipFragment.membershipList.get(position).getShow_cancel() == 1) {
            tv_cancelplan.setVisibility(View.VISIBLE);
        } else {
            tv_cancelplan.setVisibility(View.GONE);
        }

    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_scan:
                ivPhotoView.setVisibility(View.VISIBLE);
                ivClose.setVisibility(View.VISIBLE);
                break;
            case R.id.ivClose:
                ivPhotoView.setVisibility(View.GONE);
                ivClose.setVisibility(View.GONE);
                break;
            case R.id.tv_invitefrnd:

                break;
            case R.id.tv_editcard_details:

                break;

            case R.id.tv_cancelplan:
                if (statuscancel.equals("1")) {
                    Dialogfunction("Are you sure cancel the plan ?", true);
                } else {
                    Dialogfunction("Do want to sure uncancel the plan ?", true);

                }

                break;
            case R.id.tv_frrezeplan:

                showRadioButtonDialog();
               /* new AlertDialog.Builder(getActivity())
                        .setTitle("Select Make")
                        .setAdapter(dateadapter, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                tv_frrezeplan.setText(datelist.get(which));
                                dialog.dismiss();
                            }
                        }).create().show();
*/
                break;
        }
    }

    private void Dialogfunction(String title, final Boolean flag) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();

        if (flag) {
            alertDialog.setMessage("If you will cancel this membership plan now, then you will not be able to register for a new plan for next 6 months.\n\n" + title);
        } else {
            alertDialog.setMessage(title);


        }
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (flag) {


                            if (Util.isConnected(getActivity())) {
                                CancelPlan();
                                dialog.dismiss();
                            } else {
                                Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                            }

                        } else {

                            if (Util.isConnected(getActivity())) {
                                FreezeData();
                                dialog.dismiss();
                            } else {
                                Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                            }

                        }

                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    private void showRadioButtonDialog() {

        radiotext = "";

        // custom dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radiobutton_dialog);
        final RadioGroup rg = (RadioGroup) dialog.findViewById(R.id.radio_group);
        TextView tv_submit = dialog.findViewById(R.id.tv_submit);

        for (int i = 0; i < datelist.size(); i++) {
            RadioButton rb = new RadioButton(getActivity()); // dynamically creating RadioButton and adding to RadioGroup.
            rb.setText(datelist.get(i));
            rg.addView(rb);
        }


        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                for (int i = 0; i < rg.getChildCount(); i++) {
                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                    if (btn.getId() == checkedId) {
                        radiotext = btn.getText().toString();
                        // do something with text
                        return;
                    }
                }
            }
        });


        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radiotext.equals("")) {
                    Util.ShowToastMsg(getActivity(), "select any date");
                } else {
                    dialog.dismiss();

                    Dialogfunction("Do you want to sure to freeze the plan ?", false);

                }
            }
        });

        dialog.show();

    }

    private void FreezeData() {
        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<FreezeResponse> call = apiInterface.getFreezeData(Preference.getAPI_TOKEN(), String.valueOf(MemberShipFragment.membershipList.get(position).getMembership_id()), radiotext);
        call.enqueue(new Callback<FreezeResponse>() {
            @Override
            public void onResponse(Call<FreezeResponse> call, Response<FreezeResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));


                Util.ShowToastMsg(getActivity(), response.body().getMessage());
                int status = response.body().getStatus();

                switch (status) {
                    case 1:
                        tv_frrezeplan.setText("Unfreeze until " + response.body().getData().getFreeze_date());
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;
                }

            }

            @Override
            public void onFailure(Call<FreezeResponse> call, Throwable t) {
                Util.dismissCustomLoader();
            }
        });
    }

    private void CancelPlan() {
        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<CancelPlanResponse> call = apiInterface.getCancelPlanData(Preference.getAPI_TOKEN(), String.valueOf(MemberShipFragment.membershipList.get(position).getMembership_id()), statuscancel);
        call.enqueue(new Callback<CancelPlanResponse>() {
            @Override
            public void onResponse(Call<CancelPlanResponse> call, Response<CancelPlanResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));


                Util.ShowToastMsg(getActivity(), response.body().getMessage());
                int status = response.body().getStatus();

                switch (status) {
                    case 1:
                        if (response.body().getData().getIs_cancel().equals("0")) {
                            statuscancel = "1";
                            tv_cancelplan.setText("cancel");
                        } else {
                            statuscancel = "0";
                            tv_cancelplan.setVisibility(View.GONE);
                            tv_cancelplan.setText("uncancel");
                        }
                        getActivity().onBackPressed();
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;
                }

            }

            @Override
            public void onFailure(Call<CancelPlanResponse> call, Throwable t) {
                Util.dismissCustomLoader();
            }
        });
    }
}
