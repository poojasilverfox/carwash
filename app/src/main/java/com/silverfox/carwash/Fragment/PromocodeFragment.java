package com.silverfox.carwash.Fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Activity.MainActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.Model.PaymentResponse;
import com.silverfox.carwash.Model.WalletUpdateResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;
import com.silverfox.carwash.service.HLCard;
import com.silverfox.carwash.service.HLToken;
import com.silverfox.carwash.service.HLTokenService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PromocodeFragment extends Fragment implements View.OnClickListener {



    private  View view;
    private  LinearLayout llsubmit, ll_promocode, ll_promocodetext, ll_price, ll_wprice, ll_nprice;
    private  TextView tv_amount, tv_apply, tv_newprice, tv_name, tv_platename, tv_planname, tv_address, tv_wamount, tv_namount;
    private  String promocode = "", city = "", name = "", address = "", planname = "";
    private  EditText et_promocode;
    private  ApiInterface apiInterface;
    private  String text ="",tokenizationamount="";

    public PromocodeFragment(String city, String name, String address, String planname) {

        this.city = city;
        this.name = name;
        this.address = address;
        this.planname = planname;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_promocode, container, false);


        findView();

        if (Util.isConnected(getActivity())) {
            getWalletData();

            tv_amount.setText("$ " + Preference.getWALLET());
            tv_wamount.setText("$ " + Preference.getWALLET());

            if (Preference.getWALLET() > 0) {
                tv_namount.setText("$ " + (Integer.valueOf(Preference.getPRICE()) - Preference.getWALLET()));
            } else {
                tv_namount.setText("$ " + Preference.getPRICE());
            }
        }else{
            Util.ShowToastMsg(getActivity(),getString(R.string.noconn));
        }




        ((MainActivity) getActivity()).iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();
            }
        });

        return view;
    }

    private void getWalletData() {
        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<WalletUpdateResponse> call = apiInterface.getWalletData(Preference.getAPI_TOKEN());
        call.enqueue(new Callback<WalletUpdateResponse>() {
            @Override
            public void onResponse(Call<WalletUpdateResponse> call, Response<WalletUpdateResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));


                int status = response.body().getStatus();
                switch (status) {
                    case 1:
                        Preference.setWALLET(response.body().getData());
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(),LoginActivity.class);
                        intent.putExtra("type","login");
                        startActivity(intent);
                        getActivity().finish();
                        break;


                }


            }

            @Override
            public void onFailure(Call<WalletUpdateResponse> call, Throwable t) {
                Util.dismissCustomLoader();
            }
        });
    }


    private void findView() {

        ((MainActivity) getActivity()).iv_back.setVisibility(View.VISIBLE);

        llsubmit = view.findViewById(R.id.llsubmit);
        ll_promocode = view.findViewById(R.id.ll_promocode);
        ll_promocodetext = view.findViewById(R.id.ll_promocodetext);
        ll_price = view.findViewById(R.id.ll_price);
        tv_amount = view.findViewById(R.id.tv_amount);
        tv_wamount = view.findViewById(R.id.tv_wamount);
        tv_namount = view.findViewById(R.id.tv_namount);
        tv_apply = view.findViewById(R.id.tv_apply);
        et_promocode = view.findViewById(R.id.et_promocode);
        tv_newprice = view.findViewById(R.id.tv_newprice);
        tv_name = view.findViewById(R.id.tv_name);
        tv_platename = view.findViewById(R.id.tv_platename);
        tv_planname = view.findViewById(R.id.tv_planname);
        tv_address = view.findViewById(R.id.tv_address);
        llsubmit.setOnClickListener(this);
        tv_apply.setOnClickListener(this);



        tv_name.setText(name);
        tv_platename.setText(Preference.getPLATENO());
        tv_address.setText(address);
        tv_planname.setText(planname);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.llsubmit:
                tokenizationamount = tv_namount.getText().toString().replace("$", "");
                text ="";
                if (Preference.getCARDNO().equals("")) {
                    loadFragment(new PaymentInfoFragment(true,Float.valueOf(tokenizationamount)));
                } else {
                    dialogfunction();

                }
                break;
            case R.id.tv_apply:
                promocode = et_promocode.getText().toString().trim();
                if (promocode.isEmpty()) {
                    et_promocode.setError(getString(R.string.epcode));
                    et_promocode.requestFocus();
                } else {
                    if (Util.isConnected(getActivity())) {
                        Preference.setPROMOCODE(promocode);
                        PromocodeData();
                    } else {
                        Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                    }
                }

        }

    }

    private void dialogfunction() {


        // custom dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radiobutton_paymentdialog);
        final RadioGroup rg = (RadioGroup) dialog.findViewById(R.id.radio_group);
        TextView tv_submit = dialog.findViewById(R.id.tv_submit);


        final RadioButton rb = new RadioButton(getActivity()); // dynamically creating RadioButton and adding to RadioGroup.
        rb.setText(Preference.getCARDNO().substring(0, Math.min(Preference.getCARDNO().length(), 2)) + "**************");
        rg.addView(rb);
        RadioButton rb1 = new RadioButton(getActivity()); // dynamically creating RadioButton and adding to RadioGroup.
        rb1.setText("Add Using another card");
        rg.addView(rb1);


        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                for (int i = 0; i < rg.getChildCount(); i++) {
                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                    if (btn.getId() == checkedId) {
                        text = btn.getText().toString();
                        // do something with text
                        return;
                    }
                }
            }
        });


        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (text.equals("")) {
                    Util.ShowToastMsg(getActivity(), "select any option");
                } else {
                    dialog.dismiss();
                    if(rg.getCheckedRadioButtonId() == rb.getId()){
                        startTokenizing();
                    }else{
                        loadFragment(new PaymentInfoFragment(true,Float.valueOf(tokenizationamount)));
                    }



                }
            }
        });

        dialog.show();

    }


    private void startTokenizing() {

        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        Util.hideKeyboard(getActivity());
        Util.showCustomLoader(getActivity());
        HLTokenService service = new HLTokenService("pkapi_cert_zKFO423iJn1V0p3RNi"); // set the public here.
        HLCard card = new HLCard();
//        card.setNumber(et_cardno.getText().toString());
        String input = Preference.getCARDNO();
        input = input.replace(" ", "");
        Log.e("TAG", "startTokenizing: CARD:::" + input);
        card.setNumber(input);
        card.setExpMonth(Integer.parseInt(Preference.getMONTH()));
        card.setExpYear(Integer.parseInt(Preference.getYEAR()));
        card.setCvv(Preference.getCVV());

        service.getToken(card, new HLTokenService.TokenCallback() {
            @Override
            public void onComplete(HLToken response) {

                if (response == null || response.getError() != null) {
                    String message = (response == null) ? "Network Call Didnt result in status code 200" : response.getError().getMessage();
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                } else {
                    Log.e("TAG", "onComplete: FINAL TOKEN :" + response.getTokenValue());
                    if (Util.isConnected(getActivity())) {
                        getPaymentData(response.getTokenValue(),tokenizationamount );
                    } else {
                        Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                    }

                }
            }
        });
    }

    private void getPaymentData(String tokenValue, String amount) {
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<PaymentResponse> call = apiInterface.getPaymentData(Preference.getAPI_TOKEN(), tokenValue,Preference.getCOLOR(), String.valueOf(Preference.getCARWASHOWNERID()), String.valueOf(Preference.getPLANID()), Preference.getPROMOCODE(), Preference.getPRICE(), Preference.getCARMAKE(), Preference.getCARMODEL(), Preference.getTYPE(), Preference.getPLATENO(),"0",Preference.getCARDNO(),Preference.getMONTH(),Preference.getYEAR(),Preference.getCVV());
        call.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
                Util.dismissCustomLoader();
                Log.e("hellooooooo", "data: " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();

                Util.ShowToastMsg(getActivity(), response.body().getMessage());

                switch (status) {
                    case 1:
                        ((MainActivity) getActivity()).bottomNavigationView.setSelectedItemId(R.id.nav_membership);
                        break;
                    case 0:
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(),LoginActivity.class);
                        intent.putExtra("type","login");
                        startActivity(intent);
                        getActivity().finish();
                        break;


                }
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                Util.dismissCustomLoader();


            }
        });
    }


    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_layout, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    private void PromocodeData() {
       /* Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<PromocodeResponse> call = apiInterface.getPromocodeData(Preference.getAPI_TOKEN(), promocode);
        call.enqueue(new Callback<PromocodeResponse>() {
            @Override
            public void onResponse(Call<PromocodeResponse> call, Response<PromocodeResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();
                switch (status) {
                    case 1:
                        ll_promocode.setVisibility(View.GONE);

                        ll_promocodetext.setVisibility(View.VISIBLE);
                    *//*    tv_newprice.setText("Your First month price is " + response.body().getData().getNew_price());

                        Preference.setPRICE(String.valueOf(response.body().getData().getNew_price()));*//*
                        tv_amount.setText("$ " + Preference.getPRICE());
                        tv_wamount.setText("$ " + Preference.getWALLET());

                        if (Preference.getWALLET() > 0) {
                            tv_namount.setText("$ " + (Integer.valueOf(Preference.getPRICE()) - Preference.getWALLET()));
                        } else {
                            tv_namount.setText("$ " + Preference.getPRICE());
                        }
                        break;
                    case 0:

                        Util.ShowToastMsg(getActivity(), response.body().getMessage());
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(),LoginActivity.class);
                        intent.putExtra("type","login");
                        startActivity(intent);
                        getActivity().finish();
                        break;
                }


            }

            @Override
            public void onFailure(Call<PromocodeResponse> call, Throwable t) {
                Util.dismissCustomLoader();
            }
        });*/
    }
}
