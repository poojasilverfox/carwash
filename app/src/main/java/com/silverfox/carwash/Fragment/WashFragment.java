package com.silverfox.carwash.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Activity.MainActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.Model.CarModelResponse;
import com.silverfox.carwash.Model.CarSizeResponse;
import com.silverfox.carwash.Model.PromocodeResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;
import com.silverfox.carwash.adapter.CarWashListAdapter;
import com.silverfox.carwash.adapter.PlanListAdapter;

import java.util.ArrayList;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class WashFragment extends Fragment implements PlanListAdapter.loadFrgment {

    RecyclerView rv_plan_list;
    private LinearLayoutManager layoutManager;
    private PlanListAdapter planListAdapter;
    ImageView iv_image;
    TextView tv_name, tv_address, tv_city, tv_distance;
    private int position;
    EditText et_model;
    View view;
    EditText et_promocode;
    ApiInterface apiInterface;
    WashFragment washFragment;
    public static ArrayList<String> pcodearray = new ArrayList<>();

    public static Activity activity;
    public static ArrayList<CarSizeResponse.InsideTunnelDataBean> insidecarsizelist = new ArrayList<>();
    public static ArrayList<CarSizeResponse.OutsideTunnelDataBean> outsidecarsizelist = new ArrayList<>();
    public static ArrayList<CarSizeResponse.NormalTypeDataBean> normalsizeList = new ArrayList<>();
    int[] array = new int[4];


    public WashFragment(int position) {
        this.position = position;
    }

    public static CarinsidesizeAdapter adapterinside;
    public static CaroutsidesizeAdapter adapteroutside;
    public static CarnormalsizeAdapter adapternormal;


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_wash, container, false);


        findView(view);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);


        if (Util.isConnected(getActivity())) {
            getCarTypeData();

        } else {
            Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
        }



        int id = 0;
        id = getResources().getColor(R.color.pink);

        array[0] = id;
        id = getResources().getColor(R.color.yallow);

        array[1] = id;
        id = getResources().getColor(R.color.bllue);

        array[2] = id;
        id = getResources().getColor(R.color.green);

        array[3] = id;


        ((MainActivity) getActivity()).iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });


        return view;
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .hide(washFragment)
                    .add(R.id.frame_layout, fragment)
                    .addToBackStack("")
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit();
            return true;
        }
        return false;
    }

    private void getCarTypeData() {
        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);

        Log.e(TAG, "getCarTypeData: parameter data"+Preference.getAPI_TOKEN()+"id::::"+Util.carwashlistData.get(position).getCarwash_id());
        Call<CarSizeResponse> call = apiInterface.getTypeData(Preference.getAPI_TOKEN(), String.valueOf(Util.carwashlistData.get(position).getCarwash_id()));
        call.enqueue(new Callback<CarSizeResponse>() {
            @Override
            public void onResponse(Call<CarSizeResponse> call, Response<CarSizeResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));


                int status = response.body().getStatus();
                switch (status) {
                    case 1:


                        insidecarsizelist = new ArrayList<>();
                        outsidecarsizelist = new ArrayList<>();
                        normalsizeList = new ArrayList<>();
                        insidecarsizelist.addAll(response.body().getInside_tunnel_data());
                        outsidecarsizelist.addAll(response.body().getOutside_tunnel_data());
                        normalsizeList.addAll(response.body().getNormal_type_data());

                        adapterinside = new CarinsidesizeAdapter(insidecarsizelist);
                        adapteroutside = new CaroutsidesizeAdapter(outsidecarsizelist);
                        adapternormal = new CarnormalsizeAdapter(normalsizeList);

                        for(int i = 0; i < Util.carwashlistData.get(position).getPlan_details().size() ; i++ ){
                            pcodearray.add("true");
                        }

                        planListAdapter = new PlanListAdapter(getActivity(), RandomizeArray(array), Util.carwashlistData.get(position).getPlan_details(),Util.carwashlistData.get(position).getCarwash_id(), WashFragment.this);
                        rv_plan_list.setAdapter(planListAdapter);
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;

                }


            }

            @Override
            public void onFailure(Call<CarSizeResponse> call, Throwable t) {
                Util.dismissCustomLoader();
            }
        });
    }


    @Override
    public void loadFragm(int intposition, float amount) {
        loadFragment(new CarDetailsFragment(intposition, Util.carwashlistData.get(position).getPlan_details(),Util.carwashlistData.get(position).getCarwash_id(), Util.carwashlistData.get(position).getShop_name(), Util.carwashlistData.get(position).getCity(), Util.carwashlistData.get(position).getFull_address(), amount));

    }


    public static int[] RandomizeArray(int[] array) {
        Random rgen = new Random();  // Random number generator

        for (int i = 0; i < array.length; i++) {
            int randomPosition = rgen.nextInt(array.length);
            int temp = array[i];
            array[i] = array[randomPosition];
            array[randomPosition] = temp;
        }

        return array;
    }

    private void findView(View view) {
        rv_plan_list = view.findViewById(R.id.rv_plan_list);
        iv_image = view.findViewById(R.id.iv_image);
        tv_name = view.findViewById(R.id.tv_name);
        tv_address = view.findViewById(R.id.tv_address);
        tv_city = view.findViewById(R.id.tv_city);
        tv_distance = view.findViewById(R.id.tv_distance);
        et_promocode = view.findViewById(R.id.et_promocode);
        et_model = view.findViewById(R.id.et_model);

        activity = getActivity();
        washFragment = WashFragment.this;
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv_plan_list.setLayoutManager(layoutManager);

        rv_plan_list.setFocusable(false);
        ((MainActivity) getActivity()).iv_back.setVisibility(View.VISIBLE);

        tv_name.setText(Util.carwashlistData.get(position).getShop_name());
        tv_address.setText(Util.carwashlistData.get(position).getFull_address());
        tv_city.setText(Util.carwashlistData.get(position).getCity());
        tv_distance.setText(Util.carwashlistData.get(position).getDistance() + "");

        Glide.with(getActivity())
                .load(Util.carwashlistData.get(position).getLogo())
                .priority(Priority.HIGH)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(iv_image);


    }


    private class CarinsidesizeAdapter extends BaseAdapter {

        LayoutInflater inflter;
        ArrayList<CarSizeResponse.InsideTunnelDataBean> insidecarsizelist;

        public CarinsidesizeAdapter(ArrayList<CarSizeResponse.InsideTunnelDataBean> insidecarsizelist) {
            inflter = (LayoutInflater.from(getActivity()));
            this.insidecarsizelist = insidecarsizelist;

        }

        @Override
        public int getCount() {
            return insidecarsizelist.size();
        }

        @Override
        public Object getItem(int position) {
            return insidecarsizelist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.row_size_list, null);//set layout for displaying items
            TextView tv_name = view.findViewById(R.id.tv_name);//get id for image view
            TextView tv_amount = view.findViewById(R.id.tv_amount);//get id for image view


            tv_name.setText(insidecarsizelist.get(i).getName());
            tv_amount.setText("$ " + insidecarsizelist.get(i).getAmount());
            return view;
        }
    }

    private class CaroutsidesizeAdapter extends BaseAdapter {

        LayoutInflater inflter;
        ArrayList<CarSizeResponse.OutsideTunnelDataBean> outsidecarsizelist;

        public CaroutsidesizeAdapter(ArrayList<CarSizeResponse.OutsideTunnelDataBean> outsidecarsizelist) {
            inflter = (LayoutInflater.from(getActivity()));
            this.outsidecarsizelist = outsidecarsizelist;

        }

        @Override
        public int getCount() {
            return outsidecarsizelist.size();
        }

        @Override
        public Object getItem(int position) {
            return outsidecarsizelist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.row_size_list, null);//set layout for displaying items
            TextView tv_name = view.findViewById(R.id.tv_name);//get id for image view
            TextView tv_amount = view.findViewById(R.id.tv_amount);//get id for image view


            tv_name.setText(outsidecarsizelist.get(i).getName());
            tv_amount.setText("$ " + outsidecarsizelist.get(i).getAmount());
            return view;
        }
    }

    private class CarnormalsizeAdapter extends BaseAdapter {

        LayoutInflater inflter;
        ArrayList<CarSizeResponse.NormalTypeDataBean> normalsizeList;

        public CarnormalsizeAdapter(ArrayList<CarSizeResponse.NormalTypeDataBean> normalsizeList) {
            inflter = (LayoutInflater.from(getActivity()));
            this.normalsizeList = normalsizeList;

        }

        @Override
        public int getCount() {
            return normalsizeList.size();
        }

        @Override
        public Object getItem(int position) {
            return normalsizeList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.row_size_list, null);//set layout for displaying items
            TextView tv_name = view.findViewById(R.id.tv_name);//get id for image view
            TextView tv_amount = view.findViewById(R.id.tv_amount);//get id for image view


            tv_name.setText(normalsizeList.get(i).getName());
            tv_amount.setText("$ " + normalsizeList.get(i).getAmount());
            return view;
        }
    }


}
