package com.silverfox.carwash.Fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.silverfox.carwash.Activity.MainActivity;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;
import com.silverfox.carwash.adapter.MyAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class HistoryFragment extends Fragment implements View.OnClickListener {


    View view;
    TextView tv_walletT, tv_membershipT;
    ViewPager viewPager;
    MyAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_history, container, false);

        findview();

        adapter = new MyAdapter(getActivity(), getActivity().getSupportFragmentManager(), 1);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position);

                tv_walletT.setTextColor(getResources().getColor(R.color.colorAccent));
                tv_membershipT.setTextColor(getResources().getColor(R.color.white));
                tv_walletT.setBackgroundColor(getResources().getColor(R.color.white));
                tv_membershipT.setBackgroundColor(getResources().getColor(R.color.colorAccent));
               /* if(position == 0){
                    tv_walletT.setTextColor(getResources().getColor(R.color.white));
                    tv_membershipT.setTextColor(getResources().getColor(R.color.colorAccent));
                    tv_walletT.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    tv_membershipT.setBackgroundColor(getResources().getColor(R.color.white));
                }else{

                }*/
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        ((MainActivity) getActivity()).iv_back.setVisibility(View.VISIBLE);

        ((MainActivity) getActivity()).iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();
            }
        });


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    private void findview() {
        tv_walletT = view.findViewById(R.id.tv_walletT);
        tv_membershipT = view.findViewById(R.id.tv_membershipT);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);

        tv_walletT.setOnClickListener(this);
        tv_membershipT.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_walletT:
                viewPager.setCurrentItem(0);
                tv_walletT.setTextColor(getResources().getColor(R.color.white));
                tv_membershipT.setTextColor(getResources().getColor(R.color.colorAccent));
                tv_walletT.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                tv_membershipT.setBackgroundColor(getResources().getColor(R.color.white));
                break;
            case R.id.tv_membershipT:
                viewPager.setCurrentItem(1);
                tv_walletT.setTextColor(getResources().getColor(R.color.colorAccent));
                tv_membershipT.setTextColor(getResources().getColor(R.color.white));
                tv_walletT.setBackgroundColor(getResources().getColor(R.color.white));
                tv_membershipT.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                break;
        }
    }
}
