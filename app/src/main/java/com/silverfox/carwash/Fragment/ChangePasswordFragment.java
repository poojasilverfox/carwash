package com.silverfox.carwash.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Activity.MainActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.Model.UpdateProfileResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordFragment extends Fragment implements View.OnClickListener {


    View view;
    ApiInterface apiInterface;

    EditText et_old, et_new, et_confirm;
    TextView tv_update;
    String oldPass = "", newPass = "", confirmPass = "";
    private WebView mWebView;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.layout_change_password, container, false);

        findView();
        ((MainActivity) getActivity()).iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();
            }
        });


        return view;
    }


    private void findView() {


        ((MainActivity) getActivity()).iv_back.setVisibility(View.VISIBLE);
//        et_old = view.findViewById(R.id.et_old);
//        et_new = view.findViewById(R.id.et_new);
//        et_confirm = view.findViewById(R.id.et_confirm);
//        tv_update = view.findViewById(R.id.tv_update);
//        tv_update.setOnClickListener(this);

        mWebView = view.findViewById(R.id.webview);
        mWebView.loadUrl("https://yourhomewash.com/vb/yourhomewash/password/reset");

        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setSupportZoom(false);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setDomStorageEnabled(true);

        Util.showCustomLoader(getActivity());

        // Force links and redirects to open in the WebView instead of in a browser
        mWebView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                Util.dismissCustomLoader();
            }
        });



    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_update) {
            oldPass = et_old.getText().toString();
            newPass = et_new.getText().toString();
            confirmPass = et_confirm.getText().toString();

            if (oldPass.isEmpty()) {
                et_old.setError(getString(R.string.e_old_pass));
                et_old.requestFocus();
            } else if (newPass.isEmpty()) {
                et_new.setError(getString(R.string.e_new_pass));
                et_new.requestFocus();
            } else if (confirmPass.isEmpty()) {
                et_confirm.setError(getString(R.string.e_confirm_pass));
                et_confirm.requestFocus();
            } else if (!confirmPass.equals(newPass)) {
                et_confirm.setError(getString(R.string.e_confirm_pass_same));
                et_confirm.requestFocus();
            } else {
//                UpdateProfile();
                Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void UpdateProfile() {
        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<UpdateProfileResponse> call = apiInterface.getChangePassword(Preference.getAPI_TOKEN(), oldPass, newPass);
        call.enqueue(new Callback<UpdateProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));

                Util.ShowToastMsg(getActivity(), response.body().getMessage());

                int status = response.body().getStatus();
                switch (status) {
                    case 1:
                        Preference.setNAME(response.body().getData().getName());
                        Preference.setLNAME(response.body().getData().getLast_name());
                        Preference.setEMAIL(response.body().getData().getEmail());
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;

                }


            }

            @Override
            public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                Util.dismissCustomLoader();

            }
        });
    }


}
