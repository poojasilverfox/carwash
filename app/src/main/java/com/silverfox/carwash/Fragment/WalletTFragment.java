package com.silverfox.carwash.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Activity.MainActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.EndlessRecyclerOnScrollListenerNewGrid;
import com.silverfox.carwash.Model.CarWashListResponse;
import com.silverfox.carwash.Model.WalletTransactionResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;
import com.silverfox.carwash.adapter.CarWashListAdapter;
import com.silverfox.carwash.adapter.MyAdapter;
import com.silverfox.carwash.adapter.WalletTadapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletTFragment extends Fragment {


    View view;
    RecyclerView rv_wallett;
    int currentpage = 0;
    ApiInterface apiInterface;
    TextView tv_nodata;
    ArrayList<WalletTransactionResponse.DataBean> walletTdata;
    LinearLayoutManager layoutManager;
    WalletTadapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_wallett, container, false);

        findview();


        if (Util.isConnected(getActivity())) {

            getWalletT(currentpage);
        } else {
            Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
        }

        rv_wallett.setOnScrollListener(new EndlessRecyclerOnScrollListenerNewGrid(layoutManager) {
            @Override
            public void onLoadMore(int paramInt) {

                Log.e("tag", "onLoadMore:" + paramInt);
                Log.e("tag", "onLoadMore: +scroll");


                if (Util.isConnected(getActivity())) {
                    getWalletT(currentpage);
                } else {
                    Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                }
            }

        });


        return view;
    }

    private void findview() {
        rv_wallett = view.findViewById(R.id.rv_wallett);
        tv_nodata = view.findViewById(R.id.tv_nodata);


        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv_wallett.setLayoutManager(layoutManager);
    }

    private void getWalletT(final int pno) {


        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<WalletTransactionResponse> call = apiInterface.getWalletTData(Preference.getAPI_TOKEN(), pno);
        call.enqueue(new Callback<WalletTransactionResponse>() {
            @Override
            public void onResponse(Call<WalletTransactionResponse> call, Response<WalletTransactionResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();


                switch (status) {
                    case 1:


                        if (pno==0) {
                            walletTdata = new ArrayList<>();
                            walletTdata.clear();
                            walletTdata.addAll(response.body().getData());

                            rv_wallett.setVisibility(View.VISIBLE);
                            tv_nodata.setVisibility(View.GONE);

                            adapter = new WalletTadapter(getActivity(), walletTdata);
                            rv_wallett.setAdapter(adapter);

                        } else {
                            walletTdata.addAll(response.body().getData());

                        }

                        adapter.notifyDataSetChanged();
                        tv_nodata.setVisibility(View.GONE);
                        rv_wallett.setVisibility(View.VISIBLE);
                        currentpage += 1;


                        break;


                    case 0:
                        if (pno==0) {
                            if (Util.carwashlistData.size() == 0) {
                                rv_wallett.setVisibility(View.GONE);
                                tv_nodata.setVisibility(View.VISIBLE);
                            }

                        }
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;

                }


            }

            @Override
            public void onFailure(Call<WalletTransactionResponse> call, Throwable t) {

                Util.dismissCustomLoader();
            }
        });
    }


}
