package com.silverfox.carwash.Fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.silverfox.carwash.Activity.MainActivity;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;

public class InviteFriendFragment extends Fragment implements View.OnClickListener {


    View view;
    TextView tv_refer_code, tv_more;
    LinearLayout ll_msg, ll_email, ll_fb, ll_twitter;


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragmnet_frnd, container, false);

        findView();


        ((MainActivity) getActivity()).iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();


            }
        });


        return view;
    }


    private void findView() {

        tv_refer_code = view.findViewById(R.id.tv_refer_code);
        ll_msg = view.findViewById(R.id.ll_msg);
        ll_email = view.findViewById(R.id.ll_email);
        ll_fb = view.findViewById(R.id.ll_fb);
        ll_twitter = view.findViewById(R.id.ll_twitter);
        tv_more = view.findViewById(R.id.tv_more);
        ((MainActivity) getActivity()).iv_back.setVisibility(View.VISIBLE);

        tv_refer_code.setText(Preference.getREFFERCODE());

        ll_msg.setOnClickListener(this);
        ll_email.setOnClickListener(this);
        ll_fb.setOnClickListener(this);
        ll_twitter.setOnClickListener(this);
        tv_more.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
//        String message = "Use code " + Preference.getREFFERCODE() + " to get 50% off your 1st month to any unlimited car wash membership with HomeWash. https://silverfoxstudio.in/car/register?code=" + Preference.getREFFERCODE();
        String message = "Use code " + Preference.getREFFERCODE() + " to get 10% off your 1st month to any unlimited car wash membership with HomeWash. http://yourhomewash.com/register?code=" + Preference.getREFFERCODE();

        switch (v.getId()) {


            case R.id.ll_email:
                Intent email = new Intent(Intent.ACTION_SENDTO);
                email.setData(Uri.parse("mailto:"));
                email.putExtra(Intent.EXTRA_TEXT, message);
                try {
                    startActivity(Intent.createChooser(email, "sendmail"));
                } catch (ActivityNotFoundException activityNotFoundException) {
                    Toast.makeText(getActivity(), "App not found", Toast.LENGTH_SHORT);

                }
                break;

            case R.id.ll_msg:
                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("sms_body", message);
                startActivity(smsIntent);
                break;

            case R.id.ll_twitter:

                Intent twitterintent = new Intent(Intent.ACTION_SEND);
                twitterintent.setType("text/plain");
                twitterintent.setPackage("com.twitter.android");
                if (twitterintent != null) {
                    twitterintent.putExtra(Intent.EXTRA_TEXT, message);//
                    startActivity(Intent.createChooser(twitterintent, message));
                } else {
                    Toast.makeText(getActivity(), "App not found", Toast.LENGTH_SHORT)
                            .show();
                }
                break;

            case R.id.ll_fb:

                Intent fbintent = new Intent(Intent.ACTION_SEND);
                fbintent.setType("text/plain");
                fbintent.setPackage("com.facebook.katana");
                if (fbintent != null) {
                    fbintent.putExtra(Intent.EXTRA_TEXT, message);//
                    startActivity(Intent.createChooser(fbintent, message));
                } else {
                    Toast.makeText(getActivity(), "App not found", Toast.LENGTH_SHORT)
                            .show();
                }
                break;

            case R.id.tv_more:

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                if (intent != null) {
                    intent.putExtra(Intent.EXTRA_TEXT, message);//
                    startActivity(Intent.createChooser(intent, message));
                } else {
                    Toast.makeText(getActivity(), "App not found", Toast.LENGTH_SHORT)
                            .show();
                }
                break;

        }
    }
}
