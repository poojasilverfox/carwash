package com.silverfox.carwash.Fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Activity.MainActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.Model.CardUpdateResponse;
import com.silverfox.carwash.Model.PaymentResponse;
import com.silverfox.carwash.Model.PublicKeyResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;
import com.silverfox.carwash.service.HLCard;
import com.silverfox.carwash.service.HLToken;
import com.silverfox.carwash.service.HLTokenService;
import com.silverfox.carwash.view.MonthPickerDialog;
import com.silverfox.carwash.view.YearPickerDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentInfoFragment extends Fragment implements View.OnClickListener {


    View view;
    ApiInterface apiInterface;
    TextView tv_cancel, tv_done, tv_terms;
    ImageView iv_back;
    String card_number, cvv;
    float tokenizationamount = 0;
    CheckBox check_term;
    Boolean b;
    private EditText et_cardno, et_expire_month, et_expire_year, et_cvv;
    private ProgressDialog mProgressDialog;

    public PaymentInfoFragment(boolean b, float tokenizationamount) {
        this.b = b;
        this.tokenizationamount = tokenizationamount;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_payinfo, container, false);

        findView();
        ((MainActivity) getActivity()).iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();
            }
        });

       /* et_cardno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 4 || charSequence.length() == 9 || charSequence.length() == 14) {
                    et_cardno.setText(String.format("%s ", et_cardno.getText().toString()));
                }
                et_cardno.setSelection(et_cardno.getText().length());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/

        return view;
    }


    private void findView() {

        mProgressDialog = new ProgressDialog(getActivity());

        ((MainActivity) getActivity()).iv_back.setVisibility(View.VISIBLE);
        tv_cancel = view.findViewById(R.id.tv_cancel);
        tv_terms = view.findViewById(R.id.tv_terms);
        iv_back = view.findViewById(R.id.iv_back);
        tv_done = view.findViewById(R.id.tv_done);
        et_cardno = view.findViewById(R.id.et_cardno);
        et_expire_month = view.findViewById(R.id.et_expire_month);
        et_expire_year = view.findViewById(R.id.et_expire_year);

        et_cvv = view.findViewById(R.id.et_cvv);
        check_term = view.findViewById(R.id.check_term);

        tv_cancel.setOnClickListener(this);
        tv_done.setOnClickListener(this);
        tv_terms.setOnClickListener(this);

        if (!Preference.getCARDNO().equals("")) {
            et_cardno.setText(Preference.getCARDNO());
        }
        if (!Preference.getMONTH().equals("")) {
            et_expire_month.setText(Preference.getMONTH());
        }
        if (!Preference.getYEAR().equals("")) {
            et_expire_year.setText(Preference.getYEAR());
        }
        if (!Preference.getCVV().equals("")) {
            et_cvv.setText(Preference.getCVV());
        }

        et_expire_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                createDialogWithoutDateField().show();
                MonthPickerDialog newFragment = new MonthPickerDialog();
                newFragment.show(getChildFragmentManager(), "DatePicker");
                newFragment.setListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        Log.e("onDateSet", " <===> " + i1);
                        et_expire_month.setText(String.valueOf(i1));
                    }
                });
            }
        });

        et_expire_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                createDialogWithoutDateField().show();
                YearPickerDialog newFragment = new YearPickerDialog();
                newFragment.show(getChildFragmentManager(), "DatePicker");
                newFragment.setListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        Log.e("onDateSet", " <===> " + i);
                        et_expire_year.setText(String.valueOf(i));
                    }
                });
            }
        });

    }


    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_layout, fragment)
                    .addToBackStack("")
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_done:
                card_number = et_cardno.getText().toString();
                cvv = et_cvv.getText().toString();


                if (card_number.isEmpty()) {
                    et_cardno.setError(getString(R.string.ecardno));
                    et_cardno.requestFocus();
                } else if (card_number.length() < 15
                ) {
                    et_cardno.setError("Invalid Card Details!");
                    et_cardno.requestFocus();
                } else if (et_expire_month.getText().length() == 0) {
                    et_expire_month.setError("Please Enter Month!");
                    et_expire_month.requestFocus();
                } else if (et_expire_year.getText().length() == 0) {
                    et_expire_year.setError("Please Enter Year!");
                    et_expire_year.requestFocus();
                } else if (cvv.isEmpty()) {
                    et_cvv.setError(getString(R.string.ecvv));
                    et_cvv.requestFocus();
                } else if (cvv.length() < 3) {
                    et_cvv.setError("Invalid CVV!");
                    et_cvv.requestFocus();
                } else {
                    if (!check_term.isChecked()) {
                        Util.ShowToastMsg(getActivity(), getString(R.string.accept));
                    } else {
                        if (Util.isConnected(getActivity())) {

                            if (b) {
                                getPublicKey();
                            } else {

                                if (Util.isConnected(getActivity())) {
                                    UpdateCardDtails();
                                } else {
                                    Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                                }

                            }
                        } else {
                            Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                        }
                    }
                }
                break;

            case R.id.tv_cancel:
                et_cardno.setText("");
                et_cvv.setText("");
                et_expire_month.setText("");
                et_expire_year.setText("");

                break;
            case R.id.tv_terms:
                loadFragment(new TermsFragment());
                /*Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("http://www.google.com"));
                try {
                    startActivity(i);
                } catch (ActivityNotFoundException e) {
                    Util.ShowToastMsg(getActivity(), "No Activity Found to View Terms!");
                }*/
                break;
        }
    }


    private void startTokenizing(String public_key) {
        Util.hideKeyboard(getActivity());
        Util.showCustomLoader(getActivity());
        HLTokenService service = new HLTokenService(public_key); // set the public here.
        HLCard card = new HLCard();
//        card.setNumber(et_cardno.getText().toString());
        String input = et_cardno.getText().toString();
        input = input.replace(" ", "");
//        Log.e("TAG", "startTokenizing: CARD:::" + input);
        card.setNumber(input);
        card.setExpMonth(Integer.parseInt(et_expire_month.getText().toString()));
        card.setExpYear(Integer.parseInt(et_expire_year.getText().toString()));
        card.setCvv(et_cvv.getText().toString());
        Preference.setCARDNO(et_cardno.getText().toString().trim());
        Preference.setMONTH(et_expire_month.getText().toString());
        Preference.setYEAR(et_expire_year.getText().toString());
        Preference.setCVV(et_cvv.getText().toString());

        service.getToken(card, new HLTokenService.TokenCallback() {
            @Override
            public void onComplete(HLToken response) {

                if (response == null || response.getError() != null) {
                    Util.dismissCustomLoader();
                    String message = (response == null) ? "Network Call Didnt result in status code 200" : response.getError().getMessage();
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                } else {
                    Log.e("TAG", "onComplete: FINAL TOKEN :" + response.getTokenValue());
                    if (Util.isConnected(getActivity())) {
                        getPaymentData(response.getTokenValue(), tokenizationamount);
                    } else {
                        Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                    }

                }
            }

        });
    }

    private void getPaymentData(String tokenValue, float amount) {
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<PaymentResponse> call = apiInterface.getPaymentData(
                Preference.getAPI_TOKEN(),
                tokenValue, Preference.getCOLOR(),
                String.valueOf(Preference.getCARWASHOWNERID()),
                String.valueOf(Preference.getPLANID()),
                Preference.getPROMOCODE(),
                String.valueOf(amount),
                Preference.getCARMAKE(),
                Preference.getCARMODEL(),
                Preference.getCARTYPE(),
                Preference.getPLATENO(),
                "0",
                Preference.getCARDNO(),
                Preference.getMONTH(),
                Preference.getYEAR(),
                Preference.getCVV());
        call.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaymentResponse> call, @NonNull Response<PaymentResponse> response) {
                Util.dismissCustomLoader();
                Log.e("hellooooooo", "data: " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();

                Util.ShowToastMsg(getActivity(), response.body().getMessage());
                switch (status) {
                    case 1:
                        Preference.setWALLET(Integer.valueOf(response.body().getData().getWallet()));
                        ((MainActivity) getActivity()).bottomNavigationView.setSelectedItemId(R.id.nav_membership);
                        Preference.setPROMOCODE("");
                        Preference.setPLATENO("");
                        Preference.setCARTYPE("");
                        break;
                    case 0:
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;
                }

            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                Util.dismissCustomLoader();
            }
        });
    }

    private void UpdateCardDtails() {
        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<CardUpdateResponse> call = apiInterface.getUpdateCardData(Preference.getAPI_TOKEN(), card_number, et_expire_year.getText().toString(), et_expire_month.getText().toString(), cvv);
        call.enqueue(new Callback<CardUpdateResponse>() {
            @Override
            public void onResponse(Call<CardUpdateResponse> call, Response<CardUpdateResponse> response) {
                Util.dismissCustomLoader();
                Log.e("hellooooooo", "data: " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();

                Util.ShowToastMsg(getActivity(), response.body().getMessage());
                switch (status) {
                    case 1:
//                        et_cardno.setText("");
//                        et_cvv.setText("");
//                        et_expire_month.setText("");
//                        et_expire_year.setText("");
                        Preference.setCARDNO(et_cardno.getText().toString().trim());
                        Preference.setMONTH(et_expire_month.getText().toString());
                        Preference.setYEAR(et_expire_year.getText().toString());
                        Preference.setCVV(et_cvv.getText().toString());
                        break;
                    case 0:
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;
                }

            }

            @Override
            public void onFailure(Call<CardUpdateResponse> call, Throwable t) {
                Util.dismissCustomLoader();


            }
        });
    }

/*
    public boolean validateDateFormat(String dateToValdate) {
        if (dateToValdate.matches("\\d{2}-\\d{4}")) {
            return true;

        } else {
            return false;
        }


    }
*/

    private void getPublicKey() {
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<PublicKeyResponse> call = apiInterface.get_public_key(Preference.getAPI_TOKEN(),
                String.valueOf(Preference.getCARWASHOWNERID()));
        call.enqueue(new Callback<PublicKeyResponse>() {
            @Override
            public void onResponse(Call<PublicKeyResponse> call, Response<PublicKeyResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();

//                Util.ShowToastMsg(getActivity(), response.body().getMessage());

                switch (status) {
                    case 1:
                        startTokenizing(response.body().getPublic_key());
                        break;
                    case 0:
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;

                }

                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


            }

            @Override
            public void onFailure(Call<PublicKeyResponse> call, Throwable t) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                Util.dismissCustomLoader();


            }
        });
    }


}
