package com.silverfox.carwash.Fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Activity.MainActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.Model.LogOutResponse;
import com.silverfox.carwash.Model.WalletUpdateResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingFragment extends Fragment implements View.OnClickListener {


    TextView tv_logout, tv_card, tv_name, tv_amount, tv_updateprofile, tv_CustomerId, tv_reset_password;
    LinearLayout llifrnd, llreddem, llhistory;
    ApiInterface apiInterface;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_setting, container, false);

        findView();


        if (Util.isConnected(getActivity())) {
            getWalletData();

        } else {
            Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
        }


        return view;
    }

    private void getWalletData() {
        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<WalletUpdateResponse> call = apiInterface.getWalletData(Preference.getAPI_TOKEN());
        call.enqueue(new Callback<WalletUpdateResponse>() {
            @Override
            public void onResponse(@NonNull Call<WalletUpdateResponse> call, @NonNull Response<WalletUpdateResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));

                int status = response.body().getStatus();
                switch (status) {
                    case 1:
                        Preference.setWALLET(response.body().getData());

                        tv_amount.setText(new StringBuilder().append("$ ").append(Preference.getWALLET()).toString());
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;

                }


            }

            @Override
            public void onFailure(@NonNull Call<WalletUpdateResponse> call, @NonNull Throwable t) {
                Util.dismissCustomLoader();
            }
        });
    }


    private void findView() {


        ((MainActivity) getActivity()).iv_back.setVisibility(View.GONE);
        llifrnd = view.findViewById(R.id.llifrnd);
        llreddem = view.findViewById(R.id.llreddem);
        llhistory = view.findViewById(R.id.llhistory);
        tv_card = view.findViewById(R.id.tv_card);
        tv_logout = view.findViewById(R.id.tv_logout);
        tv_updateprofile = view.findViewById(R.id.tv_updateprofile);
        tv_CustomerId = view.findViewById(R.id.tv_CustomerId);
        tv_name = view.findViewById(R.id.tv_name);
        tv_amount = view.findViewById(R.id.tv_amount);
        tv_reset_password = view.findViewById(R.id.tv_reset_password);
        llifrnd.setOnClickListener(this);
        tv_logout.setOnClickListener(this);
        tv_card.setOnClickListener(this);
        llreddem.setOnClickListener(this);
        llhistory.setOnClickListener(this);
        tv_updateprofile.setOnClickListener(this);
        tv_reset_password.setOnClickListener(this);

        tv_name.setText(Preference.getNAME());
        tv_CustomerId.setText(Preference.getCUSTOMER_ID());


        if (Preference.getCARDNO().equals("")) {

            tv_card.setText(getString(R.string.anumber));

        } else {
            tv_card.setText(getString(R.string.unumber));

        }
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llifrnd:
                loadFragment(new InviteFriendFragment());
                break;
            case R.id.tv_logout:
                if (Util.isConnected(getActivity())) {
                    Dialogfunction();

                } else {
                    Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                }

                break;
            case R.id.tv_card:
                loadFragment(new PaymentInfoFragment(false, 0));

                break;
            case R.id.tv_updateprofile:
                loadFragment(new UpdateProfileFragment());

                break;
            case R.id.tv_reset_password:
                loadFragment(new ChangePasswordFragment());

                break;
            case R.id.llreddem:
                loadFragment(new ReedemFragment());

                break;
            case R.id.llhistory:
                loadFragment(new HistoryFragment());

                break;
        }
    }

    private void Dialogfunction() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setMessage("Do you want to sure Logout from application ?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if (Util.isConnected(getActivity())) {
                            Logout();
                            dialog.dismiss();
                        } else {
                            Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                        }


                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_layout, fragment)
                    .addToBackStack("")
                    .commit();
            return true;
        }
        return false;
    }


    private void Logout() {
        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<LogOutResponse> call = apiInterface.getLogoutData(Preference.getAPI_TOKEN());
        call.enqueue(new Callback<LogOutResponse>() {
            @Override
            public void onResponse(Call<LogOutResponse> call, Response<LogOutResponse> response) {
                Util.dismissCustomLoader();
                Log.e("hellooooooo", "data: " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();

                Util.ShowToastMsg(getActivity(), response.body().getMessage());

                switch (status) {
                    case 1:
                        LoginManager.getInstance().logOut();
                        Preference.setUSER_ID(0);
                        Util.carwashlistData.clear();
                        Util.carwashlistData = new ArrayList<>();
                        Util.location_flag = false;
                        Util.fragment_position_flag = true;
                        Util.currentposition = 0;
                        Util.position = 0;
                        Util.referralposition = 0;
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();

                        break;
                }


            }

            @Override
            public void onFailure(Call<LogOutResponse> call, Throwable t) {
                Util.dismissCustomLoader();


            }
        });
    }


}
