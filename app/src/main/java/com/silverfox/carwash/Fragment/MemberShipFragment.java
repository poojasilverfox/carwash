package com.silverfox.carwash.Fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Activity.MainActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.Model.MembershipListResponse;
import com.silverfox.carwash.Model.UpdateProfileResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;
import com.silverfox.carwash.adapter.MembershipAdapter;
import com.silverfox.carwash.view.CustomRobotoEditText;
import com.silverfox.carwash.view.CustomRobotoTextView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class MemberShipFragment extends Fragment implements View.OnClickListener, MembershipAdapter.loadFrgment
        , MembershipAdapter.ReloadAllData {


    public static ArrayList<MembershipListResponse.DataBean> membershipList = new ArrayList<>();
    public MembershipAdapter adapter;
    View view;
    TextView tv_find, tvCustomerID;
    RecyclerView rv_membersip_list;
    ApiInterface apiInterface;
    int finalcurrentpage = 0;
    LinearLayout ll_data, ll_nodata;
    Dialog dialog;
    private LinearLayoutManager layoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_membership, container, false);

        findView();
        if (Preference.getEMAIL().equals("")) {
            openDialog();
        }


    /*    rv_membersip_list.setOnScrollListener(new EndlessRecyclerOnScrollListenerNewGrid(layoutManager) {
            @Override
            public void onLoadMore(int paramInt) {
                Log.e("tag", "onLoadMore:" + paramInt);
                Log.e("tag", "onLoadMore: +scroll");


                if (Util.isConnected(getActivity())) {
                    getMemberShipList(finalcurrentpage);
                }else{
                    Util.ShowToastMsg(getActivity(),getString(R.string.noconn));
                }
            }


        });
*/

        if (Util.isConnected(getActivity())) {
            getMemberShipList(0);
        } else {
            Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
        }

        return view;
    }

    private void openDialog() {

        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.layout_dialog);
        final CustomRobotoEditText et_email = dialog.findViewById(R.id.et_email);
        CustomRobotoTextView tv_submit = dialog.findViewById(R.id.tv_submit);
        CustomRobotoTextView tv_title = dialog.findViewById(R.id.tv_title);

        tv_title.setText("Please enter email");


        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = et_email.getText().toString().trim();
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                if (email.length() == 0) {
                    et_email.setError(getString(R.string.eemail));
                    et_email.requestFocus();
                } else if (!email.matches(emailPattern)) {

                    et_email.setError(getString(R.string.invalidea));
                    et_email.requestFocus();

                } else {
                    if (Util.isConnected(getActivity())) {
                        AddEmail(email);
                    } else {
                        Util.ShowToastMsg(getActivity(), getString(R.string.noconn));
                    }


                }

            }
        });


        dialog.show();
    }

    private void AddEmail(String email) {
        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<UpdateProfileResponse> call = apiInterface.getUpdateProfile(Preference.getAPI_TOKEN(), email, Preference.getNAME(),
                Preference.getLNAME());
        call.enqueue(new Callback<UpdateProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));

                Util.ShowToastMsg(getActivity(), response.body().getMessage());

                int status = response.body().getStatus();

                switch (status) {
                    case 1:
                        Preference.setNAME(response.body().getData().getName());
                        Preference.setLNAME(response.body().getData().getLast_name());
                        Preference.setEMAIL(response.body().getData().getEmail());
                        dialog.dismiss();

                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;
                }


            }

            @Override
            public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                Util.dismissCustomLoader();
                dialog.dismiss();

            }
        });
    }

    public void getMemberShipList(final int currentpage) {
        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<MembershipListResponse> call = apiInterface.getMemberShipList(Preference.getAPI_TOKEN(), String.valueOf(currentpage));
        call.enqueue(new Callback<MembershipListResponse>() {
            @Override
            public void onResponse(Call<MembershipListResponse> call, Response<MembershipListResponse> response) {
                Util.dismissCustomLoader();
//                Log.e("hellooooooo", "data: " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();

                switch (status) {
                    case 1:
                        ll_data.setVisibility(View.VISIBLE);
                        ll_nodata.setVisibility(View.GONE);
                        if (currentpage == 0) {
                            membershipList = new ArrayList<>();
                            membershipList.clear();
                            membershipList.addAll(response.body().getData());
                            adapter = new MembershipAdapter(getActivity(), membershipList, MemberShipFragment.this, MemberShipFragment.this);
                            rv_membersip_list.setAdapter(adapter);
                        } else {
                            membershipList.addAll(response.body().getData());
                        }
//                        adapter.notifyDataSetChanged();
                        finalcurrentpage++;
                        break;

                    case 0:
                        if (currentpage == 0) {
                            ll_data.setVisibility(View.GONE);
                            ll_nodata.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra("type", "login");
                        startActivity(intent);
                        getActivity().finish();
                        break;
                }


            }

            @Override
            public void onFailure(Call<MembershipListResponse> call, Throwable t) {
                Log.e("tag", "onFailure: ERROR:::" + t.getMessage());
                Util.dismissCustomLoader();
            }
        });
    }


    private void findView() {

        ((MainActivity) getActivity()).iv_back.setVisibility(View.GONE);

        tv_find = view.findViewById(R.id.tv_find);
        rv_membersip_list = view.findViewById(R.id.rv_membersip_list);
        ll_data = view.findViewById(R.id.ll_data);
        ll_nodata = view.findViewById(R.id.ll_nodata);
        tvCustomerID = view.findViewById(R.id.tvCustomerID);

        Log.e("findView", "findView: In..:::" + Preference.getCUSTOMER_ID());
        tvCustomerID.setText(String.format("%s : %s", getString(R.string.customerid), Preference.getCUSTOMER_ID()));

        tv_find.setOnClickListener(this);

        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv_membersip_list.setLayoutManager(layoutManager);


    }


    @Override
    public void onClick(View v) {
        if (v == tv_find) {
            Util.carwashlistData.clear();
            Util.carwashlistData = new ArrayList<>();
            Util.fragment_position_flag = true;
            Util.location_flag = false;
            Util.currentposition = 0;

            MainActivity.bottomNavigationView.setSelectedItemId(R.id.nav_location);
            loadFragment(new LocationFragment());
        }
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_layout, fragment)
                    .addToBackStack("")
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public void loadFragm(int position) {
        Util.referralposition = position;
        loadFragment(new ScanFragment(position));

    }

    @Override
    public void onReloadCall() {
        Log.e(TAG, "onReloadCall: CALLED::::::::::");
        getMemberShipList(0);
    }
}
