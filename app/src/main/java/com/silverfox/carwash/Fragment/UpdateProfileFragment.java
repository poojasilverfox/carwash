package com.silverfox.carwash.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Activity.MainActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.Model.CardUpdateResponse;
import com.silverfox.carwash.Model.ForgotResponse;
import com.silverfox.carwash.Model.PaymentResponse;
import com.silverfox.carwash.Model.UpdateProfileResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;
import com.silverfox.carwash.service.HLCard;
import com.silverfox.carwash.service.HLToken;
import com.silverfox.carwash.service.HLTokenService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProfileFragment extends Fragment implements View.OnClickListener {


    View view;
    ApiInterface apiInterface;

    EditText et_name, et_lname, et_email, et_mono;
    TextView tv_update;
    String Name = "", LName = "", Email = "";

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.layout_updateprofile, container, false);

        findView();
        ((MainActivity) getActivity()).iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();
            }
        });


        return view;
    }


    private void findView() {


        ((MainActivity) getActivity()).iv_back.setVisibility(View.VISIBLE);
        et_name = view.findViewById(R.id.et_name);
        et_lname = view.findViewById(R.id.et_lname);
        et_email = view.findViewById(R.id.et_email);
        et_mono = view.findViewById(R.id.et_mono);
        tv_update = view.findViewById(R.id.tv_update);
        tv_update.setOnClickListener(this);

        if (!Preference.getNAME().equals("")) {
            et_name.setText(Preference.getNAME());
        }
        if (!Preference.getLNAME().equals("")) {
            et_lname.setText(Preference.getLNAME());
        }
        if (!Preference.getEMAIL().equals("")) {
            et_email.setText(Preference.getEMAIL());
        }


        String mono = Preference.getMNO();

        et_mono.setText(mono.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3"));


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_update:

                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                Name = et_name.getText().toString();
                LName = et_lname.getText().toString();
                Email = et_email.getText().toString();

                if(Name.isEmpty()){
                    et_name.setError(getString(R.string.ename));
                    et_name.requestFocus();
                }else if(LName.isEmpty()){
                    et_lname.setError(getString(R.string.elname));
                    et_lname.requestFocus();
                }else if(Email.isEmpty()){
                    et_email.setError(getString(R.string.eemail));
                    et_email.requestFocus();
                }else if (!Email.matches(emailPattern)) {
                    et_email.setError(getString(R.string.invalidea));
                    et_email.requestFocus();
                }else{
                    UpdateProfile();
                }
                break;

        }
    }

    private void UpdateProfile() {
        Util.showCustomLoader(getActivity());
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<UpdateProfileResponse> call = apiInterface.getUpdateProfile(Preference.getAPI_TOKEN(),Email,Name,LName);
        call.enqueue(new Callback<UpdateProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));

                Util.ShowToastMsg(getActivity(), response.body().getMessage());

                int status = response.body().getStatus();
                switch (status){
                    case 1:
                        Preference.setNAME(response.body().getData().getName());
                        Preference.setLNAME(response.body().getData().getLast_name());
                        Preference.setEMAIL(response.body().getData().getEmail());
                        break;
                    case 404:
                        Intent intent = new Intent(getActivity(),LoginActivity.class);
                        intent.putExtra("type","login");
                        startActivity(intent);
                        getActivity().finish();
                        break;

                }




            }

            @Override
            public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                Util.dismissCustomLoader();

            }
        });
    }


}
