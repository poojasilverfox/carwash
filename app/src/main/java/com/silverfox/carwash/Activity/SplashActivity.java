package com.silverfox.carwash.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tv_login,tv_signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        findView();



        printHashKey(SplashActivity.this);

        if (Util.isConnected(SplashActivity.this)) {
            if(Preference.getUSER_ID()!=0){


                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, 1700);

            }else{
                tv_login.setVisibility(View.VISIBLE);
                tv_signup.setVisibility(View.VISIBLE);
            }
        }else{
            NoConnectionDialouge();
        }





    }

    private void NoConnectionDialouge() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_no_internetconnection);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());

        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        int dialogWindowHeight = (int) (displayHeight * 0.32f);

        layoutParams.width = dialogWindowWidth;
        layoutParams.height = dialogWindowHeight;

        LinearLayout llRetry;
        llRetry = dialog.findViewById(R.id.llRetry);

        llRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Util.isConnected(SplashActivity.this)) {
                    if(Preference.getUSER_ID()!=0){


                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }, 1700);

                    }else{
                        tv_login.setVisibility(View.VISIBLE);
                        tv_signup.setVisibility(View.VISIBLE);
                    }
                    dialog.dismiss();
                }else{
                    Util.ShowToastMsg(SplashActivity.this,getString(R.string.noconn));
                }


            }
        });

        dialog.getWindow().setAttributes(layoutParams);
        dialog.show();
    }


    private void findView() {
        tv_login = findViewById(R.id.tv_login);
        tv_signup = findViewById(R.id.tv_signup);

        tv_login.setOnClickListener(this);
        tv_signup.setOnClickListener(this);
    }

    public static void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("tag", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {

        } catch (Exception e) {

        }
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.tv_login:
                Intent loginintent = new Intent(SplashActivity.this,LoginActivity.class);
                loginintent.putExtra("type","login");
                startActivity(loginintent);
                finish();
                break;

            case R.id.tv_signup:
                Intent regintent = new Intent(SplashActivity.this,LoginActivity.class);
                regintent.putExtra("type","signup");
                startActivity(regintent);
                finish();
                break;
        }

    }
}
