package com.silverfox.carwash.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.Model.FaceBookLoginResponse;
import com.silverfox.carwash.Model.ForgotResponse;
import com.silverfox.carwash.Model.LoginResponse;
import com.silverfox.carwash.Model.RegisterResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;
import com.silverfox.carwash.view.CustomRobotoEditText;
import com.silverfox.carwash.view.CustomRobotoTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.provider.ContactsContract.Intents.Insert.EMAIL;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


    private static final String TAG = "hello";
    public final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "^(([\\\\w-]+\\\\.)+[\\\\w-]+|([a-zA-Z]{1}|[\\\\w-]{2,}))@\"\n" +
                    " + \"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\\\.([0-1]?\"\n" +
                    " + \"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\\\.\"\n" +
                    " + \"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\\\.([0-1]?\"\n" +
                    " + \"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|\"\n" +
                    " + \"([a-zA-Z]+[\\\\w-]+\\\\.)+[a-zA-Z]{2,4})$"
    );
    private LoginButton login_button;
    private CallbackManager callbackManager;
    private TextView tv_login;
    private TextView tv_signup;
    private EditText et_login_mono, et_login_pwd, et_reg_name, et_reg_lname, et_reg_mono, et_reg_pwd, et_reg_referral;
    private LinearLayout ll_register, ll_login;
    private String Mno = "", Pwd = "", Name = "", LName = "", type = "", Referral = "";
    private Dialog dialog;
    private final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findView();
        et_login_mono.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            //we need to know if the user is erasing or inputing some new character
            private boolean backspacingFlag = false;
            //we need to block the :afterTextChanges method to be called again after we just replaced the EditText text
            private boolean editedFlag = false;
            //we need to mark the cursor position and restore it after the edition
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //we store the cursor local relative to the end of the string in the EditText before the edition
                cursorComplement = s.length() - et_login_mono.getSelectionStart();
                //we check if the user ir inputing or erasing a character
                if (count > after) {
                    backspacingFlag = true;
                } else {
                    backspacingFlag = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing to do here =D
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                //what matters are the phone digits beneath the mask, so we always work with a raw string with only digits
                String phone = string.replaceAll("[^\\d]", "");

                //if the text was just edited, :afterTextChanged is called another time... so we need to verify the flag of edition
                //if the flag is false, this is a original user-typed entry. so we go on and do some magic
                if (!editedFlag) {

                    //we start verifying the worst case, many characters mask need to be added
                    //example: 999999999 <- 6+ digits already typed
                    // masked: (999) 999-999
                    if (phone.length() >= 6 && !backspacingFlag) {
                        //we will edit. next call on this textWatcher will be ignored
                        editedFlag = true;
                        //here is the core. we substring the raw digits and add the mask as convenient
                        String ans = "(" + phone.substring(0, 3) + ") " + phone.substring(3, 6) + "-" + phone.substring(6);
                        et_login_mono.setText(ans);
                        //we deliver the cursor to its original position relative to the end of the string
                        et_login_mono.setSelection(et_login_mono.getText().length() - cursorComplement);

                        //we end at the most simple case, when just one character mask is needed
                        //example: 99999 <- 3+ digits already typed
                        // masked: (999) 99
                    } else if (phone.length() >= 3 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = "(" + phone.substring(0, 3) + ") " + phone.substring(3);
                        et_login_mono.setText(ans);
                        et_login_mono.setSelection(et_login_mono.getText().length() - cursorComplement);
                    }
                    // We just edited the field, ignoring this cicle of the watcher and getting ready for the next
                } else {
                    editedFlag = false;
                }
            }
        });

        et_reg_mono.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            //we need to know if the user is erasing or inputing some new character
            private boolean backspacingFlag = false;
            //we need to block the :afterTextChanges method to be called again after we just replaced the EditText text
            private boolean editedFlag = false;
            //we need to mark the cursor position and restore it after the edition
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //we store the cursor local relative to the end of the string in the EditText before the edition
                cursorComplement = s.length() - et_reg_mono.getSelectionStart();
                //we check if the user ir inputing or erasing a character
                if (count > after) {
                    backspacingFlag = true;
                } else {
                    backspacingFlag = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing to do here =D
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                //what matters are the phone digits beneath the mask, so we always work with a raw string with only digits
                String phone = string.replaceAll("[^\\d]", "");

                //if the text was just edited, :afterTextChanged is called another time... so we need to verify the flag of edition
                //if the flag is false, this is a original user-typed entry. so we go on and do some magic
                if (!editedFlag) {

                    //we start verifying the worst case, many characters mask need to be added
                    //example: 999999999 <- 6+ digits already typed
                    // masked: (999) 999-999
                    if (phone.length() >= 6 && !backspacingFlag) {
                        //we will edit. next call on this textWatcher will be ignored
                        editedFlag = true;
                        //here is the core. we substring the raw digits and add the mask as convenient
                        String ans = "(" + phone.substring(0, 3) + ") " + phone.substring(3, 6) + "-" + phone.substring(6);
                        et_reg_mono.setText(ans);
                        //we deliver the cursor to its original position relative to the end of the string
                        et_reg_mono.setSelection(et_reg_mono.getText().length() - cursorComplement);

                        //we end at the most simple case, when just one character mask is needed
                        //example: 99999 <- 3+ digits already typed
                        // masked: (999) 99
                    } else if (phone.length() >= 3 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = "(" + phone.substring(0, 3) + ") " + phone.substring(3);
                        et_reg_mono.setText(ans);
                        et_reg_mono.setSelection(et_reg_mono.getText().length() - cursorComplement);
                    }
                    // We just edited the field, ignoring this cicle of the watcher and getting ready for the next
                } else {
                    editedFlag = false;
                }
            }
        });


        if (type.equals("login")) {
            Mno = "";
            Pwd = "";

            ll_register.setVisibility(View.GONE);
            ll_login.setVisibility(View.VISIBLE);

            tv_login.setTextColor(getResources().getColor(R.color.white));
            tv_login.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_round));

            tv_signup.setTextColor(getResources().getColor(R.color.blue));
            tv_signup.setBackgroundDrawable(null);

        } else {

            Mno = "";
            Pwd = "";
            Name = "";
            ll_login.setVisibility(View.GONE);
            ll_register.setVisibility(View.VISIBLE);

            tv_signup.setTextColor(getResources().getColor(R.color.white));
            tv_signup.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_round));

            tv_login.setTextColor(getResources().getColor(R.color.blue));
            tv_login.setBackgroundDrawable(null);

        }
    }

    private void findView() {

        type = getIntent().getStringExtra("type");

        callbackManager = CallbackManager.Factory.create();
        TextView tv_loginfacebook = findViewById(R.id.tv_loginfacebook);
        tv_login = findViewById(R.id.tv_login);
        tv_signup = findViewById(R.id.tv_signup);
        TextView tv_fpwd = findViewById(R.id.tv_fpwd);
        et_login_mono = findViewById(R.id.et_login_mono);
        et_login_pwd = findViewById(R.id.et_login_pwd);
        et_reg_name = findViewById(R.id.et_reg_name);
        et_reg_lname = findViewById(R.id.et_reg_lname);
        et_reg_mono = findViewById(R.id.et_reg_mono);
        et_reg_pwd = findViewById(R.id.et_reg_pwd);
        et_reg_referral = findViewById(R.id.et_reg_referral);
        ll_register = findViewById(R.id.ll_register);
        ll_login = findViewById(R.id.ll_login);
        login_button = findViewById(R.id.login_button);
        login_button.setReadPermissions(Arrays.asList(EMAIL));


        tv_login.setOnClickListener(this);
        tv_signup.setOnClickListener(this);
        tv_fpwd.setOnClickListener(this);
        tv_loginfacebook.setOnClickListener(this);


        login_button.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {


                        String accessToken = loginResult.getAccessToken().getToken();
                        Log.i("accessToken", accessToken);

                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.i("LoginActivity", response.toString());
                                // Get facebook data from login
                                getFacebookData(object);

                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location"); // Parámetros que pedimos a facebook
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(LoginActivity.this, "cancel", Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onError(FacebookException exception) {

                        Toast.makeText(LoginActivity.this, "error", Toast.LENGTH_LONG).show();

                    }
                });
    }

    private Bundle getFacebookData(JSONObject object) {

        try {
            Bundle bundle = new Bundle();
            String id = object.getString("id");

            try {
                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
                Log.i("profile_pic", profile_pic + "");
                bundle.putString("profile_pic", profile_pic.toString());

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }

            String name = "", email = "";
            bundle.putString("idFacebook", id);
            if (object.has("first_name")) {
                bundle.putString("first_name", object.getString("first_name"));
                name = object.getString("first_name");
            }
            if (object.has("last_name"))
                bundle.putString("last_name", object.getString("last_name"));
            if (object.has("email")) {
                bundle.putString("email", object.getString("email"));
                email = object.getString("email");
            }
            if (object.has("gender"))
                bundle.putString("gender", object.getString("gender"));
            if (object.has("birthday"))
                bundle.putString("birthday", object.getString("birthday"));
            if (object.has("location"))
                bundle.putString("location", object.getJSONObject("location").getString("name"));


            CallFaceBookLogin(name, id);


            return bundle;
        } catch (JSONException e) {
            Log.d(TAG, "Error parsing JSON");
        }
        return null;
    }

    private void CallFaceBookLogin(String name, String fb_id) {
        Util.showCustomLoader(LoginActivity.this);
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<FaceBookLoginResponse> call = apiInterface.getFaceBookData(name, "", fb_id, "", "");
        call.enqueue(new Callback<FaceBookLoginResponse>() {
            @Override
            public void onResponse(Call<FaceBookLoginResponse> call, Response<FaceBookLoginResponse> response) {
                Util.dismissCustomLoader();
                Log.e("hellooooooo", "data: " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();
                Log.e(TAG, "success:");
                Util.ShowToastMsg(LoginActivity.this, response.body().getMessage());

                switch (status) {
                    case 1:
                        Preference.setUSER_ID(response.body().getData().getUser_id());
                        Preference.setAPI_TOKEN(response.body().getData().getApi_token());
                        Preference.setNAME(response.body().getData().getName());
                        Preference.setREFFERCODE(response.body().getData().getReffer_code());
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                        break;


                }


            }

            @Override
            public void onFailure(Call<FaceBookLoginResponse> call, Throwable t) {
                Util.dismissCustomLoader();

                Log.e(TAG, "onFailure:");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.tv_login:


                if (ll_login.getVisibility() == View.VISIBLE) {

                    Mno = et_login_mono.getText().toString().replace("-", "").replace("(", "").replace(")", "").replace(" ", "").trim();
                    Pwd = et_login_pwd.getText().toString();
                    if (Mno.isEmpty()) {
                        et_login_mono.setError(getString(R.string.emobile));
                        et_login_mono.requestFocus();
                    } else if (Pwd.isEmpty()) {
                        et_login_pwd.setError(getString(R.string.epwd));
                        et_login_pwd.requestFocus();
                    } else {
                        if (Util.isConnected(LoginActivity.this)) {
                            CallLogin();
                        } else {
                            Util.ShowToastMsg(LoginActivity.this, getString(R.string.noconn));
                        }
                    }


                } else {

                    Mno = "";
                    Pwd = "";
                    et_login_pwd.setError(null);
                    et_login_mono.setError(null);
                    ll_register.setVisibility(View.GONE);
                    ll_login.setVisibility(View.VISIBLE);

                    tv_login.setTextColor(getResources().getColor(R.color.white));
                    tv_login.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_round));

                    tv_signup.setTextColor(getResources().getColor(R.color.blue));
                    tv_signup.setBackgroundDrawable(null);


                }


                break;

            case R.id.tv_signup:

                if (ll_register.getVisibility() == View.VISIBLE) {

                    Name = et_reg_name.getText().toString();
                    LName = et_reg_lname.getText().toString();
                    Mno = et_reg_mono.getText().toString().replace("-", "").replace("(", "").replace(")", "").replace(" ", "").trim();
                    Pwd = et_reg_pwd.getText().toString();
                    Referral = et_reg_referral.getText().toString();
                    if (Name.isEmpty()) {
                        et_reg_name.setError(getString(R.string.ename));
                        et_reg_name.requestFocus();
                    } else if (LName.isEmpty()) {
                        et_reg_lname.setError(getString(R.string.elname));
                        et_reg_lname.requestFocus();
                    } else if (Mno.isEmpty()) {
                        et_reg_mono.setError(getString(R.string.emobile));
                        et_reg_mono.requestFocus();
                    } else if (Pwd.isEmpty()) {
                        et_reg_pwd.setError(getString(R.string.epwd));
                        et_reg_pwd.requestFocus();
                    } else {
                        if (Util.isConnected(LoginActivity.this)) {
                            CallRegistration();
                        } else {
                            Util.ShowToastMsg(LoginActivity.this, getString(R.string.noconn));
                        }


                    }

                } else {

                    Mno = "";
                    Pwd = "";
                    Name = "";
                    LName = "";
                    ll_login.setVisibility(View.GONE);
                    ll_register.setVisibility(View.VISIBLE);
                    et_reg_referral.setError(null);
                    et_reg_pwd.setError(null);
                    et_reg_mono.setError(null);
                    et_reg_name.setError(null);
                    et_reg_lname.setError(null);

                    tv_signup.setTextColor(getResources().getColor(R.color.white));
                    tv_signup.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_round));

                    tv_login.setTextColor(getResources().getColor(R.color.blue));
                    tv_login.setBackgroundDrawable(null);

                }
                break;

            case R.id.tv_fpwd:

                openDialog();

                break;
            case R.id.tv_loginfacebook:

                if (Util.isConnected(LoginActivity.this)) {
                    login_button.callOnClick();
                } else {
                    Util.ShowToastMsg(LoginActivity.this, getString(R.string.noconn));
                }

                break;
        }
    }

    private boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    private void openDialog() {

        dialog = new Dialog(LoginActivity.this);
        dialog.setContentView(R.layout.layout_dialog);
        final CustomRobotoEditText et_email = dialog.findViewById(R.id.et_email);
        CustomRobotoTextView tv_submit = dialog.findViewById(R.id.tv_submit);


        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = et_email.getText().toString().trim();

                if (email.length() == 0) {
                    et_email.setError(getString(R.string.eemail));
                    et_email.requestFocus();
                } else if (!email.matches(emailPattern)) {

                    et_email.setError(getString(R.string.invalidea));
                    et_email.requestFocus();

                } else {
                    if (Util.isConnected(LoginActivity.this)) {
                        CallForgotPassword(email);
                    } else {
                        Util.ShowToastMsg(LoginActivity.this, getString(R.string.noconn));
                    }

                }

            }
        });


        dialog.show();
    }

    private void CallForgotPassword(String email) {
        Util.showCustomLoader(LoginActivity.this);
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<ForgotResponse> call = apiInterface.getForgotPassword(email);
        call.enqueue(new Callback<ForgotResponse>() {
            @Override
            public void onResponse(Call<ForgotResponse> call, Response<ForgotResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));

                Util.ShowToastMsg(LoginActivity.this, response.body().getMessage());

                Boolean status = response.body().isStatus();
                if (status) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ForgotResponse> call, Throwable t) {
                Util.dismissCustomLoader();
                dialog.dismiss();
            }
        });
    }


    private void CallLogin() {
        Util.showCustomLoader(LoginActivity.this);
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<LoginResponse> call = apiInterface.getLoginData(Pwd, Mno, "android", Util.getUniqueId());
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                Util.dismissCustomLoader();
                Log.e("c", "data: " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();

                Util.ShowToastMsg(LoginActivity.this, response.body().getMessage());

                switch (status) {

                    case 1:
                        Preference.setAPI_TOKEN(response.body().getData().getApi_token());
                        Preference.setCUSTOMER_ID(response.body().getData().getCustomer_id());
                        Preference.setUSER_ID(response.body().getData().getUser_id());
                        Preference.setNAME(response.body().getData().getName());
                        Preference.setLNAME(response.body().getData().getLast_name());
                        Preference.setEMAIL(response.body().getData().getEmail());
                        Preference.setREFFERCODE(response.body().getData().getReffer_code());
                        Preference.setMNO(response.body().getData().getMobile());
                        Preference.setCARDNO(response.body().getData().getCard_number());
                        Preference.setMONTH(response.body().getData().getExpiry_month());
                        Preference.setYEAR(response.body().getData().getExpiry_year());
                        Preference.setCVV(response.body().getData().getCvv_code());
                        Preference.setWALLET(response.body().getData().getWallet());


                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                }


            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("onFailure", "In..." + t.getMessage());
                Util.dismissCustomLoader();
            }
        });
    }

    private void CallRegistration() {
        Util.showCustomLoader(LoginActivity.this);
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
        Call<RegisterResponse> call = apiInterface.getRegistrationData(Name, LName, Pwd, Referral, "91", Mno);
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                Util.dismissCustomLoader();
                Log.e(TAG, "onResponse 1: " + response.toString());
                Log.e(TAG, "onResponse 2: " + response.message());
                Log.e(TAG, "onResponse 3: " + response.isSuccessful());
                Log.e(TAG, "onResponse 4: " + response.raw());
//                Log.e(TAG, "onResponse 5: "+response.errorBody().toString());
                if (response.body() != null) {
                    Log.e(TAG, "onResponse 6: " + response.body().getMessage());
                }
                int status = response.body().getStatus();

                Util.ShowToastMsg(LoginActivity.this, response.body().getMessage());

                if (status == 1) {
//                        Mno = "";
//                        Pwd = "";
//
//
//                        et_login_mono.setText("");
//                        et_login_pwd.setText("");
//                        ll_register.setVisibility(View.GONE);
//                        ll_login.setVisibility(View.VISIBLE);
//
//                        tv_login.setTextColor(getResources().getColor(R.color.white));
//                        tv_login.setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_round));
//
//                        tv_signup.setTextColor(getResources().getColor(R.color.blue));
//                        tv_signup.setBackgroundDrawable(null);

                    Preference.setAPI_TOKEN(response.body().getData().getApi_token());
                    Preference.setCUSTOMER_ID(response.body().getData().getCustomer_id());
                    Preference.setUSER_ID(response.body().getData().getUser_id());
                    Preference.setNAME(response.body().getData().getName());
                    Preference.setEMAIL(response.body().getData().getEmail());
                    Preference.setREFFERCODE(response.body().getData().getReffer_code());
                    Preference.setMNO(response.body().getData().getMobile());
                    Preference.setCARDNO(response.body().getData().getCard_number());
                    Preference.setMONTH(response.body().getData().getExpiry_month());
                    Preference.setYEAR(response.body().getData().getExpiry_year());
                    Preference.setCVV(response.body().getData().getCvv_code());
                    Preference.setWALLET(response.body().getData().getWallet());


                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }

            }

            @Override
            public void onFailure(@NonNull Call<RegisterResponse> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: ERROR:::" + call.toString());
                Util.dismissCustomLoader();
            }
        });
    }


}

