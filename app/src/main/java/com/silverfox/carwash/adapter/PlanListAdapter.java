package com.silverfox.carwash.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.Fragment.WashFragment;
import com.silverfox.carwash.Model.CarWashListResponse;
import com.silverfox.carwash.Model.PromocodeResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;
import com.silverfox.carwash.view.CustomRobotoEditText;
import com.silverfox.carwash.view.CustomRobotoTextView;

import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class PlanListAdapter extends RecyclerView.Adapter<PlanListAdapter.ViewHolder> {

    Activity activity;
    int[] array;
    List<CarWashListResponse.DataBean.PlanDetailsBean> plan_details;
    private loadFrgment loadFrgment;
    Dialog dialog;
    String promocode = "";
    ApiInterface apiInterface;
    int per = 0, carwashid;


    public interface loadFrgment {
        void loadFragm(int position, float amount);
    }

    public PlanListAdapter(FragmentActivity activity, int[] array, List<CarWashListResponse.DataBean.PlanDetailsBean> plan_details, int carwash_id, loadFrgment loadFrgment) {
        this.activity = activity;
        this.array = array;
        this.plan_details = plan_details;
        this.loadFrgment = loadFrgment;
        this.carwashid = carwash_id;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View contactView = LayoutInflater.from(activity).inflate(R.layout.row_plan_list, parent, false);

        final ViewHolder viewHolder = new ViewHolder(contactView);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        holder.netprice = plan_details.get(position).getPrice();
        if (plan_details.get(position).getType() == 0) {
            holder.camount = WashFragment.insidecarsizelist.get(0).getAmount();
            holder.FinalAmount = Float.parseFloat(WashFragment.insidecarsizelist.get(0).getAmount());
        } else if (plan_details.get(position).getType() == 1) {
            holder.camount = WashFragment.outsidecarsizelist.get(0).getAmount();
            holder.FinalAmount = Float.parseFloat(WashFragment.outsidecarsizelist.get(0).getAmount());
        } else if (plan_details.get(position).getType() == 2) {
            holder.camount = WashFragment.normalsizeList.get(0).getAmount();
            holder.FinalAmount = Float.parseFloat(WashFragment.normalsizeList.get(0).getAmount());
        }

//        if (position % 4 == 0) {
//            holder.ff_textlayout.setBackgroundColor(array[0]);
//
//
//        } else if (position % 4 == 1) {
//            holder.ff_textlayout.setBackgroundColor(array[1]);
//
//
//        } else if (position % 4 == 2) {
//            holder.ff_textlayout.setBackgroundColor(array[2]);
//
//
//        } else if (position % 4 == 3) {
//            holder.ff_textlayout.setBackgroundColor(array[3]);
//
//
//        }


        if (plan_details.get(position).getType() == 2) {
            holder.tv_setvicetype.setText(plan_details.get(position).getName());
        } else {
            if (plan_details.get(position).getType() == 0) {
                if (!plan_details.get(position).getName_note().equals("")) {
                    holder.tv_setvicetype.setText(plan_details.get(position).getName() + "-" + "(" + plan_details.get(position).getName_note() + ")" + "-(INSIDE TUNNEL)");
                } else {
                    holder.tv_setvicetype.setText(plan_details.get(position).getName() + "-(INSIDE TUNNEL)");
                }
            } else if (plan_details.get(position).getType() == 1) {
                if (!plan_details.get(position).getName_note().equals("")) {
                    holder.tv_setvicetype.setText(plan_details.get(position).getName() + "-" + "(" + plan_details.get(position).getName_note() + ")" + "-(OUTSIDE TUNNEL)");
                } else {
                    holder.tv_setvicetype.setText(plan_details.get(position).getName() + "-(OUTSIDE TUNNEL)");
                }
            }
        }

        String s = (String.format("%.02f", plan_details.get(position).getPrice()));
        holder.tv_setvicetypeename.setText(plan_details.get(position).getExtra_plan());
        holder.tv_amount.setText("$ " + s + "/month");

        float parseFloat = Float.parseFloat(holder.camount) + plan_details.get(position).getPrice();
        Log.e(TAG, "onBindViewHolder: float value======" + parseFloat);
        holder.camount = String.valueOf(parseFloat);
//        holder.FinalAmount = plan_details.get(position).getPrice();
        holder.tv_joinnow.setText("Select " + " $" + holder.camount);

        if (plan_details.get(position).getColor_code() == null) {
            holder.cardSelect.setCardBackgroundColor(activity.getResources().getColor(R.color.blue));
            holder.llAmount.setBackgroundColor(activity.getResources().getColor(R.color.blue));
            holder.CardApply.setCardBackgroundColor(activity.getResources().getColor(R.color.blue));

        } else {
            holder.CardApply.setCardBackgroundColor(Color.parseColor(plan_details.get(position).getColor_code()));
            holder.llAmount.setBackgroundColor(Color.parseColor(plan_details.get(position).getColor_code()));
            holder.cardSelect.setCardBackgroundColor(Color.parseColor(plan_details.get(position).getColor_code()));
        }

        String message = plan_details.get(position).getDetails().replaceAll(",", "\n");
        holder.tv_service.setText(" " + Html.fromHtml(message));
        if (plan_details.get(position).getType() == 0) {

            holder.tv_setvicetypename.setText("Inside tunnel");

        } else {
            holder.tv_setvicetypename.setText("Outside tunnel");
        }

        if (plan_details.get(position).getExtra_plan_details().isEmpty()) {

            holder.checkBox.setVisibility(View.GONE);
        } else {
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.checkBox.setText("Add" + " $" + plan_details.get(position).getExtra_plan_details().get(0).getAmount() + " " + plan_details.get(position).getExtra_plan_details().get(0).getName());
        }

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    float parseFloat = holder.netprice + holder.FinalAmount + Float.parseFloat(plan_details.get(position).getExtra_plan_details().get(0).getAmount());
                    Log.e(TAG, "onBindViewHolder: float value======" + parseFloat);
                    holder.camount = String.valueOf(parseFloat);
                    holder.tv_joinnow.setText("Select " + " $" + holder.camount);
                    Preference.setEXTRAPLANAMOUNT(Float.parseFloat(plan_details.get(position).getExtra_plan_details().get(0).getAmount()));

                } else {

//                    float parseFloat = Float.parseFloat(holder.camount) - Float.parseFloat(plan_details.get(position).getExtra_plan_details().get(0).getAmount());
                    float parseFloat = holder.netprice + holder.FinalAmount;
                    Log.e(TAG, "onBindViewHolder: float value======" + parseFloat);
                    holder.camount = String.valueOf(parseFloat);
                    holder.tv_joinnow.setText("Select " + " $" + holder.camount);
                    Preference.setEXTRAPLANAMOUNT(0);

                }
            }
        });

        /*holder.ll_carsize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (plan_details.get(position).getType() == 0) {

                    new AlertDialog.Builder(activity)
                            .setTitle("Select Car Type")
                            .setAdapter(WashFragment.adapterinside, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    holder.tv_csname.setText(WashFragment.insidecarsizelist.get(which).getName());
                                    holder.tv_csamount.setText("$ " + WashFragment.insidecarsizelist.get(which).getAmount());


                                    dialog.dismiss();

                                }
                            }).create().show();
                } else {
                    new AlertDialog.Builder(activity)
                            .setTitle("Select Car Type")
                            .setAdapter(WashFragment.adapteroutside, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    holder.tv_csname.setText(WashFragment.outsidecarsizelist.get(which).getName());
                                    holder.tv_csamount.setText("$ " + WashFragment.outsidecarsizelist.get(which).getAmount());


                                    dialog.dismiss();

                                }
                            }).create().show();
                }
            }
        });*/
        holder.tv_applypcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }


            private void openDialog() {


                dialog = new Dialog(activity);
                dialog.setContentView(R.layout.layout_dialog);
                final CustomRobotoEditText et_email = dialog.findViewById(R.id.et_email);
                final CustomRobotoEditText et_pwd = dialog.findViewById(R.id.et_pwd);
                final CustomRobotoEditText et_pcode = dialog.findViewById(R.id.et_pcode);
                final CustomRobotoTextView tv_title = dialog.findViewById(R.id.tv_title);

                tv_title.setVisibility(View.GONE);
                et_pwd.setVisibility(View.GONE);
                et_email.setVisibility(View.GONE);
                et_pcode.setVisibility(View.VISIBLE);
                CustomRobotoTextView tv_submit = dialog.findViewById(R.id.tv_submit);


                tv_submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        promocode = et_pcode.getText().toString().trim();
                        if (promocode.isEmpty()) {
                            et_pcode.setError(activity.getString(R.string.epcode));
                            et_pcode.requestFocus();
                        } else {
                            if (Util.isConnected(activity)) {
                                PromocodeData();
                            } else {
                                Util.ShowToastMsg(activity, activity.getString(R.string.noconn));
                            }
                        }
                    }

                    private void PromocodeData() {
                        Util.showCustomLoader(activity);
                        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
                        Call<PromocodeResponse> call = apiInterface.getPromocodeData(Preference.getAPI_TOKEN(), promocode, String.valueOf(carwashid));
                        call.enqueue(new Callback<PromocodeResponse>() {
                            @Override
                            public void onResponse(Call<PromocodeResponse> call, Response<PromocodeResponse> response) {
                                Util.dismissCustomLoader();
                                Log.e("c", "data: " + new Gson().toJson(response.body()));
                                int status = response.body().getStatus();
                                switch (status) {
                                    case 1:
                                        holder.tv_applypcode.setTextSize(14);
                                        holder.tv_applypcode.setText("Promocode applied successfully");
                                        holder.tv_applypcode.setClickable(false);
                                        holder.tv_applypcode.setFocusableInTouchMode(false);
                                        Preference.setPROMOCODE(promocode);
                                        float amount = 0;
                                        if (response.body().getData().getType().equals("0")) {
                                            amount = Float.parseFloat(response.body().getData().getDiscount());
                                        } else if (response.body().getData().getType().equals("1")) {
                                            per = Integer.parseInt(response.body().getData().getDiscount());
                                            if (per > 0) {
                                                amount = (per * plan_details.get(position).getPrice()) / 100;
                                            }
                                        }

                                        amount = plan_details.get(position).getPrice() - amount;
                                        holder.netprice = amount;
                                        holder.tv_amount.setText("$ " + amount + "/month");
                                        if (holder.rb_1.isChecked()) {
                                            amount = amount + Float.parseFloat(holder.amount0);

                                        } else if (holder.rb_2.isChecked()) {
                                            amount = amount + Float.parseFloat(holder.amount1);

                                        } else if (holder.rb_3.isChecked()) {
                                            amount = amount + Float.parseFloat(holder.amount2);

                                        } else if (holder.rb_4.isChecked()) {
                                            amount = amount + Float.parseFloat(holder.amount3);

                                        } else if (holder.rb_5.isChecked()) {
                                            amount = amount + Float.parseFloat(holder.amount4);

                                        } else if (holder.rb_6.isChecked()) {
                                            amount = amount + Float.parseFloat(holder.amount5);

                                        }
                                        if (holder.checkBox.isChecked()) {
                                            holder.tv_joinnow.setText("Select " + " $" + (amount + Float.parseFloat(plan_details.get(position).getExtra_plan_details().get(0).getAmount())));
                                        } else {
                                            holder.tv_joinnow.setText("Select " + " $" + amount);
                                        }
//                                            holder.tv_oldamount.setVisibility(View.VISIBLE);
//                                            holder.tv_oldamount.setPaintFlags(holder.tv_oldamount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


                                        break;
                                    case 0:

                                        Util.ShowToastMsg(activity, response.body().getMessage());
                                        break;
                                    case 404:
                                        Intent intent = new Intent(activity, LoginActivity.class);
                                        intent.putExtra("type", "login");
                                        activity.startActivity(intent);
                                        activity.finish();
                                        break;
                                }


                            }

                            @Override
                            public void onFailure(Call<PromocodeResponse> call, Throwable t) {
                                Util.dismissCustomLoader();
                            }
                        });

                        dialog.dismiss();
                    }

                });


                dialog.show();
            }

        });
        if (plan_details.get(position).getType() == 0) {


            if (WashFragment.insidecarsizelist.size() > 0) {
                holder.tva_1.setText("$ " + WashFragment.insidecarsizelist.get(0).getAmount());
                holder.tva_2.setText("$ " + WashFragment.insidecarsizelist.get(1).getAmount());
                holder.tva_3.setText("$ " + WashFragment.insidecarsizelist.get(2).getAmount());
                holder.tva_4.setText("$ " + WashFragment.insidecarsizelist.get(3).getAmount());
                holder.tva_5.setText("$ " + WashFragment.insidecarsizelist.get(4).getAmount());
                holder.tva_6.setText("$ " + WashFragment.insidecarsizelist.get(5).getAmount());


                holder.amount0 = WashFragment.insidecarsizelist.get(0).getAmount();
                holder.amount1 = WashFragment.insidecarsizelist.get(1).getAmount();
                holder.amount2 = WashFragment.insidecarsizelist.get(2).getAmount();
                holder.amount3 = WashFragment.insidecarsizelist.get(3).getAmount();
                holder.amount4 = WashFragment.insidecarsizelist.get(4).getAmount();
                holder.amount5 = WashFragment.insidecarsizelist.get(5).getAmount();
            }
        } else if (plan_details.get(position).getType() == 1) {

            if (WashFragment.outsidecarsizelist.size() > 0) {

                holder.tva_1.setText("$ " + WashFragment.outsidecarsizelist.get(0).getAmount());
                holder.tva_2.setText("$ " + WashFragment.outsidecarsizelist.get(1).getAmount());
                holder.tva_3.setText("$ " + WashFragment.outsidecarsizelist.get(2).getAmount());
                holder.tva_4.setText("$ " + WashFragment.outsidecarsizelist.get(3).getAmount());
                holder.tva_5.setText("$ " + WashFragment.outsidecarsizelist.get(4).getAmount());
                holder.tva_6.setText("$ " + WashFragment.outsidecarsizelist.get(5).getAmount());

                holder.amount0 = WashFragment.outsidecarsizelist.get(0).getAmount();
                holder.amount1 = WashFragment.outsidecarsizelist.get(1).getAmount();
                holder.amount2 = WashFragment.outsidecarsizelist.get(2).getAmount();
                holder.amount3 = WashFragment.outsidecarsizelist.get(3).getAmount();
                holder.amount4 = WashFragment.outsidecarsizelist.get(4).getAmount();
                holder.amount5 = WashFragment.outsidecarsizelist.get(5).getAmount();

            }
        } else if (plan_details.get(position).getType() == 2) {

            if (WashFragment.outsidecarsizelist.size() > 0) {

                holder.tva_1.setText("$ " + WashFragment.normalsizeList.get(0).getAmount());
                holder.tva_2.setText("$ " + WashFragment.normalsizeList.get(1).getAmount());
                holder.tva_3.setText("$ " + WashFragment.normalsizeList.get(2).getAmount());
                holder.tva_4.setText("$ " + WashFragment.normalsizeList.get(3).getAmount());
                holder.tva_5.setText("$ " + WashFragment.normalsizeList.get(4).getAmount());
                holder.tva_6.setText("$ " + WashFragment.normalsizeList.get(5).getAmount());

                holder.amount0 = WashFragment.normalsizeList.get(0).getAmount();
                holder.amount1 = WashFragment.normalsizeList.get(1).getAmount();
                holder.amount2 = WashFragment.normalsizeList.get(2).getAmount();
                holder.amount3 = WashFragment.normalsizeList.get(3).getAmount();
                holder.amount4 = WashFragment.normalsizeList.get(4).getAmount();
                holder.amount5 = WashFragment.normalsizeList.get(5).getAmount();

            }
        }




      /*  holder.tv_setvicetype.setShadowLayer(
                6f *//* radius*//*,
                4f *//* x-offset *//*,
                4f *//* y-offset *//*,
                Color.BLACK);
*/


    }

    @Override
    public int getItemCount() {
        return plan_details.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout ll_bbcolor, ll_carsize;

        RadioGroup radioGroup;
        FrameLayout ff_textlayout;
        TextView tv_extraAmount, tv_setvicetype, tv_setvicetypeename, tv_setvicetypename, tv_joinnow, tv_amount, tv_service, tv_oldamount, tv_applypcode, tv_csamount, tv_csname, tva_1, tva_2, tva_3, tva_4, tva_5, tva_6, tvn_1, tvn_2, tvn_3, tvn_4, tvn_5, tvn_6;
        RadioButton rb_1, rb_2, rb_3, rb_4, rb_5, rb_6;
        String cname, camount = "0.00", amount0, amount1, amount2, amount3, amount4, amount5;
        CardView cardSelect, CardApply;
        LinearLayout llAmount;
        CheckBox checkBox;
        float FinalAmount, netprice;

        public ViewHolder(View itemView) {
            super(itemView);


            rb_1 = itemView.findViewById(R.id.rb_1);
            rb_2 = itemView.findViewById(R.id.rb_2);
            rb_3 = itemView.findViewById(R.id.rb_3);
            rb_4 = itemView.findViewById(R.id.rb_4);
            rb_5 = itemView.findViewById(R.id.rb_5);
            rb_6 = itemView.findViewById(R.id.rb_6);
            ll_bbcolor = itemView.findViewById(R.id.ll_bbcolor);
            ll_carsize = itemView.findViewById(R.id.ll_carsize);
            ff_textlayout = itemView.findViewById(R.id.ff_textlayout);
            llAmount = itemView.findViewById(R.id.llAmount);
            tv_setvicetype = itemView.findViewById(R.id.tv_setvicetype);
            tv_setvicetypename = itemView.findViewById(R.id.tv_setvicetypename);
            tv_setvicetypeename = itemView.findViewById(R.id.tv_setvicetypeename);
            tv_joinnow = itemView.findViewById(R.id.tv_joinnow);
            tv_amount = itemView.findViewById(R.id.tv_amount);
//            tv_oldamount = itemView.findViewById(R.id.tv_oldamount);
            tv_service = itemView.findViewById(R.id.tv_service);
            cardSelect = itemView.findViewById(R.id.cardSelect);
            CardApply = itemView.findViewById(R.id.cardPromoCode);
            tv_applypcode = itemView.findViewById(R.id.tv_applypcode);
            tv_csname = itemView.findViewById(R.id.tv_csname);
            tv_extraAmount = itemView.findViewById(R.id.tv_extraAmount);
            checkBox = itemView.findViewById(R.id.checkBox);
            tv_csamount = itemView.findViewById(R.id.tv_csamount);
            radioGroup = itemView.findViewById(R.id.radioGroup);
            tva_1 = itemView.findViewById(R.id.tva_1);
            tva_2 = itemView.findViewById(R.id.tva_2);
            tva_3 = itemView.findViewById(R.id.tva_3);
            tva_4 = itemView.findViewById(R.id.tva_4);
            tva_5 = itemView.findViewById(R.id.tva_5);
            tva_6 = itemView.findViewById(R.id.tva_6);
            tvn_1 = itemView.findViewById(R.id.tvn_1);
            tvn_2 = itemView.findViewById(R.id.tvn_2);
            tvn_3 = itemView.findViewById(R.id.tvn_3);
            tvn_4 = itemView.findViewById(R.id.tvn_4);
            tvn_5 = itemView.findViewById(R.id.tvn_5);
            tvn_6 = itemView.findViewById(R.id.tvn_6);


            cname = tvn_1.getText().toString();
            Typeface tf = Typeface.createFromAsset(activity.getAssets(), "fonts/Montserrat_SemiBold.otf");
            tv_setvicetype.setTypeface(tf);
            tv_joinnow.setTypeface(tf);
            tv_joinnow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Preference.setPLANID(plan_details.get(getAdapterPosition()).getPlan_id());
//                    Preference.setPRICE(String.valueOf(plan_details.get(getAdapterPosition()).getPrice()));
                    Preference.setCARSIZEAMOUNT(FinalAmount);
                    Preference.setCARTYPE(cname);
                    loadFrgment.loadFragm(getAdapterPosition(), netprice);


                }
            });
            rb_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        cname = WashFragment.outsidecarsizelist.get(0).getName();
                        FinalAmount = Float.parseFloat(amount0);
                        float parseFloat = netprice + Float.parseFloat(amount0);
                        if (checkBox.isChecked()) {
                            camount = String.valueOf(parseFloat + Float.parseFloat(plan_details.get(getAdapterPosition()).getExtra_plan_details().get(0).getAmount()));
                        } else {
                            camount = String.valueOf(parseFloat);
                        }

                        Log.e(TAG, "onBindViewHolder: float value======" + parseFloat);
                        tv_extraAmount.setText("Pay Extra " + "$" + amount0 + " " + cname);
                        rb_2.setChecked(false);
                        rb_3.setChecked(false);
                        rb_4.setChecked(false);
                        rb_5.setChecked(false);
                        rb_6.setChecked(false);
//                        FinalAmount = FinalAmount + Float.parseFloat(amount0);
                        tv_joinnow.setText("Select " + " $" + camount);

                    } /*else {
                        float parseFloat = Float.parseFloat(camount) - Float.parseFloat(amount0);
                        camount = String.valueOf(parseFloat);
//                    FinalAmount = FinalAmount - Float.parseFloat(amount0);
                        tv_joinnow.setText("Select" + " $" + camount);
                    }*/

                }
            });

            rb_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked) {
                        cname = WashFragment.outsidecarsizelist.get(1).getName();
                        FinalAmount = Float.parseFloat(amount1);
                        float parseFloat = netprice + Float.parseFloat(amount1);
                        if (checkBox.isChecked()) {
                            camount = String.valueOf(parseFloat + Float.parseFloat(plan_details.get(getAdapterPosition()).getExtra_plan_details().get(0).getAmount()));
                        } else {
                            camount = String.valueOf(parseFloat);
                        }
                        tv_extraAmount.setText("Pay Extra " + "$" + amount1 + " " + cname);
                        rb_1.setChecked(false);
                        rb_3.setChecked(false);
                        rb_4.setChecked(false);
                        rb_5.setChecked(false);
                        rb_6.setChecked(false);
//                    FinalAmount = FinalAmount + Float.parseFloat(amount1);
                        tv_joinnow.setText("Select " + "$" + camount);
                    } /*else {
                        float parseFloat = Float.parseFloat(camount) - Float.parseFloat(amount1);
                        camount = String.valueOf(parseFloat);
//                    FinalAmount = FinalAmount - Float.parseFloat(amount1);
                        tv_joinnow.setText("Select" + " $" + camount);
                    }*/

                }
            });
            rb_3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked) {
                        cname = WashFragment.outsidecarsizelist.get(2).getName();
                        FinalAmount = Float.parseFloat(amount2);
                        float parseFloat = netprice + Float.parseFloat(amount2);
                        if (checkBox.isChecked()) {
                            camount = String.valueOf(parseFloat + Float.parseFloat(plan_details.get(getAdapterPosition()).getExtra_plan_details().get(0).getAmount()));
                        } else {
                            camount = String.valueOf(parseFloat);
                        }
                        tv_extraAmount.setText("Pay Extra " + "$" + amount2 + " " + cname);
                        rb_2.setChecked(false);
                        rb_1.setChecked(false);
                        rb_4.setChecked(false);
                        rb_5.setChecked(false);
//                    FinalAmount = FinalAmount - Float.parseFloat(amount2);
                        rb_6.setChecked(false);
                        tv_joinnow.setText("Select " + " $" + camount);
                    } /*else {
                        float parseFloat = Float.parseFloat(camount) - Float.parseFloat(amount2);
                        camount = String.valueOf(parseFloat);
//                    FinalAmount = FinalAmount - Float.parseFloat(amount2);
                        tv_joinnow.setText("Select" + " $" + camount);
                    }*/


                }
            });
            rb_4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked) {
                        cname = WashFragment.outsidecarsizelist.get(3).getName();
                        FinalAmount = Float.parseFloat(amount3);
                        float parseFloat = netprice + Float.parseFloat(amount3);
                        if (checkBox.isChecked()) {
                            camount = String.valueOf(parseFloat + Float.parseFloat(plan_details.get(getAdapterPosition()).getExtra_plan_details().get(0).getAmount()));
                        } else {
                            camount = String.valueOf(parseFloat);
                        }
                        tv_extraAmount.setText("Pay Extra " + "$" + amount3 + " " + cname);
                        rb_2.setChecked(false);
                        rb_3.setChecked(false);
                        rb_1.setChecked(false);
                        rb_5.setChecked(false);
                        rb_6.setChecked(false);
//                    FinalAmount = FinalAmount + Float.parseFloat(amount3);
                        tv_joinnow.setText("Select " + " $" + camount);
                    } /*else {
                        float parseFloat = Float.parseFloat(camount) - Float.parseFloat(amount3);
                        camount = String.valueOf(parseFloat);
//                    FinalAmount = FinalAmount - Float.parseFloat(amount3);
                        tv_joinnow.setText("Select" + " $" + camount);
                    }*/


                }
            });
            rb_5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked) {
                        cname = WashFragment.outsidecarsizelist.get(4).getName();
                        FinalAmount = Float.parseFloat(amount4);
                        float parseFloat = netprice + Float.parseFloat(amount4);
                        if (checkBox.isChecked()) {
                            camount = String.valueOf(parseFloat + Float.parseFloat(plan_details.get(getAdapterPosition()).getExtra_plan_details().get(0).getAmount()));
                        } else {
                            camount = String.valueOf(parseFloat);
                        }
                        tv_extraAmount.setText("Pay Extra " + "$" + amount4 + " " + cname);
                        rb_2.setChecked(false);
                        rb_3.setChecked(false);
                        rb_4.setChecked(false);
                        rb_1.setChecked(false);
//                    FinalAmount = FinalAmount + Float.parseFloat(amount5);
                        rb_6.setChecked(false);
                        tv_joinnow.setText("Select " + " $" + camount);
                    }/* else {
                        float parseFloat = Float.parseFloat(camount) - Float.parseFloat(amount4);
                        camount = String.valueOf(parseFloat);
//                    FinalAmount = FinalAmount - Float.parseFloat(amount4);
                        tv_joinnow.setText("Select" + " $" + camount);
                    }*/


                }
            });
            rb_6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked) {
                        cname = WashFragment.outsidecarsizelist.get(5).getName();
//                    camount = amount5;
                        FinalAmount = Float.parseFloat(amount5);
                        float parseFloat = netprice + Float.parseFloat(amount5);
                        if (checkBox.isChecked()) {
                            camount = String.valueOf(parseFloat + Float.parseFloat(plan_details.get(getAdapterPosition()).getExtra_plan_details().get(0).getAmount()));
                        } else {
                            camount = String.valueOf(parseFloat);
                        }
                        tv_extraAmount.setText("Pay Extra " + "$" + amount5 + " " + cname);
                        tv_joinnow.setText("Select " + " $" + camount);
                        rb_2.setChecked(false);
                        rb_3.setChecked(false);
                        rb_4.setChecked(false);
//                    FinalAmount = FinalAmount + Float1parseFloat(amount5);
                        rb_1.setChecked(false);
                        rb_5.setChecked(false);
                    } /*else {
                        float parseFloat = Float.parseFloat(camount) - Float.parseFloat(amount5);
                        camount = String.valueOf(parseFloat);
//                    FinalAmount = FinalAmount - Float.parseFloat(amount5);
                        tv_joinnow.setText("Select" + " $" + camount);
                    }*/


                }
            });
        }


    }


}
