package com.silverfox.carwash.adapter;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.silverfox.carwash.Fragment.HistoryFragment;
import com.silverfox.carwash.Fragment.MemberShipFragment;
import com.silverfox.carwash.Fragment.MembershipTFragment;
import com.silverfox.carwash.Fragment.WalletTFragment;

public class MyAdapter extends FragmentStatePagerAdapter {

    private Activity myContext;
    int totalTabs;

    public MyAdapter(Activity context, FragmentManager fm, int totalTabs) {


        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;


    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {


        switch (position) {
         /*   case 0:
                WalletTFragment homeFragment = new WalletTFragment();
                return homeFragment;
*/
            case 0:
                MembershipTFragment sportFragment = new MembershipTFragment();
                return sportFragment;


            default:
                return null;
        }
    }

    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}