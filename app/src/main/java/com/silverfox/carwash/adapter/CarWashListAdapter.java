package com.silverfox.carwash.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.silverfox.carwash.Model.CarWashListResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;

import java.util.ArrayList;

public class CarWashListAdapter extends RecyclerView.Adapter<CarWashListAdapter.ViewHolder> {

    Activity activity;
    Context context;
    ArrayList<CarWashListResponse.DataBean> carwashlistData;
    private loadFrgment loadFrgment;


    public CarWashListAdapter(Activity activity,
                              ArrayList<CarWashListResponse.DataBean> carwashlistData,
                              loadFrgment loadFrgment) {
        this.activity = activity;
        this.carwashlistData = carwashlistData;
        this.loadFrgment = loadFrgment;
    }

    public interface loadFrgment{
        void loadFragm(int position);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View contactView = LayoutInflater.from(activity).inflate(R.layout.row_location_detail, parent, false);

        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.tv_name.setText(carwashlistData.get(position).getShop_name());
        holder.tv_address.setText(carwashlistData.get(position).getFull_address());
        holder.tv_city.setText(carwashlistData.get(position).getCity());
        holder.tv_distance.setText(carwashlistData.get(position).getDistance()+ "");

        Glide.with(activity)
                .load(carwashlistData.get(position).getLogo())
                .priority(Priority.HIGH)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.iv_image);

        holder.ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Preference.setCARWASHOWNERID(carwashlistData.get(position).getCarwash_id());
                loadFrgment.loadFragm(position);
            }

        });

    }



    @Override
    public int getItemCount() {
        return carwashlistData.size();


    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_name, tv_address, tv_city, tv_distance;
        private ImageView iv_image;
        private CardView ll_main;



        public ViewHolder(View itemView) {
            super(itemView);

            iv_image = itemView.findViewById(R.id.iv_image);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_city = itemView.findViewById(R.id.tv_city);
            tv_distance = itemView.findViewById(R.id.tv_distance);
            ll_main = itemView.findViewById(R.id.ll_main);



        }
    }


}