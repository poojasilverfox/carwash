package com.silverfox.carwash.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.Fragment.MemberShipFragment;
import com.silverfox.carwash.Model.MailInvoiceResponse;
import com.silverfox.carwash.Model.MembershipTListResponse;
import com.silverfox.carwash.Model.PrintInvoiceResponse;
import com.silverfox.carwash.Model.ReactivePlanResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class MembershipTadapter extends RecyclerView.Adapter<MembershipTadapter.ViewHolder> {

    Activity activity;
    int[] array;
    List<MembershipTListResponse.DataBean> membershipTTdata;
    ApiInterface apiInterface;

    public MembershipTadapter(FragmentActivity activity, ArrayList<MembershipTListResponse.DataBean> membershipTTdata) {
        this.activity = activity;
        this.membershipTTdata = membershipTTdata;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View contactView = LayoutInflater.from(activity).inflate(R.layout.row_membershipt_list, parent, false);

        final ViewHolder viewHolder = new ViewHolder(contactView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.id = membershipTTdata.get(position).getId();
        holder.tv_name.setText(membershipTTdata.get(position).getMembership_full_details().getCarwash_owner_details().getOwner_name());
        holder.tv_planname.setText(membershipTTdata.get(position).getMembership_full_details().getWash_plan());
        holder.tv_date.setText(membershipTTdata.get(position).getCreated_at());
        holder.tv_amount.setText("$ " + membershipTTdata.get(position).getAmount());

    }

    @Override
    public int getItemCount() {
        return membershipTTdata.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView tv_name, tv_planname, tv_date, tv_amount, tv_email, tv_print;
        String id;


        public ViewHolder(View itemView) {
            super(itemView);


            tv_name = itemView.findViewById(R.id.tv_name);
            tv_planname = itemView.findViewById(R.id.tv_planname);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_email = itemView.findViewById(R.id.tv_email);
            tv_print = itemView.findViewById(R.id.tv_print);


            tv_email.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EmailData();
                }
            });
            tv_print.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    printDataAPI(getAdapterPosition());
                }
            });

        }



        private void EmailData() {
            Log.e("tag", "EmailData: id" + id + Preference.getAPI_TOKEN());
            Util.showCustomLoader(activity);
            apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);

            Call<MailInvoiceResponse> call = apiInterface.getMailinvoice(Preference.getAPI_TOKEN(), id);
            call.enqueue(new Callback<MailInvoiceResponse>() {
                @Override
                public void onResponse(Call<MailInvoiceResponse> call, Response<MailInvoiceResponse> response) {
                    Util.dismissCustomLoader();
                    Log.e("hellooooooo", "data: " + new Gson().toJson(response.body()));
                    int status = response.body().getStatus();

                    Util.ShowToastMsg(activity, response.body().getMessage());

                    switch (status) {
                        case 1:

                            break;
                        case 404:
                            Intent intent = new Intent(activity, LoginActivity.class);
                            intent.putExtra("type", "login");
                            activity.startActivity(intent);
                            activity.finish();
                            break;
                    }


                }

                @Override
                public void onFailure(Call<MailInvoiceResponse> call, Throwable t) {
                    Util.dismissCustomLoader();
                    Log.e("tg", "onFailure: email invoice" + t.toString());


                }
            });
        }

    }

    private void printDataAPI(int pos) {
        Util.showCustomLoader(activity);
        apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);

        Call<PrintInvoiceResponse> call = apiInterface.getPrintData(Preference.getAPI_TOKEN(), Integer.parseInt(membershipTTdata.get(pos).getId()));
        call.enqueue(new Callback<PrintInvoiceResponse>() {
            @Override
            public void onResponse(Call<PrintInvoiceResponse> call, Response<PrintInvoiceResponse> response) {

//                Log.e("hellooooooo", "data: " + new Gson().toJson(response.body()));
                int status = response.body().getStatus();

//                Util.ShowToastMsg(activity, response.body().getMessage());

                switch (status) {
                    case 1:
                        doWebViewPrint(response.body().getData());
                        break;
                    case 404:
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.putExtra("type", "login");
                        activity.startActivity(intent);
                        activity.finish();
                        break;
                }


            }

            @Override
            public void onFailure(Call<PrintInvoiceResponse> call, Throwable t) {
                Util.dismissCustomLoader();
                Log.e("tg", "onFailure: email invoice" + t.toString());


            }
        });
    }
    private void doWebViewPrint(String htmlData) {
        // Create a WebView object specifically for printing
        WebView webView = new WebView(activity);
        webView.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                Log.i(TAG, "page finished loading " + url);
                createWebPrintJob(view);
            }
        });

        webView.loadDataWithBaseURL(null, htmlData, "text/HTML", "UTF-8", null);

    }

    private void createWebPrintJob(WebView webView) {

        // Get a PrintManager instance
        PrintManager printManager = (PrintManager) activity
                .getSystemService(Context.PRINT_SERVICE);

        String jobName = activity.getString(R.string.app_name) + " Document";

        // Get a print adapter instance
        PrintDocumentAdapter printAdapter = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            printAdapter = webView.createPrintDocumentAdapter(jobName);
        }

        // Create a print job with name and adapter instance
        printManager.print(jobName, printAdapter,
                new PrintAttributes.Builder().build());
        Util.dismissCustomLoader();

    }
}




