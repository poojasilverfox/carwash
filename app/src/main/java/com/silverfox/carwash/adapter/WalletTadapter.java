package com.silverfox.carwash.adapter;

import android.app.Activity;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.silverfox.carwash.Model.WalletTransactionResponse;
import com.silverfox.carwash.R;

import java.util.ArrayList;
import java.util.List;

public class WalletTadapter extends RecyclerView.Adapter<WalletTadapter.ViewHolder> {

    Activity activity;
    int[] array;
    List<WalletTransactionResponse.DataBean> walletTdata;


    public WalletTadapter(FragmentActivity activity, ArrayList<WalletTransactionResponse.DataBean> walletTdata) {
        this.activity = activity;
        this.walletTdata = walletTdata;
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View contactView = LayoutInflater.from(activity).inflate(R.layout.row_wallet_list, parent, false);

        final ViewHolder viewHolder = new ViewHolder(contactView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        holder.tv_date.setText(walletTdata.get(position).getCreated_at());
        holder.tv_amount.setText("$ " + walletTdata.get(position).getAmount());
        holder.tv_type.setText(walletTdata.get(position).getType());
        holder.tv_reedemcode.setText(walletTdata.get(position).getRedeem_code());


    }

    @Override
    public int getItemCount() {
        return walletTdata.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView tv_date, tv_amount, tv_type, tv_reedemcode;

        public ViewHolder(View itemView) {
            super(itemView);


            tv_date = itemView.findViewById(R.id.tv_date);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_type = itemView.findViewById(R.id.tv_type);
            tv_reedemcode = itemView.findViewById(R.id.tv_reedemcode);


        }
    }

}
