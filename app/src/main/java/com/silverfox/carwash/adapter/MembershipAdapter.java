package com.silverfox.carwash.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.silverfox.carwash.Activity.LoginActivity;
import com.silverfox.carwash.Api.ApiClient;
import com.silverfox.carwash.Api.ApiInterface;
import com.silverfox.carwash.Model.MembershipListResponse;
import com.silverfox.carwash.Model.ReactivePlanResponse;
import com.silverfox.carwash.Model.RepaymentPlanResponse;
import com.silverfox.carwash.Model.UpdateResponse;
import com.silverfox.carwash.Preference;
import com.silverfox.carwash.R;
import com.silverfox.carwash.Util;
import com.silverfox.carwash.view.CustomRobotoEditText;
import com.silverfox.carwash.view.CustomRobotoTextView;
import com.tooltip.Tooltip;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MembershipAdapter extends RecyclerView.Adapter<MembershipAdapter.ViewHolder> {

    public ReloadAllData reloadAllData;
    Activity activity;
    loadFrgment loadFrgment;
    ArrayList<MembershipListResponse.DataBean> memebershipList;
    int membership_id;


    public MembershipAdapter(FragmentActivity activity,
                             ArrayList<MembershipListResponse.DataBean> memebershipList,
                             loadFrgment loadFrgment, ReloadAllData reloadAllData) {
        this.activity = activity;
        this.memebershipList = memebershipList;
        this.loadFrgment = loadFrgment;
        this.reloadAllData = reloadAllData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View contactView = LayoutInflater.from(activity).inflate(R.layout.row_membership_plan_list, parent, false);

        return new ViewHolder(contactView);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {


        if ((memebershipList.get(position).getDeactive_plan() == 1)) {
            holder.tv_status.setText("Deactivated Customer Membership");
            holder.text = "HomeWash tried to make payment 3 times but HomeWash was unable to make payment. Hance, HomeWash has deactivated your account. You need to reactivate it by paying extra $20.";
            holder.status = "Deactive";
            holder.tv_status.setTextColor(activity.getResources().getColor(R.color.red));
            holder.tv_statusbtn.setVisibility(View.GONE);
            holder.tv_UpdateInfo.setVisibility(View.GONE);

        } else if ((memebershipList.get(position).getIs_cancel() == 1)) {
            holder.tv_status.setText("Cancelled on " + memebershipList.get(position).getCancel_date());
            holder.tv_status.setTextColor(activity.getResources().getColor(R.color.red));
            holder.text = "This membership has been cancelled by you. So you can not use this plan until your next 6 months from the date of cancelled.";
            holder.tv_statusbtn.setVisibility(View.VISIBLE);
            holder.tv_UpdateInfo.setVisibility(View.GONE);
            holder.tv_statusbtn.setText("Retry");
        } else if ((memebershipList.get(position).getIs_failed() == 1)) {
            holder.tv_status.setText("Failed(Payment Declined)");
            holder.text = "HomeWash tried to make payment for this month for this membership but HomeWash was unable to complete the transaction due to some reason";
            holder.status = "Retry";
            holder.tv_statusbtn.setVisibility(View.VISIBLE);
            holder.tv_statusbtn.setText("Retry");
            holder.tv_UpdateInfo.setVisibility(View.VISIBLE);
        } else if ((memebershipList.get(position).getInactive_plan() == 1)) {
            holder.tv_status.setText("Inactive");
            holder.text = "Your payment that was due could not be processed for 30 consecutive days so your membership has become inactive. If you would like to reinstate your plan, please update your payment information and then press the reactivation button. Please note that there is a $20 reactivation fee as stated in your terms &amp; conditions, in addition to your regular monthly rate, but then your membership will start over again with a new 30 day paid billing cycle.";
            holder.status = "Inactive";
            holder.tv_status.setTextColor(activity.getResources().getColor(R.color.colorAccent));
            holder.tv_statusbtn.setVisibility(View.VISIBLE);
            holder.tv_statusbtn.setText("Reactivate");
        } else if (memebershipList.get(position).getIs_freeze() == 1) {
            holder.tv_status.setText("Freeze until " + memebershipList.get(position).getFreeze_date());
            holder.tv_status.setTextColor(activity.getResources().getColor(R.color.colorAccent));
            holder.text = "This membership plan has been Freezed by you. So you can not use this membership till " + memebershipList.get(position).getFreeze_date();

        } else {
            holder.tv_status.setText("Active");
            holder.tv_status.setTextColor(activity.getResources().getColor(R.color.green1));
            holder.text = "This membership plan is active. You may use this plan to get your car wash.";
        }

//        Preference.setSTATUS(holder.tv_status.getText().toString());
        holder.tv_shopname.setText(memebershipList.get(position).getCarwash_owner_details().getShop_name());
        holder.tv_planname.setText(memebershipList.get(position).getPlan_details().getName());
        holder.tv_address.setText(memebershipList.get(position).getCarwash_owner_details().getFull_address());
        holder.tv_notes.setText(memebershipList.get(position).getNote());
        holder.noteold = memebershipList.get(position).getNote();

        membership_id = memebershipList.get(position).getMembership_id();

        Glide.with(activity)
//                .load(BuildConfig.BASE_URLFrames+frame_data.get(position).getLogo())
                .load(memebershipList.get(position).getQr_code())
//                .placeholder(R.drawable.loader)
//                .error(R.drawable.ic_error)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.iv_scan);


        if (!memebershipList.get(position).getQr_code().equals(""))
            Preference.setQRCODE(memebershipList.get(position).getQr_code());
        else
            Preference.setQRCODE("");

    }

    @Override
    public int getItemCount() {
        return memebershipList.size();

    }

    public interface loadFrgment {
        void loadFragm(int position);
    }

    public interface ReloadAllData {
        void onReloadCall();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        LinearLayout ll_membershipplan;
        TextView tv_shopname, tv_planname, tv_address, tv_notes, tv_status, tv_updatenote, tv_statusbtn, tv_UpdateInfo;
        ImageView iv_scan;
        String text = "";
        String status = "";
        String noteold = "";
        Tooltip mTooltip;
        ApiInterface apiInterface;
        Dialog dialog;

        public ViewHolder(final View itemView) {
            super(itemView);


            ll_membershipplan = itemView.findViewById(R.id.ll_membershipplan);
            tv_shopname = itemView.findViewById(R.id.tv_shopname);
            tv_planname = itemView.findViewById(R.id.tv_planname);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_UpdateInfo = itemView.findViewById(R.id.tv_UpdateInfo);
            iv_scan = itemView.findViewById(R.id.iv_scan);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_notes = itemView.findViewById(R.id.tv_notes);
            tv_updatenote = itemView.findViewById(R.id.tv_updatenote);
            tv_statusbtn = itemView.findViewById(R.id.tv_statusbtn);


            tv_statusbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ReactivePlan();
                }
            });

            ll_membershipplan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTooltip != null) {
                        if (mTooltip.isShowing()) {
                            mTooltip.dismiss();

                        } else {
                            if ((memebershipList.get(getAdapterPosition()).getDeactive_plan() == 1)) {
                                Preference.setSTATUS("Deactive");
                            } else if ((memebershipList.get(getAdapterPosition()).getIs_cancel() == 1)) {
                                Preference.setSTATUS("Cancelled on " + memebershipList.get(getAdapterPosition()).getCancel_date());
                            } else if ((memebershipList.get(getAdapterPosition()).getIs_failed() == 1)) {
                                Preference.setSTATUS("Fail");
                            } else if ((memebershipList.get(getAdapterPosition()).getInactive_plan() == 1)) {
                                Preference.setSTATUS("Inactive");
                            } else if (memebershipList.get(getAdapterPosition()).getIs_freeze() == 1) {
                                Preference.setSTATUS("Freeze");
                            } else {
                                Preference.setSTATUS("Active");
                            }
                            loadFrgment.loadFragm(getAdapterPosition());

                        }

                    } else {
                        if ((memebershipList.get(getAdapterPosition()).getDeactive_plan() == 1)) {
                            Preference.setSTATUS("Deactive");
                        } else if ((memebershipList.get(getAdapterPosition()).getIs_cancel() == 1)) {
                            Preference.setSTATUS("Cancelled on " + memebershipList.get(getAdapterPosition()).getCancel_date());
                        } else if ((memebershipList.get(getAdapterPosition()).getIs_failed() == 1)) {
                            Preference.setSTATUS("Fail");
                        } else if ((memebershipList.get(getAdapterPosition()).getInactive_plan() == 1)) {
                            Preference.setSTATUS("Inactive");
                        } else if (memebershipList.get(getAdapterPosition()).getIs_freeze() == 1) {
                            Preference.setSTATUS("Freeze");
                        } else {
                            Preference.setSTATUS("Active");
                        }
                        loadFrgment.loadFragm(getAdapterPosition());
                    }


                }
            });

            tv_updatenote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openDialog();
                }
            });
            tv_status.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mTooltip == null) {
                        mTooltip = new Tooltip.Builder(v, R.style.Tooltip2)
                                .setCornerRadius(20f)
                                .setText(text)
                                .show();
                    } else {
                        if (mTooltip.isShowing()) {
                            mTooltip.dismiss();
                        } else {
                            mTooltip.show();
                        }
                    }


                }
            });


        }

        private void UpdateNote(final String note) {

            Util.showCustomLoader(activity);
            apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);
            Call<UpdateResponse> call = apiInterface.getUpdateNoteData(Preference.getAPI_TOKEN(), String.valueOf(membership_id), note);
            call.enqueue(new Callback<UpdateResponse>() {
                @Override
                public void onResponse(Call<UpdateResponse> call, Response<UpdateResponse> response) {
                    Util.dismissCustomLoader();
                    Log.e("hellooooooo", "data: " + new Gson().toJson(response.body()));
                    int status = response.body().getStatus();

                    Util.ShowToastMsg(activity, response.body().getMessage());

                    switch (status) {
                        case 1:
                            noteold = note;
                            tv_notes.setText(note);
                            dialog.dismiss();
                            break;
                        case 404:
                            Intent intent = new Intent(activity, LoginActivity.class);
                            intent.putExtra("type", "login");
                            activity.startActivity(intent);
                            activity.finish();
                            break;
                    }


                }

                @Override
                public void onFailure(Call<UpdateResponse> call, Throwable t) {
                    Util.dismissCustomLoader();
                    dialog.dismiss();


                }
            });
        }

        private void ReactivePlan() {


            Util.showCustomLoader(activity);
            apiInterface = ApiClient.getClientResponse().create(ApiInterface.class);

            if (status.equals("Deactive")) {
                Call<ReactivePlanResponse> call = apiInterface.getReactivePlan(Preference.getAPI_TOKEN(), String.valueOf(memebershipList.get(getAdapterPosition()).getMembership_id()));
                call.enqueue(new Callback<ReactivePlanResponse>() {
                    @Override
                    public void onResponse(Call<ReactivePlanResponse> call, Response<ReactivePlanResponse> response) {
                        Util.dismissCustomLoader();
                        Log.e("hellooooooo", "data: " + new Gson().toJson(response.body()));
                        int status = response.body().getStatus();

                        Util.ShowToastMsg(activity, response.body().getMessage());

                        switch (status) {
                            case 1:
                                reloadAllData.onReloadCall();
                                break;
                            case 404:
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.putExtra("type", "login");
                                activity.startActivity(intent);
                                activity.finish();
                                break;
                        }


                    }

                    @Override
                    public void onFailure(Call<ReactivePlanResponse> call, Throwable t) {
                        Util.dismissCustomLoader();
                        dialog.dismiss();


                    }
                });
            } else {

                Call<RepaymentPlanResponse> call = apiInterface.getRepaymentPlan(Preference.getAPI_TOKEN(), String.valueOf(memebershipList.get(getAdapterPosition()).getMembership_id()));
                call.enqueue(new Callback<RepaymentPlanResponse>() {
                    @Override
                    public void onResponse(Call<RepaymentPlanResponse> call, Response<RepaymentPlanResponse> response) {
                        Util.dismissCustomLoader();
                        Log.e("hellooooooo", "data: " + new Gson().toJson(response.body()));
                        int status = response.body().getStatus();

                        Util.ShowToastMsg(activity, response.body().getMessage());

                        switch (status) {
                            case 1:
                                reloadAllData.onReloadCall();
                                break;
                            case 404:
                                Intent intent = new Intent(activity, LoginActivity.class);
                                intent.putExtra("type", "login");
                                activity.startActivity(intent);
                                activity.finish();
                                break;
                        }


                    }

                    @Override
                    public void onFailure(Call<RepaymentPlanResponse> call, Throwable t) {
                        Util.dismissCustomLoader();
                        dialog.dismiss();


                    }
                });
            }
        }


        private void openDialog() {

            dialog = new Dialog(activity);
            dialog.setContentView(R.layout.note_dialog);
            final CustomRobotoEditText et_note = dialog.findViewById(R.id.et_note);
            et_note.setText(noteold);
            et_note.setSelection(et_note.getText().toString().length());
            CustomRobotoTextView tv_submit = dialog.findViewById(R.id.tv_submit);


            tv_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (et_note.getText().toString().length() == 0) {
                        et_note.setError("Enter Note");
                        et_note.requestFocus();
                    } else {
                        UpdateNote(et_note.getText().toString());
                    }

                }
            });


            dialog.show();
        }
    }

}