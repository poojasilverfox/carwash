package com.silverfox.carwash;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.silverfox.carwash.Model.CarWashListResponse;

import java.util.ArrayList;

public class Util {



    public static ArrayList<CarWashListResponse.DataBean> carwashlistData = new ArrayList<>();
    public static Boolean location_flag = false;
    public static Boolean fragment_position_flag = true;
    public static int currentposition = 0 , position = 0,referralposition = 0;
    public static String searchfield = "";

    public static void ShowToastMsg(Activity activity,String message){
        Toast.makeText(activity,message,Toast.LENGTH_LONG).show();
    }

    public static boolean isConnected(Activity activity) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String AndroidId;

    public static String getUniqueId(){
        AndroidId =  Settings.Secure.getString(Application.getApp().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return AndroidId;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static Dialog alertDialog;

    public static void showCustomLoader(Activity activity) {
        if (alertDialog != null && alertDialog.isShowing()) {
            return;
        } else {
            alertDialog = new Dialog(activity);
            alertDialog.setCancelable(false);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.loader_only);
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.show();
        }
    }

    public static void dismissCustomLoader() {
        if (alertDialog != null) {
            if (alertDialog.isShowing()) {
                alertDialog.dismiss();
            }
        }
    }
}
