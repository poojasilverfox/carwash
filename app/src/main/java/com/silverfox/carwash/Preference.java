package com.silverfox.carwash;

import android.content.Context;
import android.content.SharedPreferences;

public class Preference {


    private static final String CUSTOMER_ID = "customer_id";
    private static final String API_TOKEN = "api_token";
    private static final String USER_ID = "user_id";
    private static final String NAME = "name";
    private static final String LNAME = "lname";
    private static final String EMAIL = "email";
    private static final String REFFERCODE = "refercode";
    private static final String MNO = "mno";
    private static final String PRICE = "price";
    private static final String PLANID = "planid";
    private static final String PROMOCODE = "promocode";
    private static final String CARMAKE = "carmake";
    private static final String CARTYPE = "cartype";
    private static final String CARMODEL = "carmodel";
    private static final String COLOR = "color";
    private static final String TYPE = "type";
    private static final String PLATENO = "plateno";
    private static final String CARWASHOWNERID = "carwashownerid";
    private static final String CARDNO = "cardno";
    private static final String MONTH = "month";
    private static final String YEAR = "year";
    private static final String CVV = "cvv";
    private static final String WALLET = "wallet";
    private static final String CARSIZEAMOUNT = "carsizeamount";
    private static final String EXTRAPLANAMOUNT = "extraplanamount";
    private static final String STATUS = "status";
    private static final String QRCODE = "qrcode";


    private static SharedPreferences get() {
        return Application.getApp().getSharedPreferences(
                "Application", Context.MODE_PRIVATE);
    }


    public static void setCUSTOMER_ID(String value) {
        get().edit().putString(CUSTOMER_ID, value).apply();
    }

    public static String getCUSTOMER_ID() {
        return get().getString(CUSTOMER_ID, "");
    }

    public static void setAPI_TOKEN(String value) {
        get().edit().putString(API_TOKEN, value).apply();
    }

    public static String getAPI_TOKEN() {
        return get().getString(API_TOKEN, "");
    }

    public static void setSTATUS(String value) {
        get().edit().putString(STATUS, value).apply();
    }

    public static String getSTATUS() {
        return get().getString(STATUS, "Membership Running");
    }

    public static void setQRCODE(String value) {
        get().edit().putString(QRCODE, value).apply();
    }

    public static String getQRCODE() {
        return get().getString(QRCODE, "");
    }

    public static void setUSER_ID(int value) {
        get().edit().putInt(USER_ID, value).apply();
    }

    public static int getUSER_ID() {
        return get().getInt(USER_ID, 0);
    }

    public static void setNAME(String value) {
        get().edit().putString(NAME, value).apply();
    }

    public static String getNAME() {
        return get().getString(NAME, "");
    }

    public static void setLNAME(String value) {
        get().edit().putString(LNAME, value).apply();
    }

    public static String getLNAME() {
        return get().getString(LNAME, "");
    }

    public static void setEMAIL(String value) {
        get().edit().putString(EMAIL, value).apply();
    }

    public static String getEMAIL() {
        return get().getString(EMAIL, "");
    }

    public static void setREFFERCODE(String value) {
        get().edit().putString(REFFERCODE, value).apply();
    }

    public static String getREFFERCODE() {
        return get().getString(REFFERCODE, "");
    }

    public static void setMNO(String value) {
        get().edit().putString(MNO, value).apply();
    }

    public static String getMNO() {
        return get().getString(MNO, "");
    }

    public static void setPRICE(String value) {
        get().edit().putString(PRICE, value).apply();
    }

    public static String getPRICE() {
        return get().getString(PRICE, "");
    }

    public static void setPLANID(int value) {
        get().edit().putInt(PLANID, value).apply();
    }

    public static int getPLANID() {
        return get().getInt(PLANID, 0);
    }

    public static void setWALLET(int value) {
        get().edit().putInt(WALLET, value).apply();
    }

    public static int getWALLET() {
        return get().getInt(WALLET, 0);
    }

    public static void setPROMOCODE(String value) {
        get().edit().putString(PROMOCODE, value).apply();
    }

    public static String getPROMOCODE() {
        return get().getString(PROMOCODE, "");
    }

    public static void setCARMAKE(String value) {
        get().edit().putString(CARMAKE, value).apply();
    }

    public static String getCARMAKE() {
        return get().getString(CARMAKE, "");
    }

    public static void setCARMODEL(String value) {
        get().edit().putString(CARMODEL, value).apply();
    }

    public static String getCARMODEL() {
        return get().getString(CARMODEL, "");
    }

    public static void setTYPE(String value) {
        get().edit().putString(TYPE, value).apply();
    }

    public static String getTYPE() {
        return get().getString(TYPE, "");
    }

    public static void setCOLOR(String value) {
        get().edit().putString(COLOR, value).apply();
    }

    public static String getCOLOR() {
        return get().getString(COLOR, "");
    }

    public static void setCARWASHOWNERID(int value) {
        get().edit().putInt(CARWASHOWNERID, value).apply();
    }

    public static int getCARWASHOWNERID() {
        return get().getInt(CARWASHOWNERID, 0);
    }

    public static void setPLATENO(String value) {
        get().edit().putString(PLATENO, value).apply();
    }

    public static String getPLATENO() {
        return get().getString(PLATENO, "");
    }

    public static void setCARDNO(String value) {
        get().edit().putString(CARDNO, value).apply();
    }

    public static String getCARDNO() {
        return get().getString(CARDNO, "");
    }

    public static void setCARTYPE(String value) {
        get().edit().putString(CARTYPE, value).apply();
    }

    public static String getCARTYPE() {
        return get().getString(CARTYPE, "");
    }

    public static void setMONTH(String value) {
        get().edit().putString(MONTH, value).apply();
    }

    public static String getMONTH() {
        return get().getString(MONTH, "");
    }

    public static void setYEAR(String value) {
        get().edit().putString(YEAR, value).apply();
    }

    public static String getYEAR() {
        return get().getString(YEAR, "");
    }

    public static void setCVV(String value) {
        get().edit().putString(CVV, value).apply();
    }

    public static String getCVV() {
        return get().getString(CVV, "");
    }

    public static void setCARSIZEAMOUNT(float value) {
        get().edit().putFloat(CARSIZEAMOUNT, value).apply();
    }

    public static float getCARSIZEAMOUNT() {
        return get().getFloat(CARSIZEAMOUNT, 0);
    }

    public static void setEXTRAPLANAMOUNT(float value) {
        get().edit().putFloat(EXTRAPLANAMOUNT, value).apply();
    }

    public static float getEXTRAPLANAMOUNT() {
        return get().getFloat(EXTRAPLANAMOUNT, 0);
    }


}
